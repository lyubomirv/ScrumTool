# ScrumTool

This project was developed for my Master's thesis.

ScrumTool is an online system for managing and tracking a SCRUM process. Users belong to a team, which can have many projects, managed by a SCRUM process. Managing a project means you can edit the Backlog, create sprints, manage sprints by moving tasks around and see charts representing your progress.
