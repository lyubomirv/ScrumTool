/*!
 * jQuery confirmationDialog
 * A jQuery-ui dialog that requires a confirmation from the user
 * The dialog presets some html to the user (possibly an 'Are you sure?' question).
 * The dialog has two buttons - Ok and Cancel.
 * Pressing Cancel closes the dialog.
 * Pressing Ok sends an ajax request to the server, that calls 'callback' on return.
 * 
 * Depends:
 *	jquery.ui.dialog.js
 *
 * @author Lyubomir Vasilev <lyubomirv@gmail.com>
 * @copyright Copyright &copy; 2011
 */
 
;(function($)
{
	var methods =
	{
		/**
		 * init method
		 * Creates a jQuery-ui dialog.
		 * Attaches a handler to the click event that opens the dialog.
		 * Attaches a handler to the clicking on the overlay when the
		 * dialog is open, that will close the dialog.
		 */
		init: function(options)
		{
			settings = $.extend({}, $.fn.confirmationDialog.defaults, options || {});
			
			return this.each(function()
			{
				var $this = $(this);
				var data = $this.data('confirmationDialog');
				
				if(!data)
				{
					if(settings.url === undefined
						|| settings.parameters === undefined)
					{
						return;
					}
					
					var confirmationDialog = $('<div />')
						.html(settings.htmlContent)
						.dialog(
						{
							autoOpen: false,
							modal: true,
							title: settings.title,
							context: confirmationDialog,
							buttons: [
								{
									text: 'Ok',
									click: function()
									{
										$.ajax(
										{
											type: 'POST',
											url: settings.url,
											data: settings.parameters,
											dataType: 'json',
											success: function(data)
											{
												if(data.status === 'ok')
												{
													if(settings.successCallback !== undefined)
													{
														settings.successCallback(data);
													}
												}
												else
												{
													if(settings.errorCallback !== undefined)
													{
														settings.errorCallback(data);
													}
												}
											}
										});
										
										confirmationDialog.dialog('close');
									}
								},
								{
									text: 'Cancel',
									click: function()
									{
										confirmationDialog.dialog('close');
									}
								},
							]
						});
					
					$this.data('confirmationDialog',
					{
						target: $this,
						confirmationDialog: confirmationDialog
					});
					
					$(this).live('click', function()
					{
						methods.open.apply($this);
					});
					
					// Close the dialog on mouse click outside it
					$('.ui-widget-overlay').live('click', function()
					{
						confirmationDialog.dialog('close');
					});
				}
				
				return $this;
			});
		},
		
		/**
		 * open method
		 * Opens the dialog
		 */
		open: function()
		{
			var $this = this;
			
			var data = $this.data('confirmationDialog');
			if(!data)
			{
				return;
			}
			
			var confirmationDialog = data.confirmationDialog;
			confirmationDialog.dialog('open');
			
			return $this;
		},
	};
	
	/**
	 * confirmationDialog
	 */
	$.fn.confirmationDialog = function(method)
	{	
		if(methods[method])
		{
			return methods[method].apply(this, Array.prototype.slice.call(arguments));
		}
		else if(typeof method === 'object' || !method)
		{
			return methods.init.apply(this, arguments);
		}
		else
		{
			$.error('Method ' + method + ' does not exist');
		}
	}
	
	var settings = undefined;
	
	/**
	 * Default settings of the confirmation dialog
	 * @param url string url to access on confirmation
	 * @param parameters object parameters to send to the server on confirmation
	 * @param successCallback function callback for server's success response.
	 * Must be in the form: function(data), where data is an object.
	 * @param errorCallback function callback for server's error response.
	 * Must be in the form: function(data), where data is an object.
	 * @param title string title of the dialog
	 * @param htmlContent string html to fill the dialog with
	 */
	$.fn.confirmationDialog.defaults = {
		url: undefined,
		parameters: undefined,
		successCallback: undefined,
		errorCallback: undefined,
		title: '',
		htmlContent: ''
	};
})(jQuery);
