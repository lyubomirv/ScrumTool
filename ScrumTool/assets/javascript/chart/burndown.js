/*!
 * Burn-down chart drawing
 * 
 * Depends:
 *	raphael.js
 *
 * @author Lyubomir Vasilev <lyubomirv@gmail.com>
 * @copyright Copyright &copy; 2012
 */

/**
 * @param elementId string id of the html element that will hold the chart
 * @param width int width of the chart
 * @param height int height of the chart
 * @param labels array(string) the horizontal labels for the chart
 * @param values array(string) the values for the chart
 * @param valueInformation array(string) the text that will appear for the value point
 */
BurndownChart = function(elementId, width, height, labels, values, valueInformation)
{
	// Private members
	var m_canvas = null;
	var m_width = null;
	var m_height = null;
	var m_labels = null;
	var m_values = null;
	var m_valueInformation = null;
	
	var m_chartOffsetX;
	var m_chartOffsetY;
	var m_chartRightMargin;
	var m_chartBottomMargin;
	var m_chartWidth;
	var m_chartHeight;
	var m_chartHorizontalLabelsY;
	var m_chartVerticalLabelsX;
	var m_chartBottomY;
	var m_periodsCount;
	var m_columnWidth;
	var m_maxValue;
	var m_topLimitValue;
	var m_verticalValueStep;
	var m_unitValue;
	
	/**
	 * Constructor
	 */
	function construct(elementId, width, height, labels, values, valueInformation)
	{
		m_canvas = Raphael(elementId, width, height);
		m_width = width;
		m_height = height;
		m_labels = labels;
		m_values = values;
		m_valueInformation = valueInformation;
		
		m_chartOffsetX = 25;
		m_chartOffsetY = 5;
		m_chartRightMargin = 15;
		m_chartBottomMargin = 30;
		m_chartWidth = m_width - m_chartOffsetX - m_chartRightMargin;
		m_chartHeight = m_height - m_chartOffsetY - m_chartBottomMargin;
		m_chartHorizontalLabelsY = m_height - m_chartBottomMargin + 10;
		m_chartVerticalLabelsX = m_chartOffsetX - 15;
		m_chartBottomY = m_chartOffsetY + m_chartHeight;
		
		m_periodsCount = m_labels.length - 1;
		m_columnWidth = m_chartWidth / m_periodsCount;
		
		m_maxValue = Math.max.apply(null, m_values);
		m_topLimitValue = m_maxValue + Math.round(m_maxValue / 10);
		m_verticalValueStep = Math.round(m_maxValue / 6);
		m_unitValue = m_chartHeight / m_topLimitValue;
		
		return this;
	}

	/**
	 * Draw the rectangular border of the chart
	 */
	function drawBorder(strokeColor, fillColor)
	{
		strokeColor = strokeColor || '#000000';
		fillColor = fillColor || '#ffffff';
		
		var path =
		[
			'M', m_chartOffsetX, m_chartOffsetY,
			'H', m_chartOffsetX + m_chartWidth,
			'V', m_chartOffsetY + m_chartHeight,
			'H', m_chartOffsetX,
			'V', m_chartOffsetY,
		];
		
		return m_canvas
			.path(path.join(','))
			.attr({stroke: strokeColor, fill: fillColor, 'fill-opacity': 0.5})
			.transform('t0.5,0.5');
	}

	/**
	 * Draw the vertical guides above each horizontal label
	 */
	function drawVerticalGuides(color)
	{
		color = color || '#000000';
		
		var path = [];
		
		for(var i = 0; i < m_periodsCount; ++i)
		{
			path = path.concat(
				[
					'M', Math.round(m_chartOffsetX + i * m_columnWidth), m_chartOffsetY,
					'V', m_chartOffsetY + m_chartHeight
				]);
		}
		
		return m_canvas
			.path(path.join(',')).attr({stroke: color, 'stroke-width': 1, opacity: 0.2})
			.transform('t0.5,0.5');
	}

	/**
	 * Draw the horizontal labels
	 * Horizontal labels show the sprint, for which the related bar is.
	 * If the horizontal labels can not be placed on a single line, they
	 * appear on two lines and every other label is drawn on the second line.
	 */
	function drawHorizontalLabels(color)
	{
		color = color || '#000000';
		
		var textWidth = 0;
		var textHeight = 0;
		
		if(m_labels.length > 0)
		{
			var dummyText = m_canvas.text(10, 10, m_labels[0]);
			var textBBox = dummyText.getBBox();
			dummyText.hide();
			
			textWidth = textBBox.width;
			textHeight = textBBox.height;
		}
		
		var labelsOnTwoLines = false;
		if(m_columnWidth < textWidth)
		{
			labelsOnTwoLines = true;
		}
		
		var labelObjects = [];
		
		for(var i = 0; i < m_labels.length; ++i)
		{
			var textX = Math.round(m_chartOffsetX + i * m_columnWidth);
			var textY = m_chartHorizontalLabelsY;
			
			if(labelsOnTwoLines && i % 2 == 1)
			{
				textY += textHeight + 2;
			}
			
			var label = m_canvas
				.text(textX, textY, m_labels[i])
				.attr({color: color});
				
			labelObjects.push(label);
		}
		
		return labelObjects;
	}
	
	/**
	 * Draw the vertical labels of the chart
	 * They display the story points. Not every story point may have its value on
	 * the vertical axis. Values are displayed using a step so that they do not
	 * become too much.
	 */
	function drawVerticalLabels(color)
	{
		color = color || '#000000';
		
		var labelObjects = [];
		
		for(var i = 0; i < m_maxValue; i += m_verticalValueStep)
		{
			if(i === 0)
			{
				continue;
			}
			
			var textY = Math.round(m_chartBottomY - i * m_unitValue);
			labelObjects.push(m_canvas.text(m_chartVerticalLabelsX, textY, i));
		}
		
		var maxValueY = Math.round(m_chartBottomY - m_maxValue * m_unitValue);
		labelObjects.push(m_canvas.text(m_chartVerticalLabelsX, maxValueY, m_maxValue));
		
		return labelObjects;
	}

	/**
	 * Draw the perfect line for the sprint
	 * The perfect line spans from the top left starting value to the 0 value at bottom right.
	 * It shows what the sprint chart should look like at its best.
	 * The closer the value line is to the perfect line is, the better.
	 */
	function drawGuideLine(strokeColor, fillColor)
	{
		strokeColor = strokeColor || '#000000';
		fillColor = fillColor || '#ffffff';
		
		var path =
		[
			'M', m_chartOffsetX, m_chartOffsetY + m_chartHeight,
			'L', m_chartOffsetX, (m_chartOffsetY + m_chartHeight) - m_maxValue * m_unitValue,
			'L', m_chartOffsetX + m_chartWidth, m_chartOffsetY + m_chartHeight,
			'z',
		];
		
		var attributes =
		{
			'stroke': strokeColor,
			'stroke-width': 2,
			'stroke-opacity': 0.5,
			'fill': fillColor,
			'fill-opacity': 0.5,
		};
		
		return m_canvas
			.path(path.join(','))
			.attr(attributes)
			.transform('t0.5,0.5');
	}

	/**
	 * Draw the value line
	 */
	function drawValueLine(strokeColor, fillColor)
	{
		strokeColor = strokeColor || '#000000';
		fillColor = fillColor || '#ffffff';
		
		if(m_values.length < 2)
		{
			return null;
		}
		
		var path = ['M', m_chartOffsetX, m_chartOffsetY + m_chartHeight];
		
		for(var i = 0; i < m_values.length; ++i)
		{
			var pointX = Math.round(m_chartOffsetX + i * m_columnWidth);
			var pointY = Math.round((m_chartOffsetY + m_chartHeight) - m_values[i] * m_unitValue);
			path = path.concat(
				[
					'L',
					pointX,
					pointY,
				]);
		}
		
		path = path.concat(['V', m_chartOffsetY + m_chartHeight, 'z']);
		
		var attributes =
		{
			'stroke': strokeColor,
			'stroke-width': 3,
			'stroke-opacity': 0.5,
			'fill': fillColor,
			'fill-opacity': 0.4,
		};
		return m_canvas
			.path(path.join(','))
			.attr(attributes)
			.transform('t0.5,0.5');
	}

	/**
	 * Draw a popup that holds a point's value
	 * The popup is rectangular with text in it. It is moved beside a value point
	 * when hovering over a value point on the line.
	 */
	function drawPopup(x, y, text, strokeColor, fillColor)
	{
		strokeColor = strokeColor || '#000000';
		fillColor = fillColor || '#ffffff';
		
		var textAttributes =
		{
			'color': strokeColor,
		};
		
		var borderAttributes =
		{
			'stroke': strokeColor,
			'stroke-linecap': 'round',
			'stroke-width': 2,
			'stroke-opacity': 0.5,
			'fill': fillColor,
			'fill-opacity': 0.8,
		};
		
		var textOffsetX = 20;
		var textOffsetY = 10;
		
		var textTemporary = m_canvas.text(x, y, text).attr(textAttributes);
		var textBBox = textTemporary.getBBox();
		textTemporary.hide();
		
		var borderWidth = textBBox.width + textOffsetX;
		var borderHeight = textBBox.height + textOffsetY;
		
		var path =
		[
			'M', x, y,
			'H', x + borderWidth,
			'V', y + borderHeight,
			'H', x,
			'V', y,
		];
		
		var border = m_canvas.path(path.join(',')).attr(borderAttributes);
		
		var text = m_canvas.text(x, y, text).attr(textAttributes);
		text.attr(
		{
			'x': textBBox.x + textBBox.width + textOffsetY,
			'y': textBBox.y + textBBox.height + textOffsetY / 2,
		});
		
		var popup = m_canvas.set();
		popup.push(border);
		popup.push(text);
		
		return popup;
	}

	/**
	 * Draw the points on the value line
	 * The points are small circles at each value point. Hovering over
	 * a circle will show a popup besides the circle that shows the value.
	 */
	function drawValuePoints(strokeColor, fillColor)
	{
		strokeColor = strokeColor || '#000000';
		fillColor = fillColor || '#ffffff';
		
		var valueObjects = [];
		
		var normalAttributes =
		{
			'r': 4,
			'stroke': strokeColor,
			'stroke-width': 1.2,
			'fill': fillColor,
			'fill-opacity': 0.4,
		};
		
		var selectedAttributes =
		{
			'r': 6,
			'stroke-width': 1.4,
			'fill-opacity': 0.6,
		};
		
		var popup = drawPopup(0, 0, 'empty information');
		popup.hide();
		
		for(var i = 0; i < m_values.length; ++i)
		{
			var val = m_values[i];
			var valueText = m_valueInformation[i] || val;
			
			var centreX = Math.round(m_chartOffsetX + i * m_columnWidth);
			var centreY = Math.round((m_chartOffsetY + m_chartHeight) - val * m_unitValue);
			
			var circle = m_canvas.circle(centreX, centreY, 4).attr(normalAttributes);
				
			valueObjects.push(circle);
			
			// Register event for hovering over the point.
			// A popup will appear that will show the value.
			(function(circle, popup, valueText)
			{
				circle.hover(
					function()
					{
						circle.attr(selectedAttributes);
						var bbox = circle.getBBox();
						
						popup.attr({text: valueText});
						var popupBBox = popup.getBBox();
						
						// Calculate whether the popup will be shown on the right or
						// on the left of the point.
						var popupX = bbox.x + bbox.width + 2;
						var popupY = bbox.y + bbox.height / 2 - popupBBox.height / 2;
						
						if(popupX + popupBBox.width > m_width)
						{
							popupX = bbox.x - popupBBox.width - 4;
							popupY = bbox.y + bbox.height / 2 - popupBBox.height / 2;
						}
						
						popup.transform('T' + (popupX + 0.5) + ',' + (popupY + 0.5));
						popup.toFront();
						
						popup.show();
					},
					function()
					{
						circle.attr(normalAttributes);
						
						popup.transform('');
						popup.hide();
					}
					);
			})(circle, popup, valueText);
		}
		
		return valueObjects;
	}

	/**
	 * Draw the chart
	 * Combines all the other drawing methods to draw the entire chart.
	 */
	this.drawChart = function()
	{
		drawBorder('#999999', '#fef963');
		drawVerticalGuides();
		drawHorizontalLabels();
		drawVerticalLabels();
		drawGuideLine('#4ec640', '#4ec640');
		drawValueLine('#13a9af', '#13a9af');
		drawValuePoints('#3366bb', '#5555ff');
		
		// Other colors
		/*
		drawBorder(chartOffsetX, chartOffsetY, chartWidth, chartHeight, '#ffc500', '#ffc500');
		drawVerticalGuides(chartOffsetX, chartOffsetY, chartWidth, chartHeight, periodsCount);
		drawHorizontalLabels(chartOffsetX, chartHorizontalLabelsY, chartWidth);
		drawVerticalLabels(chartVerticalLabelsX, chartOffsetY, chartHeight, maxValue, topLimitValue, Math.round(maxValue / 6));
		drawGuideLine(chartOffsetX, chartOffsetY, chartWidth, chartHeight, maxValue, topLimitValue, '#ff397f', '#ff397f');
		drawValueLine(chartOffsetX, chartOffsetY, chartWidth, chartHeight, periodsCount, topLimitValue, '#a30496', '#a30496');
		drawValuePoints(chartOffsetX, chartOffsetY, chartWidth, chartHeight, periodsCount, topLimitValue, '#3366bb', '#5555ff');
		*/
		
		/*
		drawBorder(chartOffsetX, chartOffsetY, chartWidth, chartHeight, '#999999', '#a0ffa7');
		drawVerticalGuides(chartOffsetX, chartOffsetY, chartWidth, chartHeight, periodsCount);
		drawHorizontalLabels(chartOffsetX, chartHorizontalLabelsY, chartWidth);
		drawVerticalLabels(chartVerticalLabelsX, chartOffsetY, chartHeight, maxValue, topLimitValue, Math.round(maxValue / 6));
		drawGuideLine(chartOffsetX, chartOffsetY, chartWidth, chartHeight, maxValue, topLimitValue, '#045510', '#045510');
		drawValueLine(chartOffsetX, chartOffsetY, chartWidth, chartHeight, periodsCount, topLimitValue, '#0d005f', '#0d005f');
		drawValuePoints(chartOffsetX, chartOffsetY, chartWidth, chartHeight, periodsCount, topLimitValue, '#aac500', '#ffc800');
		*/
		
		/*
		drawBorder(chartOffsetX, chartOffsetY, chartWidth, chartHeight, '#999999', '#000b44');
		drawVerticalGuides(chartOffsetX, chartOffsetY, chartWidth, chartHeight, periodsCount);
		drawHorizontalLabels(chartOffsetX, chartHorizontalLabelsY, chartWidth);
		drawVerticalLabels(chartVerticalLabelsX, chartOffsetY, chartHeight, maxValue, topLimitValue, Math.round(maxValue / 6));
		drawGuideLine(chartOffsetX, chartOffsetY, chartWidth, chartHeight, maxValue, topLimitValue, '#75e930', '#75e930');
		drawValueLine(chartOffsetX, chartOffsetY, chartWidth, chartHeight, periodsCount, topLimitValue, '#f4da00', '#f4da00');
		drawValuePoints(chartOffsetX, chartOffsetY, chartWidth, chartHeight, periodsCount, topLimitValue, '#33ccaa', '#ffc800');
		*/
	};
	
	return construct(elementId, width, height, labels, values, valueInformation);
}
