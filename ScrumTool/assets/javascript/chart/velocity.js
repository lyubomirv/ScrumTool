/*!
 * Burn-down chart drawing
 * 
 * Depends:
 *	raphael.js
 *
 * @author Lyubomir Vasilev <lyubomirv@gmail.com>
 * @copyright Copyright &copy; 2012
 */

/**
 * @param elementId string id of the html element that will hold the chart
 * @param width int width of the chart
 * @param height int height of the chart
 * @param labels array(string) the horizontal labels for the chart
 * @param values array(string) the values for the chart
 */
VelocityChart = function(elementId, width, height, labels, values)
{
	// Private members
	var m_canvas = null;
	var m_width = null;
	var m_height = null;
	var m_labels = null;
	var m_values = null;
	
	var m_chartOffsetX;
	var m_chartOffsetY;
	var m_chartRightMargin;
	var m_chartBottomMargin;
	var m_chartWidth;
	var m_chartHeight;
	var m_chartHorizontalLabelsY;
	var m_chartVerticalLabelsX;
	var m_chartBottomY;
	var m_periodsCount;
	var m_columnWidth;
	var m_barWidth;
	var m_barCenter;
	var m_maxValue;
	var m_topLimitValue;
	var m_meanValue;
	var m_verticalValueStep;
	var m_unitValue;
	
	/**
	 * Constructor
	 */
	function construct(elementId, width, height, labels, values)
	{
		m_canvas = Raphael(elementId, width, height);
		m_width = width;
		m_height = height;
		m_labels = labels;
		m_values = values;
		
		m_chartOffsetX = 25;
		m_chartOffsetY = 5;
		m_chartRightMargin = 15;
		m_chartBottomMargin = 30;
		m_chartWidth = m_width - m_chartOffsetX - m_chartRightMargin;
		m_chartHeight = m_height - m_chartOffsetY - m_chartBottomMargin;
		m_chartHorizontalLabelsY = m_height - m_chartBottomMargin + 10;
		m_chartVerticalLabelsX = m_chartOffsetX - 15;
		m_chartBottomY = m_chartOffsetY + m_chartHeight;
		
		m_periodsCount = Math.max(m_labels.length, m_values.length);
		m_columnWidth = m_chartWidth / (m_periodsCount);
		m_barWidth = m_columnWidth * 0.75;
		m_barCenter = m_columnWidth * 0.5;
		
		m_maxValue = Math.max.apply(null, m_values);
		m_topLimitValue = m_maxValue + 10;
		m_meanValue = getMeanValue();
		m_verticalValueStep = Math.round(m_maxValue / 6) + 1;
		
		m_unitValue = m_chartHeight / m_topLimitValue;
		
		return this;
	}
	
	/**
	 * Get the mean of the values
	 * @return int mean value or -1 in case of insufficient data
	 */
	function getMeanValue()
	{
		if(m_values.length < 2)
		{
			return -1;
		}
		
		var sum = 0;
		for(var i = 0; i < m_values.length; ++i)
		{
			sum += m_values[i] * 1;	// Multiplication keeps it an int
		}
		
		return meanValue = Math.round(sum / m_values.length);
	}
	
	/**
	 * Draw the rectangular border of the chart
	 */
	function drawBorder(strokeColor, fillColor)
	{
		strokeColor = strokeColor || '#000000';
		fillColor = fillColor || '#000000';
		
		var path =
		[
			'M', m_chartOffsetX, m_chartOffsetY,
			'H', m_chartOffsetX + m_chartWidth,
			'V', m_chartOffsetY + m_chartHeight,
			'H', m_chartOffsetX,
			'V', m_chartOffsetY,
		];
		return m_canvas
			.path(path.join(','))
			.attr({'stroke': strokeColor, 'fill': fillColor, 'fill-opacity': 0.5})
			.transform('t0.5,0.5');
	}
	
	/**
	 * Draw the vertical labels of the chart
	 * They display the story points. Not every story point may have its value on
	 * the vertical axis. Values are displayed using a step so that they do not
	 * become too much.
	 */
	function drawVerticalLabels(color)
	{
		color = color || '#000000';
		
		var labelObjects = [];
		
		for(var i = 0; i < m_topLimitValue; i += m_verticalValueStep)
		{
			if(i === 0)
			{
				continue;
			}
			
			var textY = Math.round(m_chartBottomY - i * m_unitValue);
			labelObjects.push(m_canvas.text(m_chartVerticalLabelsX, textY, i));
		}
		
		return labelObjects;
	}
	
	/**
	 * Draw horizontal guides
	 * Horizontal guides are at the level of each story point on the vertical
	 * axis and span the whole chart width.
	 */
	function drawHorizontalGuides(color)
	{
		color = color || '#000000';
		
		var path = [];
		
		for(var i = 0; i < m_topLimitValue; i += m_verticalValueStep)
		{
			if(i === 0)
			{
				continue;
			}
			
			var lineY = Math.round(m_chartBottomY - i * m_unitValue);
			
			path = path.concat(
				[
					'M', m_chartOffsetX, lineY,
					'H', m_chartOffsetX + m_chartWidth,
				]);
		}
		
		return m_canvas
			.path(path.join(','))
			.attr({'stroke': color, 'opacity': 0.2})
			.transform('t0.5,0.5');
	}
	
	/**
	 * Draw horizontal labels
	 * Horizontal labels show the sprint, for which the related bar is.
	 * If the horizontal labels can not be placed on a single line, they
	 * appear on two lines and every other label is drawn on the second line.
	 */
	function drawHorizontalLabels(color)
	{
		color = color || '#000000';
		
		var textWidth = 0;
		var textHeight = 0;
		if(m_labels.length > 0)
		{
			var longestLabelText = "";
			for(var i = 0; i < m_labels.length; ++i)
			{
				if(longestLabelText.length < m_labels[i].length)
				{
					longestLabelText = m_labels[i];
				}
			}
			
			var dummyText = m_canvas.text(10, 10, longestLabelText);
			var textBBox = dummyText.getBBox();
			dummyText.hide();
			
			textWidth = textBBox.width;
			textHeight = textBBox.height;
		}
		
		var labelsOnTwoLines = false;
		if(m_columnWidth < textWidth)
		{
			labelsOnTwoLines = true;
		}
		
		var labelObjects = [];
		
		for(var i = 0; i < m_labels.length; ++i)
		{
			var textX = Math.round(m_chartOffsetX + i * m_columnWidth + m_barCenter);
			var textY = m_chartHorizontalLabelsY;
			
			if(labelsOnTwoLines && i % 2 == 1)
			{
				textY += textHeight + 2;
			}
			
			var label = m_canvas
				.text(textX, textY, m_labels[i])
				.attr({'color': color});
			
			labelObjects.push(label);
		}
		
		return labelObjects;
	}
	
	/**
	 * Interpolates an integer value
	 * @param begin int the begin value of the interval
	 * @param end int the end value of the interval
	 * @param step int step at which the desired value is
	 * @param maxSteps int total number of steps for the interval
	 * @return int interpolated value
	 */
	function interpolate(begin, end, step, maxSteps)
	{
		if(begin < end)
		{
			return (end - begin) * (step / maxSteps) + begin;
		}
		else
		{
			return (begin - end) * (1 - step / maxSteps) + end;
		}
	}
	
	/**
	 * Gets an interpolated color at a specific interpolation step
	 * @param from int(hex) starting color, expressed as a hex integer
	 * @param to int(hex) ending color, expressed as a hex integer
	 * @param step int step at which the desired color is
	 * @param maxSteps int total number of steps for the interval
	 * @return string the interpolated color,
	 * in the form: '#xxxxxx', where x is a hexadecimal number
	 */
	function getGradientColor(from, to, step, maxSteps)
	{
		var r0 = (from & 0xff0000) >> 16;
		var g0 = (from & 0x00ff00) >> 8;
		var b0 = (from & 0x0000ff) >> 0;
		
		var r1 = (to & 0xff0000) >> 16;
		var g1 = (to & 0x00ff00) >> 8;
		var b1 = (to & 0x0000ff) >> 0;
		
		var r = interpolate(r0, r1, step, maxSteps);
		var g = interpolate(g0, g1, step, maxSteps);
		var b = interpolate(b0, b1, step, maxSteps);
		
		var result = (((r << 8) | g) << 8) | b;
		
		return '#' + result.toString(16);
	}
	
	/**
	 * Draw the bar values
	 * Each bar will be filled with a color that is decided by the method.
	 * The color will be taken from a red-yellow gradinent, if the value is
	 * lower than the mean value, and from a yellow-green gradient, if the
	 * value is greater than the mean value.
	 */
	function drawBars(strokeColor)
	{
		strokeColor = strokeColor || '#000000';
		
		var barOffsetFromX = (m_columnWidth * 0.25) / 2;
		var maxGradientSteps = 10;
		
		var barObjects = [];
		
		for(var i = 0; i < m_values.length; ++i)
		{
			var val = m_values[i];
			
			var barX = Math.round(m_chartOffsetX + i * m_columnWidth + barOffsetFromX);
			var barY = Math.round(m_chartBottomY - val * m_unitValue);
			
			var fillColor;
			if(val <= m_meanValue)
			{
				var gradientStep = (val / m_meanValue) * maxGradientSteps;
				fillColor = getGradientColor(0xff4400, 0xffff00, gradientStep, maxGradientSteps);
			}
			else
			{
				var gradientStep = ((val - m_meanValue) / (m_topLimitValue - m_meanValue)) * maxGradientSteps;
				fillColor = getGradientColor(0xffff00, 0x44ff00, gradientStep, maxGradientSteps);
			}
			
			var path =
			[
				'M', barX, barY,
				'H', barX + m_barWidth,
				'V', m_chartBottomY,
				'H', barX,
				'V', barY,
			];
			
			var bar = m_canvas
				.path(path.join(','))
				.attr({'stroke': strokeColor, 'stroke-opacity': 0.9, 'fill': fillColor, 'fill-opacity': 0.9})
				.transform('t0.5,0.5');
			
			barObjects.push(bar);
		}
		
		return barObjects;
	}
	
	/**
	 * Draw the bar values
	 * Each bar value will be drawn inside the bar just below its higest end, slightly
	 * rotated. If the bar is too small, the value will be drawn just above it.
	 */
	function drawBarValues(color)
	{
		color = color || '#000000';
		
		var barValueObjects = [];
		
		for(var i = 0; i < m_values.length; ++i)
		{
			var val = m_values[i];
			
			var barY = Math.round(m_chartBottomY - val * m_unitValue);
			var textX = Math.round(m_chartOffsetX + i * m_columnWidth + m_barCenter);
			var textY = 0;
			
			if(val < m_maxValue * 0.05)
			{
				textY = barY - 10;
			}
			else
			{
				textY = barY + 10;
			}
			
			var barValue = m_canvas
				.text(textX, textY, val)
				.attr({'stroke': color, 'stroke-width': 0.1})
				.transform('r-21');
			barValueObjects.push(barValue);
		}
		
		return barValueObjects;
	}
	
	/**
	 * Draw the mean line and its value
	 * The mean value line is horizontal line at the level of the mean value,
	 * that spans the entire width of the chart, at a little more on the right.
	 * Its value is on the right of the chart, just above the line.
	 */
	function drawMeanLineValue(strokeColor, textColor)
	{
		strokeColor = strokeColor || '#000000';
		
		if(m_meanValue < 0)
		{
			return;
		}
		
		var chartRightX = m_chartOffsetX + m_chartWidth;
		var meanValueLineY = Math.round(m_chartBottomY - m_meanValue * m_unitValue);
		
		var path =
		[
			'M', m_chartOffsetX - 1, meanValueLineY,
			'H', chartRightX + m_chartRightMargin,
		];
		
		m_canvas
			.path(path.join(','))
			.attr({'stroke': strokeColor, 'stroke-width': 2, 'opacity': 0.6})
			.transform('t0.5,0.5');
		
		var textX = chartRightX + m_chartRightMargin / 2;
		var textY = meanValueLineY - 10;
		
		m_canvas
			.text(textX, textY, m_meanValue)
			.attr({'stroke': textColor, 'font-size': 12});
	}
	
	/**
	 * Draw the chart
	 * Combines all the other drawing methods to draw the entire chart.
	 */
	this.drawChart = function()
	{
		drawBorder('#999999', '#fef963');
		drawVerticalLabels();
		drawHorizontalGuides();
		drawHorizontalLabels();
		drawBars('#555555');
		drawBarValues();
		drawMeanLineValue('#127dec');
	}
	
	return construct(elementId, width, height, labels, values);
}
