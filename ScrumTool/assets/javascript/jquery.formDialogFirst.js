/*!
 * jQuery formDialog
 * A jQuery-ui dialog that holds a form.
 * The dialog' contents are filled from the server by an ajax request when the dialog is opened.
 * The dialog has a button that performs the submitting of the form - again by using ajax.
 * 
 * Depends:
 *	jquery.ui.dialog.js
 *
 * @author Lyubomir Vasilev <lyubomirv@gmail.com>
 * @copyright Copyright &copy; 2011
 */
 
;(function($)
{
	var methods =
	{
		/**
		 * init method
		 * Creates a jQuery-ui dialog.
		 * Attaches a handler to the click event that opens the dialog.
		 * Attaches a handler to the clicking on the overlay when the
		 * dialog is open, that will close the dialog.
		 */
		init: function(options)
		{
			settings = $.extend({}, $.fn.formDialog.defaults, options || {});
			
			return this.each(function()
			{
				var $this = $(this);
				var data = $this.data('formDialog');
				
				if(!data)
				{
					var formDialog = $('<div />')
						.html('')
						.dialog(
						{
							autoOpen: false,
							modal: true,
							title: settings.title,
							buttons: [
								{
									text: settings.submitButtonText,
									click: function()
									{
										// Get the form parameters
										var form = $(this).find('form');
										var formValues = form.serialize();
										
										if(settings.actionSubmit === undefined
											|| settings.contentUrl === undefined)
										{
											return;
										}
										
										// Add the needed by the server 'ajax' parameter
										formValues += '&' + settings.ajaxVar + '=' + settings.actionSubmit;
										
										var button = getDialogButton(formDialog, settings.submitButtonText);
										button.attr('disabled', 'disabled');
										
										// Send the request to the server
										$.ajax(
										{
											type: 'POST',
											url: settings.contentUrl,
											data: formValues,
											dataType: 'json',
											context: formDialog,
											success: function(data)
											{
												if(data.status === 'ok')
												{
													formDialog.dialog('close');
													if(settings.submitSuccessCallback !== undefined)
													{
														settings.submitSuccessCallback(data);
													}
												}
												else
												{
													updateErrors(formDialog, data);
												}
												
												button.removeAttr('disabled');
											}
										});
										
										// Debug
										/*
										request.done(function(msg) {
											$("#log").html(msg);
										});

										request.fail(function(jqXHR, textStatus) {
											alert("Request failed: " + textStatus);
										});
										*/
									}
								}
							]
						});
					
					// Set the data on this.
					$this.data('formDialog',
					{
						target: $this,
						formDialog: formDialog
					});
					
					$(this).live('click', function()
					{
						methods.open.apply($this);
					});
					
					// Close the dialog on mouse click outside it
					$('.ui-widget-overlay').live('click', function()
					{
						formDialog.dialog('close');
					});
				}
			});
		},
		
		/**
		 * open method
		 * Opens the dialog and calls the server for filling the form's html
		 */
		open: function()
		{
			var $this = this;
			
			var data = $this.data('formDialog');
			if(!data)
			{
				return;
			}
			
			var formDialog = data.formDialog;
			
			if(settings.contentUrl === undefined
				|| settings.actionRender === undefined)
			{
				return;
			}
			
			formDialog.dialog('open');
			$.ajax(
			{
				type: 'POST',
				url: settings.contentUrl,
				data: settings.ajaxVar + '=' + settings.actionRender,
				success: function(html)
				{
					formDialog.html(html);
				}
			});
		},
		
		/**
		 * destroy method
		 * Destroys the dialog
		 */
		destroy: function()
		{
			return this.each(function()
			{
				var $this = $(this);
				//var $this = this;
				
				var data = $this.data('formDialog');
				if(!data)
				{
					return;
				}
				
				var formDialog = data.formDialog;
				formDialog.dialog('destroy');
			});
		},
	};
	
	/**
	 * formDialog
	 */
	$.fn.formDialog = function(method)
	{	
		if(methods[method])
		{
			return methods[method].apply(this, Array.prototype.slice.call(arguments));
		}
		else if(typeof method === 'object' || !method)
		{
			return methods.init.apply(this, arguments);
		}
		else
		{
			$.error('Method ' + method + ' does not exist');
		}
	}
	
	/**
	 * Get a jQuery button from a dialog
	 * @param dialog object jQuery-ui dialog - the ui-dialog-content part of it
	 * @param buttonName string Name of the button that is searched for
	 */
	var getDialogButton = function(dialog, buttonName)
	{
		var parent = dialog.parent();
		var buttonPane = parent.children('.ui-dialog-buttonpane');
		var buttonSet = buttonPane.children('.ui-dialog-buttonset');
		var buttons = buttonSet.children('button');
		for(var i = 0; i < buttons.length; ++i)
		{
			var button = $(buttons[i]);
			if(button.text() === buttonName)
			{
				return button;
			}
		}

		return null;
	}
	
	/**
	 * Update the error messages in the form
	 * Error message fields must have the '.errorMessage' class
	 * @param dialog object The dialog containing the form
	 * @param data object JSON data, as sent by the server
	 * JSON is in the form:
	 * <id of field>: [ list of errors ]
	 */
	var updateErrors = function(dialog, data)
	{
		var errorsObj = data;
		if(typeof errorsObj === 'object')
		{
			// Iterate the properties - they are the ids of
			// the input fields
			for(var property in errorsObj)
			{
				var id = property;
				
				// Error messages are array. Show only the first one.
				var errorMessage = errorsObj[property][0];
				
				var inputField = dialog.find('#' + id);
				if(inputField !== null)
				{
					// It is assumed that the Error message field will be
					// besides the input field
					var errorMessageDiv = inputField.next('.errorMessage');
					if(errorMessageDiv !== null)
					{
						// Make it visible
						errorMessageDiv.removeAttr('style');
						errorMessageDiv.html(errorMessage);
					}
				}
			}
		}
	}
	
	var settings = undefined;
	
	/**
	 * Default settings of the form dialog
	 * @param ajaxVar string ajax variable
	 * @param contentUrl string url of the form content
	 * @param actionRender string action identifier for the render action
	 * @param actionSubmit string action identifier for the submit action
	 * @param submitSuccessCallback function callback on submit action success.
	 * Must be in the form: function(data), where data is an object.
	 * @param title string title of the dialog
	 */
	$.fn.formDialog.defaults = {
		ajaxVar: 'ajax',
		contentUrl: undefined,
		actionRender: undefined,
		actionSubmit: undefined,
		submitSuccessCallback: undefined,
		title: '',
		submitButtonText: 'Ok'
	};
})(jQuery);
