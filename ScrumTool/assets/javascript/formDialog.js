/*!
 * jQuery formDialog
 * A jQuery-ui dialog that holds a form.
 * The dialog' contents are filled from the server by an ajax request when the dialog is opened.
 * The dialog has a button that performs the submitting of the form - again by using ajax.
 * 
 * Depends:
 *	jquery.ui.dialog.js
 *
 * @author Lyubomir Vasilev <lyubomirv@gmail.com>
 * @copyright Copyright &copy; 2012
 */

// It is required that the dialog's element is
// created only once. This is the Form Dialog.
var GLOBAL_FORM_DIALOG = jQuery('<div />');
 
function createFormDialog(settings)
{
	/**
	 * Default settings of the form dialog
	 * @param ajaxVar string ajax variable
	 * @param contentUrl string url of the form content
	 * @param actionRender string action identifier for the render action
	 * @param actionSubmit string action identifier for the submit action
	 * @param submitSuccessCallback function callback on submit action success.
	 * Must be in the form: function(data), where data is an object.
	 * @param title string title of the dialog
	 */
	var defaults = {
		ajaxVar: 'ajax',
		additionalData: {},
		contentUrl: undefined,
		actionRender: undefined,
		actionSubmit: undefined,
		submitSuccessCallback: undefined,
		title: '',
		submitButtonText: 'Ok'
	};
	
	/**
	 * Get a jQuery button from a dialog
	 * @param dialog object jQuery-ui dialog - the ui-dialog-content part of it
	 * @param buttonName string Name of the button that is searched for
	 */
	var getDialogButton = function(dialog, buttonName)
	{
		var parent = dialog.parent();
		var buttonPane = parent.children('.ui-dialog-buttonpane');
		var buttonSet = buttonPane.children('.ui-dialog-buttonset');
		var buttons = buttonSet.children('button');
		for(var i = 0; i < buttons.length; ++i)
		{
			var button = $(buttons[i]);
			if(button.text() === buttonName)
			{
				return button;
			}
		}

		return null;
	}
	
	/**
	 * Update the error messages in the form
	 * Error message fields must have the '.errorMessage' class
	 * @param dialog object The dialog containing the form
	 * @param data object JSON data, as sent by the server
	 * JSON is in the form:
	 * <id of field>: [ list of errors ]
	 */
	var updateErrors = function(dialog, data)
	{
		var errorsObj = data;
		if(typeof errorsObj === 'object')
		{
			// Iterate the properties - they are the ids of
			// the input fields
			for(var property in errorsObj)
			{
				var id = property;
				
				// Error messages are array. Show only the first one.
				var errorMessage = errorsObj[property][0];
				
				var inputField = dialog.find('#' + id);
				if(inputField !== null)
				{
					// It is assumed that the Error message field will be
					// besides the input field
					var errorMessageDiv = inputField.next('.errorMessage');
					if(errorMessageDiv !== null)
					{
						// Make it visible
						errorMessageDiv.removeAttr('style');
						errorMessageDiv.html(errorMessage);
					}
				}
			}
		}
	}
	
	var getFormValuesAsJson = function(form)
	{
		var formId = form.attr('id');
		
		var inputs = jQuery('form#' + formId + ' :input');
		var inputsObject = {};
		jQuery.map(inputs, function(element, i)
		{
			var input = jQuery(element);
			
			if(!input.attr('disabled'))
			{
				// Checkboxes and radio buttons must be
				// checked, in order to take their value.
				// Otherwise they are always added to the request.
				if(input.attr('type') === 'checkbox' ||
					input.attr('type') === 'radio')
				{
					if(!input.attr('checked'))
					{
						return;
					}
				}
				
				inputsObject[element.name] = input.val();
			}
		});
		
		return inputsObject;
	}
	
	var formDialog = GLOBAL_FORM_DIALOG;
	
	var methods =
	{
		/**
		 * init method
		 * Creates a jQuery-ui dialog.
		 * Attaches a handler to the click event that opens the dialog.
		 * Attaches a handler to the clicking on the overlay when the
		 * dialog is open, that will close the dialog.
		 */
		init: function(options)
		{
			settings = jQuery.extend({}, defaults, options || {});
			
			formDialog
				.html('')
				.dialog(
				{
					autoOpen: false,
					modal: true,
					title: settings.title,
					width: 800,
					minHeight: 500,
					position: ['center', 50],
					draggable: false,
					resizable: false,
					buttons: [
						{
							text: settings.submitButtonText,
							click: function()
							{
								// Get the form parameters
								if(settings.actionSubmit === undefined
									|| settings.contentUrl === undefined)
								{
									return;
								}
								
								var form = formDialog.find('form');
								
								var submitData = getFormValuesAsJson(form);
								submitData[settings.ajaxVar] = settings.actionSubmit;
								jQuery.extend(submitData, settings.additionalData);
								
								var button = getDialogButton(formDialog, settings.submitButtonText);
								button.attr('disabled', 'disabled');
								
								// Send the request to the server
								$.ajax(
								{
									type: 'POST',
									url: settings.contentUrl,
									data: submitData,
									dataType: 'json',
									context: formDialog,
									success: function(data)
									{
										if(data.status === 'ok')
										{
											formDialog.dialog('close');
											if(settings.submitSuccessCallback !== undefined)
											{
												settings.submitSuccessCallback(data);
											}
										}
										else
										{
											updateErrors(formDialog, data);
										}
										
										button.removeAttr('disabled');
									}
								});
								
								// Debug
								/*
								request.done(function(msg) {
									$("#log").html(msg);
								});

								request.fail(function(jqXHR, textStatus) {
									alert("Request failed: " + textStatus);
								});
								*/
							}
						}
					]
				});
		},
		
		/**
		 * open method
		 * Opens the dialog and calls the server for filling the form's html
		 */
		open: function()
		{
			if(settings.contentUrl === undefined
				|| settings.actionRender === undefined)
			{
				return;
			}
			
			var openData = {};
			openData[settings.ajaxVar] = settings.actionRender;
			
			jQuery.extend(openData, settings.additionalData);
			
			formDialog.dialog('open');
			$.ajax(
			{
				type: 'POST',
				url: settings.contentUrl,
				data: openData,
				success: function(html)
				{
					formDialog.html(html);
				}
			});
		},
	};
	
	methods.init(settings);
	methods.open();
	
	return false;
}
 
// Close the dialog on mouse click outside it
$('.ui-widget-overlay').live('click', function()
{
	GLOBAL_FORM_DIALOG.dialog('close');
});
