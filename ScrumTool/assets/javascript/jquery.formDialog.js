//var GLOBAL_DIALOG = $('<div />');

/**
 * Triggers a destroyed event when an element is removed via the jQuery modifiers
 */
(function()
{
    var oldClean = jQuery.cleanData;

    jQuery.cleanData = function(elems)
	{
        for(var i = 0, elem; (elem = elems[i]) != null; i++)
		{
            jQuery(elem).triggerHandler('destroyed');
            //jQuery.event.remove( elem, 'destroyed' );
        }
        oldClean(elems)
    }
})();

// Creates a Namespace
ScrumTool = {};

// A Basic Constructor function
ScrumTool.formDialog = function(el, options)
{
    // if we don't have arguments, we're inheriting
    if(el)
	{
        this.init(el, options);
    }
}

// Extend the prototype
$.extend(ScrumTool.formDialog.prototype,
{
	/**
	 * Default settings of the form dialog
	 * @param ajaxVar string ajax variable
	 * @param contentUrl string url of the form content
	 * @param actionRender string action identifier for the render action
	 * @param actionSubmit string action identifier for the submit action
	 * @param submitSuccessCallback function callback on submit action success.
	 * Must be in the form: function(data), where data is an object.
	 * @param title string title of the dialog
	 */
	defaults:
	{
		ajaxVar: 'ajax',
		openAttributes: {},
		contentUrl: undefined,
		actionRender: undefined,
		actionSubmit: undefined,
		submitSuccessCallback: undefined,
		title: '',
		submitButtonText: 'Ok'
	},
   
    // the name of the plugin, where it will be saved in jQuery.data
    name: "formDialog",
    
    // Sets up the tabs widget
    init: function(el, options)
	{
        // save this element for faster queries
        this.element = $(el);
        
        // save options if there are any
        this.settings = jQuery.extend({}, this.defaults, options || {});
        
        // bind if the element is destroyed
        this.element.bind('destroyed', $.proxy(this.teardown, this));
		
		var $this = this;
		
		this.formDialog = $('<div />');
		this.formDialog
			.html('')
			.dialog(
			{
				autoOpen: false,
				modal: true,
				title: this.settings.title,
				buttons: [
					{
						text: this.settings.submitButtonText,
						click: function()
						{
							// Get the form parameters
							var form = $(this).find('form');
							var formValues = form.serialize();
							
							if($this.settings.actionSubmit === undefined
								|| $this.settings.contentUrl === undefined)
							{
								return;
							}
							
							// Add the needed by the server 'ajax' parameter
							formValues += '&' + $this.settings.ajaxVar + '=' + $this.settings.actionSubmit;
							
							var button = $this.getDialogButton($this.formDialog, $this.settings.submitButtonText);
							button.attr('disabled', 'disabled');
							
							// Send the request to the server
							$.ajax(
							{
								type: 'POST',
								url: $this.settings.contentUrl,
								data: formValues,
								dataType: 'json',
								context: $this,
								success: function(data)
								{
									if(data.status === 'ok')
									{
										$this.formDialog.dialog('close');
										if($this.settings.submitSuccessCallback !== undefined)
										{
											$this.settings.submitSuccessCallback(data);
										}
									}
									else
									{
										$this.updateErrors($this.formDialog, data);
									}
									
									button.removeAttr('disabled');
								}
							});
							
							// Debug
							/*
							request.done(function(msg) {
								$("#log").html(msg);
							});

							request.fail(function(jqXHR, textStatus) {
								alert("Request failed: " + textStatus);
							});
							*/
						}
					}
				]
			});
        
        // save this instance in jQuery data
        $.data(el, this.name, this);
		
        // bind event handlers
        this.bind();
    },
	
    // bind events to this instance's methods
    bind: function()
	{
        this.element.delegate(this, 'click', jQuery.proxy(this.open, this));
    },
	
    // call destroy to teardown the controller while leaving the element
    destroy: function()
	{
        this.element.unbind('destroyed', this.teardown);
        this.teardown();
    },
	
    // remove all the functionality of this tabs widget
    teardown: function()
	{
        jQuery.removeData(this.element[0], this.name);
        
        // unbind this widget
        this.unbind();
		
		this.formDialog.dialog('destroy');
        
        // clear references to this element
        this.element = null;
    },
	
    unbind: function()
	{
        this.element.undelegate(this, 'click', this.open);
    },
	
    // on an li click, activates new tab  
    open: function(ev)
	{
        ev.preventDefault();
		
        this.formDialog.dialog('open');
		
		var openData = {};
		openData[this.settings.ajaxVar] = this.settings.actionRender;
		
		for(var paramName in this.settings.openAttributes)
		{
			var attribute = this.settings.openAttributes[paramName];
			openData[paramName] = this.element.attr(attribute);
		}
		
		/*
		var openData =
		{
			this.settings.ajaxVar: this.settings.actionRender
		}
		*/
		//openData = jQuery.extend(openData, this.openAttributes);
		
		var formDialog = this.formDialog;
		$.ajax(
		{
			type: 'POST',
			url: this.settings.contentUrl,
			data: openData,
			context: formDialog,
			success: function(html)
			{
				formDialog.html(html);
			}
		});
    },
	
	close: function(ev)
	{
		ev.preventDefault();
		
		this.formDialog.dialog('close');
	},
	
	/**
	 * Get a jQuery button from a dialog
	 * @param dialog object jQuery-ui dialog - the ui-dialog-content part of it
	 * @param buttonName string Name of the button that is searched for
	 */
	getDialogButton: function(dialog, buttonName)
	{
		var parent = dialog.parent();
		var buttonPane = parent.children('.ui-dialog-buttonpane');
		var buttonSet = buttonPane.children('.ui-dialog-buttonset');
		var buttons = buttonSet.children('button');
		for(var i = 0; i < buttons.length; ++i)
		{
			var button = $(buttons[i]);
			if(button.text() === buttonName)
			{
				return button;
			}
		}

		return null;
	},
	
	/**
	 * Update the error messages in the form
	 * Error message fields must have the '.errorMessage' class
	 * @param dialog object The dialog containing the form
	 * @param data object JSON data, as sent by the server
	 * JSON is in the form:
	 * <id of field>: [ list of errors ]
	 */
	updateErrors: function(dialog, data)
	{
		var errorsObj = data;
		if(typeof errorsObj === 'object')
		{
			// Iterate the properties - they are the ids of
			// the input fields
			for(var property in errorsObj)
			{
				var id = property;
				
				// Error messages are array. Show only the first one.
				var errorMessage = errorsObj[property][0];
				
				var inputField = dialog.find('#' + id);
				if(inputField !== null)
				{
					// It is assumed that the Error message field will be
					// besides the input field
					var errorMessageDiv = inputField.next('.errorMessage');
					if(errorMessageDiv !== null)
					{
						// Make it visible
						errorMessageDiv.removeAttr('style');
						errorMessageDiv.html(errorMessage);
					}
				}
			}
		}
	}
});

// takes a plugin Constructor function and creates a jQuery style widget
$.pluginMaker = function(plugin)
{
    $.fn[plugin.prototype.name] = function(options)
	{
        var args = $.makeArray(arguments),
            after = args.slice(1);

        return this.each(function()
		{
            // see if we have an instance
            var instance = $.data(this, plugin.prototype.name);
            if (instance)
			{
                // call a method on the instance
                if (typeof options == "string")
				{
                    instance[options].apply(instance, after);
                }
				else if (instance.update)
				{
                    
                    // call update on the instance
                    instance.update.apply(instance, args);
                }
            }
			else
			{
                // create the plugin
                new plugin(this, options);
            }
        })
    };
};
    
// make the jptr_tabs widget
$.pluginMaker(ScrumTool.formDialog);