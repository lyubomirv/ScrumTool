/*!
 * Common functions
 *
 * @author Lyubomir Vasilev <lyubomirv@gmail.com>
 * @copyright Copyright &copy; 2011
 */

/**
 * Strips a specified prefix from an element's id and
 * returns the id without the prefix.
 * The element id is in the form: prefix_N, where
 * N is the id that will be retrieved.
 * @param prefix string the prefix
 * @param elementId string the element's id
 * @return string the id or -1 if there was a problem
 */
function stripPrefix(prefix, elementId)
{
	if(elementId.indexOf(prefix) !== 0)
	{
		return -1;
	}
	
	return elementId.substr(prefix.length);
}

/**
 * Create an url with GET parameters
 * @param url string the url
 * @param parameters object the parameters
 * @param append bool whether just to append the parameters to the url -
 * set to true if the url already contains some parameters.
 * @return string the url with the appended parameters
 */
function createUrl(url, parameters, append)
{
	if(parameters === undefined || parameters.length === 0)
	{
		return url;
	}
	
	var firstParameter = true;
	var resultUrl = url;
	
	if(append === undefined || append === false)
	{
		var resultUrl = resultUrl + '?';
	}
	else
	{
		var firstParameter = false;
	}
	
	for(key in parameters)
	{
		if(firstParameter)
		{
			firstParameter = false;
		}
		else
		{
			resultUrl = resultUrl + '&';
		}
		
		resultUrl = resultUrl + key + '=' + parameters[key];
	}
	
	return resultUrl;
}

// Test
/*
console.log(createUrl('<?php echo $this->createUrl('board'); ?>'));
console.log(createUrl('<?php echo $this->createUrl('board', array('sprintId' => $sprint->id)); ?>'));
console.log(createUrl('<?php echo $this->createUrl('board', array('sprintId' => $sprint->id)); ?>', {columnId: 1}, true));
console.log(createUrl('<?php echo $this->createUrl('board', array('sprintId' => $sprint->id)); ?>', {columnId: 1, test2: 2}, true));
*/
