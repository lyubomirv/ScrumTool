/*!
 * jQuery infoDialog
 * A jQuery-ui dialog that shows information.
 * The dialog' contents are filled from the server by an ajax request when the dialog is opened.
 * 
 * Depends:
 *	jquery.ui.dialog.js
 *
 * @author Lyubomir Vasilev <lyubomirv@gmail.com>
 * @copyright Copyright &copy; 2012
 */

// It is required that the dialog's element is
// created only once. This is the Info Dialog.
var GLOBAL_INFO_DIALOG = jQuery('<div />');
 
function createInfoDialog(settings)
{
	/**
	 * Default settings of the info dialog
	 * @data object POST data to send to the server while making a request for the content
	 * @param contentUrl string url of the info content
	 * @param title string title of the dialog
	 * @param buttonText string text for the dialog button
	 */
	var defaults = {
		data: {},
		contentUrl: undefined,
		title: '',
		buttonText: 'Ok'
	};
	
	var infoDialog = GLOBAL_INFO_DIALOG;
	
	var methods =
	{
		/**
		 * init method
		 * Creates a jQuery-ui dialog.
		 * Attaches a handler to the click event that opens the dialog.
		 * Attaches a handler to the clicking on the overlay when the
		 * dialog is open, that will close the dialog.
		 */
		init: function(options)
		{
			settings = jQuery.extend({}, defaults, options || {});
			
			infoDialog
				.html('')
				.dialog(
				{
					autoOpen: false,
					modal: true,
					title: settings.title,
					width: 800,
					minHeight: 500,
					position: ['center', 50],
					draggable: false,
					resizable: false,
					buttons: [
						{
							text: settings.buttonText,
							click: function()
							{
								infoDialog.dialog('close');
							}
						}
					]
				});
		},
		
		/**
		 * open method
		 * Opens the dialog and calls the server for filling the info's html
		 */
		open: function()
		{
			if(settings.contentUrl === undefined)
			{
				return;
			}
			
			infoDialog.dialog('open');
			$.ajax(
			{
				type: 'POST',
				url: settings.contentUrl,
				data: settings.data,
				success: function(html)
				{
					infoDialog.html(html);
				}
			});
		},
	};
	
	methods.init(settings);
	methods.open();
	
	return false;
}
 
// Close the dialog on mouse click outside it
$('.ui-widget-overlay').live('click', function()
{
	GLOBAL_INFO_DIALOG.dialog('close');
});
