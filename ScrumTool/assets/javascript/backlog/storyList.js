/*!
 * Story list - related javascript code
 */

(function()
{
	jQuery('.story-tasks').hide();
	
	jQuery('.story-tasks-show').live('click', function(event)
	{
		event.preventDefault();
		
		jQuery(this).parent().siblings('.story-tasks').slideToggle(300);
		
		return false;
	});
})();
