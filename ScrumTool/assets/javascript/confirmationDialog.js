// It is required that the dialog's element is
// created only once. This is the Confirm Dialog.
var GLOBAL_CONFIRMATION_DIALOG = $('<div />');

/**
 * Presents a confirmation dialog to the user.
 * The dialog has 'Ok' and 'Cancel' buttons. When the user presses the 'Ok' button,
 * an ajax request is sent to the server and a callback is called when the request returns.
 * The function uses a global 'GLOBAL_CONFIRMATION_DIALOG' variable.
 */
function confirmationDialog(options)
{
	/**
	 * Default settings of the confirmation dialog
	 * @param url string The url that will be accessed if confirmed
	 * @param parameters object Parameters that will be passed to the server
	 * @param title string Dialog title
	 * @param content string Content of the dialog - can be html
	 * @param okText string Text to show on the Ok button
	 * @param cancelText string Text to show on the Cancel button
	 * @param successCallback function Function that will be called
	 *	when the ajax request returns and is successful. The function must have the
	 *	following declaration: function(data)
	 * @param errorCallback function Function that will be called
	 *	when the ajax request returns and there is an error. The function must have the
	 *	following declaration: function(data)
	 */
	var defaults = {
		url: undefined,
		parameters: {},
		title: 'Confirmation required',
		content: 'Are you sure?',
		okText: 'Ok',
		cancelText: 'Cancel',
		successCallback: undefined,
		errorCallback: undefined
	};
	
	var dialog = GLOBAL_CONFIRMATION_DIALOG;
	
	var settings = jQuery.extend({}, defaults, options || {});
	
	var fullContent = '<p>' + settings.content + '</p>';
	
	dialog
		.html(fullContent)
		.dialog(
		{
			autoOpen: false,
			modal: true,
			title: settings.title,
			draggable: false,
			resizable: false,
			buttons: [
				{
					text: settings.okText,
					click: function()
					{
						$.ajax(
						{
							type: 'POST',
							url: settings.url,
							data: settings.parameters,
							dataType: 'json',
							success: function(data)
							{
								if(data.status === 'ok')
								{
									if(settings.successCallback !== undefined)
									{
										settings.successCallback(data);
									}
								}
								else
								{
									if(settings.errorCallback !== undefined)
									{
										settings.errorCallback(data);
									}
								}
							}
						});
						
						dialog.dialog('close');
					}
				},
				{
					text: settings.cancelText,
					click: function()
					{
						dialog.dialog('close');
					}
				},
			]
		});
		
	dialog.dialog('open');
	
	return false;
}

// Close the dialog on mouse click outside it
$('.ui-widget-overlay').live('click', function()
{
	GLOBAL_CONFIRMATION_DIALOG.dialog('close');
});