<?php

return array(
	'user' => array (
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Normal SCRUM member. Can view and manage tasks.',
		'bizRule' => '',
		'data' => ''
	),

	'scrum_master' => array (
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Scrum Master. Can do more things than normal members.',
		'children' => array(
			'user',
		),
		'bizRule' => '',
		'data' => ''
	),

	'product_owner' => array (
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Product Owner. Can manage the Backlog.',
		'children' => array(
			'user',
		),
		'bizRule' => '',
		'data' => ''
	),
	
	'team_administrator' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Team Administrator. Can add to and remove users from the team.',
		'children' => array(
			'user',
			'scrum_master',
			'product_owner',
		),
		'bizRule' => '',
		'data' => ''
	),
	
	'administrator' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Administrator. Can do everything.',
		'children' => array(
			'user',
			'scrum_master',
			'product_owner',
			'team_administrator',
		),
		'bizRule' => '',
		'data' => ''
	),
);
