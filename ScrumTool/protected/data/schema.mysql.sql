-- ----------------------------------------------------------
-- Drop tables
DROP TABLE IF EXISTS `sprint_task_move`;
DROP TABLE IF EXISTS `sprint_column_task`;
DROP TABLE IF EXISTS `sprint_task_assignment`;
DROP TABLE IF EXISTS `sprint_task`;
DROP TABLE IF EXISTS `sprint`;
DROP TABLE IF EXISTS `task`;
DROP TABLE IF EXISTS `story`;
DROP TABLE IF EXISTS `project_column`;
DROP TABLE IF EXISTS `project`;
DROP TABLE IF EXISTS `team_member_role`;
DROP TABLE IF EXISTS `team_member`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `role`;
DROP TABLE IF EXISTS `team`;

SET NAMES utf8;

-- ----------------------------------------------------------
-- Tables

-- User
CREATE TABLE `user` (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(128) NOT NULL UNIQUE,
    password CHAR(64) NOT NULL,
	salt CHAR(16) NOT NULL,
    email VARCHAR(128) NOT NULL,
	name VARCHAR(128)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- Role
CREATE TABLE `role` (
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(128) NOT NULL
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- Team
CREATE TABLE `team` (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(128) NOT NULL UNIQUE
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- Team member
CREATE TABLE `team_member` (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	team_id INTEGER NOT NULL,
	user_id INTEGER NOT NULL,
	FOREIGN KEY (team_id) REFERENCES `team`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (user_id) REFERENCES `user`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
);

-- Team member role
CREATE TABLE `team_member_role` (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	team_member_id INTEGER NOT NULL,
	role_id INTEGER NOT NULL,
	FOREIGN KEY (team_member_id) REFERENCES `team_member`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (role_id) REFERENCES `role`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
);

-- Project
CREATE TABLE `project` (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(128) NOT NULL,
	active TINYINT(1) NOT NULL DEFAULT 1,
	team_id INTEGER NULL,
	FOREIGN KEY (team_id) REFERENCES `team`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	user_id INTEGER NULL,
	FOREIGN KEY (user_id) REFERENCES `user`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- Story
CREATE TABLE `story` (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(128) NOT NULL,
	description VARCHAR(1024),
	priority INTEGER NULL,
	project_id INTEGER NOT NULL,
	FOREIGN KEY (project_id) REFERENCES `project`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- Task
CREATE TABLE `task` (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(128) NOT NULL,
	description VARCHAR(1024),
	estimation INTEGER NULL,
	story_id INTEGER NOT NULL,
	FOREIGN KEY (story_id) REFERENCES `story`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- Sprint
CREATE TABLE `sprint` (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(128) NOT NULL,
	story_points INTEGER NOT NULL,
	date_start DATETIME,
	date_end DATETIME,
	project_id INTEGER NOT NULL,
	FOREIGN KEY (project_id) REFERENCES `project`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- Sprint task
CREATE TABLE `sprint_task` (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	sprint_id INTEGER NOT NULL,
	FOREIGN KEY (sprint_id) REFERENCES `sprint`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	task_id INTEGER NOT NULL,
	FOREIGN KEY (task_id) REFERENCES `task`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
);

-- Sprint task assignment
CREATE TABLE `sprint_task_assignment` (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	sprint_task_id INTEGER NOT NULL,
	FOREIGN KEY (sprint_task_id) REFERENCES `sprint_task`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	user_id INTEGER NOT NULL,
	FOREIGN KEY (user_id) REFERENCES `user`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
);

-- Project column
CREATE TABLE `project_column` (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	project_id INTEGER,
	FOREIGN KEY (project_id) REFERENCES `project`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	name VARCHAR(128) NOT NULL,
	previous_column_id INTEGER
) CHARACTER SET utf8 COLLATE utf8_general_ci;

-- Sprint column task
CREATE TABLE `sprint_column_task` (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	sprint_id INTEGER NOT NULL,
	FOREIGN KEY (sprint_id) REFERENCES `sprint`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	column_id INTEGER NOT NULL,
	FOREIGN KEY (column_id) REFERENCES `project_column`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	task_id INTEGER NOT NULL,
	FOREIGN KEY (task_id) REFERENCES `task`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	previous_id INTEGER
);

-- Sprint task move
CREATE TABLE `sprint_task_move` (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	sprint_id INTEGER NOT NULL,
	FOREIGN KEY (sprint_id) REFERENCES `sprint`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	task_id INTEGER NOT NULL,
	FOREIGN KEY (task_id) REFERENCES `task`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	from_column_id INTEGER NOT NULL,
	FOREIGN KEY (from_column_id) REFERENCES `project_column`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	to_column_id INTEGER NOT NULL,
	FOREIGN KEY (to_column_id) REFERENCES `project_column`(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	`date` DATETIME
);


-- ----------------------------------------------------------
-- Insert data

-- User
INSERT INTO `user` (username, password, salt, email, name) VALUES ('lyubomirv', '703a5d9ce2b5bcbf891bd2b0656bb8db5bbdf658a8290018474aa12b3a7ba219', 'a6aae04e0114b1dd', 'test1@example.com', 'Любо');
INSERT INTO `user` (username, password, salt, email) VALUES ('test2', 'c0cf5560bbd13e2b7766070be75fcd1a45931b1241efaaf095e8f494844807ac', 'a6aae04e0114b1dd', 'test2@example.com');
INSERT INTO `user` (username, password, salt, email, name) VALUES ('test3', 'c9d7c9ca04bcb4f5605fe3aaa9183ef656b9270948722d36eccc71720bba55dd', 'a6aae04e0114b1dd', 'test3@example.com', 'Test Third');
INSERT INTO `user` (username, password, salt, email) VALUES ('test4', 'e3de6be0ff93560a385de4464b52b6fd61618aa4230f21ccd5366bb068277e0b', 'a6aae04e0114b1dd', 'test4@example.com');
INSERT INTO `user` (username, password, salt, email, name) VALUES ('test5', '5f20c6888de5e8829d2da10681757983485bfc9b4650dc8eb7ab9afa55eafa13', 'a6aae04e0114b1dd', 'test5@example.com', 'Test Fifth');
INSERT INTO `user` (username, password, salt, email) VALUES ('test6', '033fe2f0c51c401eb0e78d99cbde6e26bd2493ef3909005c727343cfb3d8fd47', 'a6aae04e0114b1dd', 'test6@example.com');
INSERT INTO `user` (username, password, salt, email) VALUES ('test7', '847c071207499068e429670472c91174c4f2410ea529bfe49b0c1a1bde88cbe2', 'a6aae04e0114b1dd', 'test7@example.com');
INSERT INTO `user` (username, password, salt, email) VALUES ('test8', 'ecc89d6779c68554b47b697cea495d70e9955a4347f26617d53f383797ea7353', 'a6aae04e0114b1dd', 'test8@example.com');
INSERT INTO `user` (username, password, salt, email) VALUES ('test9', '345cd327ec41fc83aef2851b44271c32817f18f8ce6b2d8cd62a479ad9e5b32b', 'a6aae04e0114b1dd', 'test9@example.com');
INSERT INTO `user` (username, password, salt, email) VALUES ('test10', 'a3417292438d07bca532194b7b85c00bbf815ead45585441b8c7ed6427145b94', 'a6aae04e0114b1dd', 'test10@example.com');
INSERT INTO `user` (username, password, salt, email) VALUES ('test11', '5f40706aa8d58ce4e1ef5259cac289e26256632b0a8e9bf45c9412a3d914e05f', 'a6aae04e0114b1dd', 'test11@example.com');
INSERT INTO `user` (username, password, salt, email) VALUES ('test12', '9de3a08943704149c234a7048e45409225ad2536ce2df8056023fcfd8ef8c02a', 'a6aae04e0114b1dd', 'test12@example.com');
INSERT INTO `user` (username, password, salt, email, name) VALUES ('petarpetrov', '847c071207499068e429670472c91174c4f2410ea529bfe49b0c1a1bde88cbe2', 'a6aae04e0114b1dd', 'petarpetrov@gmail.com', 'Петър Петров');
INSERT INTO `user` (username, password, salt, email, name) VALUES ('krasimirast', 'ecc89d6779c68554b47b697cea495d70e9955a4347f26617d53f383797ea7353', 'a6aae04e0114b1dd', 'krasimirast@gmail.com', 'Красимира');
INSERT INTO `user` (username, password, salt, email, name) VALUES ('ivang', '345cd327ec41fc83aef2851b44271c32817f18f8ce6b2d8cd62a479ad9e5b32b', 'a6aae04e0114b1dd', 'ivang@abv.bg', 'Иван Георгиев');
INSERT INTO `user` (username, password, salt, email) VALUES ('goshohubaveca', 'a3417292438d07bca532194b7b85c00bbf815ead45585441b8c7ed6427145b94', 'a6aae04e0114b1dd', 'goshoas@mail.bg');
INSERT INTO `user` (username, password, salt, email, name) VALUES ('plamenp', '5f40706aa8d58ce4e1ef5259cac289e26256632b0a8e9bf45c9412a3d914e05f', 'a6aae04e0114b1dd', 'plamenp@yahoo.com', 'Пламен Панайотов');
INSERT INTO `user` (username, password, salt, email, name) VALUES ('craigoneill', '9de3a08943704149c234a7048e45409225ad2536ce2df8056023fcfd8ef8c02a', 'a6aae04e0114b1dd', 'craigoneill@strike.com', 'Craig Oneill');

-- Role
INSERT INTO `role` (id, name) VALUES (1, 'User');
INSERT INTO `role` (id, name) VALUES (2, 'Scrum master');
INSERT INTO `role` (id, name) VALUES (3, 'Product owner');
INSERT INTO `role` (id, name) VALUES (4, 'Team administrator');
INSERT INTO `role` (id, name) VALUES (5, 'Administrator');

-- Team
INSERT INTO `team` (name) VALUES ('Game Geenies');
INSERT INTO `team` (name) VALUES ('Financial solutions');
INSERT INTO `team` (name) VALUES ('Професионалистите');

-- Team member
INSERT INTO `team_member` (team_id, user_id) VALUES (1, 1);
INSERT INTO `team_member` (team_id, user_id) VALUES (1, 2);
INSERT INTO `team_member` (team_id, user_id) VALUES (1, 3);
INSERT INTO `team_member` (team_id, user_id) VALUES (1, 4);
INSERT INTO `team_member` (team_id, user_id) VALUES (1, 5);
INSERT INTO `team_member` (team_id, user_id) VALUES (1, 6);
INSERT INTO `team_member` (team_id, user_id) VALUES (1, 7);
INSERT INTO `team_member` (team_id, user_id) VALUES (1, 8);
INSERT INTO `team_member` (team_id, user_id) VALUES (2, 9);
INSERT INTO `team_member` (team_id, user_id) VALUES (2, 10);
INSERT INTO `team_member` (team_id, user_id) VALUES (3, 1);
INSERT INTO `team_member` (team_id, user_id) VALUES (3, 13);
INSERT INTO `team_member` (team_id, user_id) VALUES (3, 14);
INSERT INTO `team_member` (team_id, user_id) VALUES (3, 15);
INSERT INTO `team_member` (team_id, user_id) VALUES (3, 16);
INSERT INTO `team_member` (team_id, user_id) VALUES (3, 17);
INSERT INTO `team_member` (team_id, user_id) VALUES (3, 18);

-- Team member role
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (1, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (1, 2);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (1, 3);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (1, 4);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (2, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (2, 2);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (3, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (3, 3);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (3, 4);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (4, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (4, 2);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (5, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (6, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (7, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (7, 3);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (8, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (8, 2);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (8, 3);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (8, 4);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (9, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (9, 2);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (9, 3);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (9, 4);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (10, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (10, 2);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (11, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (11, 2);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (11, 3);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (11, 4);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (12, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (12, 2);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (13, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (14, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (15, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (16, 1);
INSERT INTO `team_member_role` (team_member_id, role_id) VALUES (17, 1);

-- Project
INSERT INTO `project` (name, team_id) VALUES ('Eternal Stardust RPG', 1);
INSERT INTO `project` (name, team_id) VALUES ('Driftmania 3000', 1);
INSERT INTO `project` (name, user_id) VALUES ('Project Secret', 1);
INSERT INTO `project` (name, team_id) VALUES ('Тайните на магията (онлайн ролева игра)', 3);

-- Story
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Game window', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam accumsan ligula a felis hendrerit et lacinia nisl luctus. Etiam blandit, dolor sed sodales dapibus, justo tellus eleifend risus, sed feugiat dolor augue at arcu.', 5, 1);
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Move around', 'Curabitur viverra leo diam, commodo condimentum nisl. Integer suscipit metus libero, nec posuere est. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 6, 1);
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Level up', 'Morbi sed ultrices massa. Nulla fermentum, augue et venenatis pulvinar, diam lectus ullamcorper risus, et mattis orci purus vel nisi.', 10, 1);
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Assign points to statistics', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Curabitur viverra leo diam, commodo condimentum nisl. Integer suscipit metus libero, nec posuere est.', 3, 1);
INSERT INTO `story` (name, description, project_id) VALUES ('Select skills', 'Phasellus placerat sapien vel enim dictum rutrum. Nullam a malesuada mauris. Praesent augue elit, tristique sed posuere et, pharetra sed felis. Etiam eu quam ac sapien adipiscing ullamcorper.', 1);
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Upgrade skills', 'Praesent rhoncus, felis vel vestibulum lacinia, enim velit venenatis libero, id congue augue turpis id metus. Phasellus ultrices dui placerat enim commodo pulvinar.', 2, 1);
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Pick items', 'Morbi consequat erat nec leo dapibus tincidunt. Nullam nec tellus ac est aliquet lacinia et eu ligula. Quisque euismod, nisi in porta iaculis, purus turpis condimentum elit, porttitor posuere justo ipsum a ipsum.', 15, 1);
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Equip items', 'Praesent tellus urna, lobortis ut hendrerit sit amet, volutpat quis augue. Ut ligula ante, faucibus a molestie vitae, euismod eget eros. Suspendisse sed diam eget eros blandit posuere in sit amet sem.', 20, 1);
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Прозорец на приложението', 'Като потребител, искам да мога да стартирам приложение, което да бъде в прозорец.', 5, 4);
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Местене на играча', 'Като потребител, искам да мога да местя играча в игровия свят.', 6, 4);
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Покачване на нивото на героя', 'Като потребител, искам да мога да повишавам нивото на героя чрез игра.', 10, 4);
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Разпределяне на точки', 'Като потребител, искам да мога да разпределям точки по статистиките на героя.', 3, 4);
INSERT INTO `story` (name, description, project_id) VALUES ('Избиране на умения', 'Като потребител, искам да мога да разпределям точки по уменията на героя.', 4);
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Подобряване на умения', 'Като потребител, искам да мога да подобря характеристиките на уменията на героя.', 2, 4);
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Взимане на предмети', 'Като потребител, искам да мога да събирам предмети с героя.', 15, 4);
INSERT INTO `story` (name, description, priority, project_id) VALUES ('Екипиране на предмети', 'Като потребител, искам да мога да екипирам предметите на героя.', 20, 4);

-- Task
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Create C++ project', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam accumsan ligula a felis hendrerit et lacinia nisl luctus. Etiam blandit, dolor sed sodales dapibus, justo tellus eleifend risus, sed feugiat dolor augue at arcu.', 5, 1);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Create Application class', 'Curabitur viverra leo diam, commodo condimentum nisl. Integer suscipit metus libero, nec posuere est. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 4, 1);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Make exe with a window', 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi est est, auctor at pulvinar et, ultrices at mi. Etiam bibendum, risus sed posuere porta, lacus felis interdum diam, non auctor mauris massa a lacus.', 10, 1);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Create Controller class', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam accumsan ligula a felis hendrerit et lacinia nisl luctus. Etiam blandit, dolor sed sodales dapibus, justo tellus eleifend risus, sed feugiat dolor augue at arcu.', 4, 2);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Create UserInput class', 'Curabitur viverra leo diam, commodo condimentum nisl. Integer suscipit metus libero, nec posuere est. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 3, 2);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Add level concept to character', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam accumsan ligula a felis hendrerit et lacinia nisl luctus. Etiam blandit, dolor sed sodales dapibus, justo tellus eleifend risus, sed feugiat dolor augue at arcu.', 14, 3);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Implement level-up rewarding', 'Curabitur viverra leo diam, commodo condimentum nisl. Integer suscipit metus libero, nec posuere est. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 5, 3);
INSERT INTO `task` (name, description, story_id) VALUES ('Assign points to statistics', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam accumsan ligula a felis hendrerit et lacinia nisl luctus. Etiam blandit, dolor sed sodales dapibus, justo tellus eleifend risus, sed feugiat dolor augue at arcu.', 4);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Create SkillTree class', 'Curabitur viverra leo diam, commodo condimentum nisl. Integer suscipit metus libero, nec posuere est. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 5, 5);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Create some skills', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam accumsan ligula a felis hendrerit et lacinia nisl luctus. Etiam blandit, dolor sed sodales dapibus, justo tellus eleifend risus, sed feugiat dolor augue at arcu.', 2, 5);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Add unlocked skills to character', 'Curabitur viverra leo diam, commodo condimentum nisl. Integer suscipit metus libero, nec posuere est. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 3, 5);
INSERT INTO `task` (name, description, story_id) VALUES ('Increase skill characteristics', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam accumsan ligula a felis hendrerit et lacinia nisl luctus. Etiam blandit, dolor sed sodales dapibus, justo tellus eleifend risus, sed feugiat dolor augue at arcu.', 6);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Create Item class', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam accumsan ligula a felis hendrerit et lacinia nisl luctus. Etiam blandit, dolor sed sodales dapibus, justo tellus eleifend risus, sed feugiat dolor augue at arcu.', 4, 7);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Create some items', 'Curabitur viverra leo diam, commodo condimentum nisl. Integer suscipit metus libero, nec posuere est. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 3, 7);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Create Loot class', 'Nam sagittis, sem eget bibendum tempus, odio massa aliquet nunc, non pellentesque est nulla ultricies ipsum. In arcu turpis, facilisis ut elementum ac, molestie ac est. Praesent et lorem ut leo vehicula egestas vel vel enim. Ut sollicitudin dui eget erat placerat vehicula.', 2, 7);
INSERT INTO `task` (name, description, story_id) VALUES ('Create Interaction class', 'Maecenas et sapien ante, in imperdiet nunc. Proin vel mi magna. Phasellus blandit turpis eu arcu luctus a elementum ipsum cursus.', 7);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Create Inventory class', 'Praesent a bibendum nibh. Integer imperdiet, metus ac scelerisque interdum, justo justo auctor metus, a tincidunt tortor ligula id magna. Suspendisse a consequat justo.', 10, 7);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Implement picking items from loot', 'Maecenas sed tellus non lorem placerat feugiat. In malesuada dictum justo a luctus. Quisque et pharetra dui. Vivamus non pretium sapien.', 11, 7);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Create Equipment class', 'Praesent a bibendum nibh. Integer imperdiet, metus ac scelerisque interdum, justo justo auctor metus, a tincidunt tortor ligula id magna. Suspendisse a consequat justo.', 3, 8);
INSERT INTO `task` (name, description, story_id) VALUES ('Implement equipping items from inventory', 'Maecenas sed tellus non lorem placerat feugiat. In malesuada dictum justo a luctus. Quisque et pharetra dui. Vivamus non pretium sapien.', 8);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Създаване на C++ проект', 'Да настрои CMake и да се създаде проект за приложението.', 5, 9);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Да се създаде клас Application', 'Той трябва да представлява играта.', 4, 9);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Създаване на изпълним файл с прозорец', 'При стартиране се визуализира прозорец.', 10, 9);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Клас Controller', 'Управлява събитията, които движат играча.', 4, 10);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Клас UserInput', 'Обработва събитията от входните устройства - мишка, клавиатура (джойстик).', 3, 10);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Концепция за ниво на героя', 'Да може да се вижда кое ниво е героя и да съществуват нужните вътрешни средства за това.', 14, 11);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Награждаване за покачване на ниво', 'Трябва да се поддържа добавяне на различни награди при покачване на нивото на героя.', 5, 11);
INSERT INTO `task` (name, description, story_id) VALUES ('Разпределяне на точки по статистиките', 'Прозорец, в който се визуализират статистиките на героя и дава възможност за използване на точки за повишаването им.', 12);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Клас SkillTree', 'Трябва да поддържа създаване на дърво с умения, които могат да бъдат научени от героя.', 5, 13);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Създаване на няколко умения', 'Да се създадат няколко примерни умения, с които да може да се проиграе системата.', 2, 13);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Добавяне на отключени умения към героя', 'Играчът трябва да може да отключва умения, с което те се добавят към героя и стават използваеми от него.', 3, 13);
INSERT INTO `task` (name, description, story_id) VALUES ('Подобряване на уменията на героя', 'Чрез повторво отключване на умение, то трябвя да подобрява характеристиките си.', 14);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Клас Item (предмет)', 'Предметите трябва да могат да бъдат създавани и модифицирани от гейм-дизайнер.', 4, 15);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Създаване на няколко предмета', 'Да се създадат няколко примерни предмета, с които да може да се проиграе системата.', 3, 15);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Клас Loot (награди)', 'Наградите трябва да могат да съдържат предмети.', 2, 15);
INSERT INTO `task` (name, description, story_id) VALUES ('Клас Interaction (взаимодействие)', 'Играчът трябва да може да взаимодейртва с околната среда в играта.', 15);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Клас Inventory (инвентар)', 'Героят трябва да може да носи в себе си предмети.', 10, 15);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Взимане на награди', 'Героят трябва да може да взима предметите, които се съдържат в наградите и те да се появяват в инвентара му.', 11, 15);
INSERT INTO `task` (name, description, estimation, story_id) VALUES ('Клас Equipment (екипировка)', 'Героят трябва да може да бъде екипиран с някакви предмети.', 3, 16);
INSERT INTO `task` (name, description, story_id) VALUES ('Екипиране на предмети', 'Играчът трябва да може да избира предмети от инвентара си и да ги екипира върху героя си.', 16);

INSERT INTO `sprint` (name, story_points, date_start, date_end, project_id) VALUES ('Sprint 1', 20, UTC_TIMESTAMP(), ADDTIME(UTC_TIMESTAMP(), '14 0:0:0'), 1);
INSERT INTO `sprint` (name, story_points, date_start, date_end, project_id) VALUES ('Спринт 1', 20, '2012-06-04', '2012-06-08', 4);
INSERT INTO `sprint` (name, story_points, date_start, date_end, project_id) VALUES ('Спринт 2', 20, '2012-06-11', '2012-06-15', 4);
INSERT INTO `sprint` (name, story_points, date_start, date_end, project_id) VALUES ('Спринт 3', 20, '2012-06-18', '2012-06-22', 4);
INSERT INTO `sprint` (name, story_points, date_start, date_end, project_id) VALUES ('Спринт 4', 20, '2012-06-25', '2012-06-29', 4);

INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (1, 1);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (1, 2);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (1, 4);

INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (2, 21);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (2, 22);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (2, 23);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (3, 24);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (3, 25);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (3, 26);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (3, 27);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (4, 29);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (4, 30);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (4, 31);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (4, 33);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (5, 34);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (5, 35);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (5, 37);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (5, 38);
INSERT INTO `sprint_task` (sprint_id, task_id) VALUES (5, 39);

INSERT INTO `sprint_task_assignment` (sprint_task_id, user_id) VALUES (1, 1);
INSERT INTO `sprint_task_assignment` (sprint_task_id, user_id) VALUES (2, 1);
INSERT INTO `sprint_task_assignment` (sprint_task_id, user_id) VALUES (2, 2);

INSERT INTO `project_column` (name) VALUES ('To do');
INSERT INTO `project_column` (name) VALUES ('In progress');
INSERT INTO `project_column` (name) VALUES ('Review');
INSERT INTO `project_column` (name) VALUES ('Done');
INSERT INTO `project_column` (project_id, name, previous_column_id) VALUES (1, 'Testing', 3);

INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (1, 1, 1, -1);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (1, 1, 2, 1);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (1, 1, 4, 2);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (2, 4, 21, -1);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (2, 4, 22, 23);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (2, 4, 23, 21);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (3, 4, 24, -1);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (3, 4, 25, 24);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (3, 4, 26, 25);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (3, 4, 27, 26);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (4, 4, 29, -1);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (4, 4, 30, 29);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (4, 4, 31, 30);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (4, 4, 33, 31);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (5, 3, 34, -1);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (5, 4, 35, -1);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (5, 2, 37, -1);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (5, 1, 38, -1);
INSERT INTO `sprint_column_task` (sprint_id, column_id, task_id, previous_id) VALUES (5, 3, 39, 34);

INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (2, 21, 1, 4, '2012-06-06');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (2, 23, 1, 4, '2012-06-07');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (2, 22, 1, 4, '2012-06-08');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (3, 24, 1, 4, '2012-06-12');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (3, 25, 1, 4, '2012-06-13');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (3, 26, 1, 4, '2012-06-14');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (3, 27, 1, 4, '2012-06-15');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (4, 29, 1, 4, '2012-06-19');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (4, 30, 1, 4, '2012-06-20');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (4, 31, 1, 4, '2012-06-21');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (4, 33, 1, 4, '2012-06-21');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (5, 34, 1, 3, '2012-06-26');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (5, 35, 1, 4, '2012-06-26');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (5, 37, 1, 2, '2012-06-25');
INSERT INTO `sprint_task_move` (sprint_id, task_id, from_column_id, to_column_id, `date`) VALUES (5, 39, 1, 3, '2012-06-26');
