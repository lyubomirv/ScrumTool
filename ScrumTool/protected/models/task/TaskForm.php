<?php

/**
 * TaskForm class.
 * TaskForm is the data structure for keeping form data
 * when adding and updating a task in a story. It is used by
 * the 'add' and 'edit' actions of 'TaskController'.
 */
class TaskForm extends FormModel
{
	public $name;
	public $description;
	public $estimation;
	
	private $_storyId;
	private $_taskId;
	
	function __construct($storyId, $taskId = -1, $scenario = '')
	{
		parent::__construct($scenario);
		$this->_storyId = $storyId;
		$this->_taskId = $taskId;
	}

	/**
	 * Declares the validation rules.
	 * The rules state that name is required and task with
	 * such name must not exist in the selected story.
	 */
	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max' => 128),
			array('name', 'checkNotExists'),
			array('description', 'length', 'max' => 1024),
			array('estimation', 'numerical', 'integerOnly' => true),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'name' => Yii::t('backlog', 'Name'),
			'description' => Yii::t('backlog', 'Description'),
			'estimation' => Yii::t('backlog', 'Estimation'),
		);
	}
	
	/**
	 * Check that a story with the same name does not
	 * exist for the current project.
	 */
	public function checkNotExists($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			$this->purify();
			
			$task = Task::model()->findByAttributes(array('name' => $this->name, 'story_id' => $this->_storyId));
			if($task !== null && $task->id !== $this->_taskId)
			{
				$this->addError('name', 'Task already exists.');
			}
		}
	}
}
