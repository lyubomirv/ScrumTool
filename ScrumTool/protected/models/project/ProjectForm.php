<?php

/**
 * ProjectForm class.
 * ProjectForm is the data structure for keeping
 * form data when creating a project. It is used by
 * the 'create' and 'edit' actions of 'ProjectController'.
 */
class ProjectForm extends FormModel
{
	public $name;
	//public $description;
	
	public $teamId;
	
	public $useColumn1;
	public $column1Name;
	public $column1Relativity;
	public $column1OtherColumn;
	
	public $useColumn2;
	public $column2Name;
	public $column2Relativity;
	public $column2OtherColumn;

	/**
	 * Declares the validation rules.
	 * The rules state that name is required and project with such name must not exist.
	 */
	public function rules()
	{
		return array(
			// name is required
			array('name', 'required'),
			// name must exist
			array('name', 'checkNameNotExists'),
			array('teamId', 'checkUserIsMemberOfTeam'),
			array('useColumn1, useColumn2', 'boolean'),
			array('useColumn1, useColumn2', 'checkColumnInfoComplete'),
			array('column1Name', 'checkColumnName1NotExists'),
			array('column2Name', 'checkColumnName2NotExists'),
			array('column1Relativity, column2Relativity', 'verifyRelativity'),
			array('column1OtherColumn, column2OtherColumn', 'verifyColumnOtherColumn'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'name' => Yii::t('project', 'Name'),
			//'description' => 'Description',
			'teamId' => Yii::t('project', 'Add this project to team'),
			'useColumn1' => Yii::t('project', '1.'),
			'column1Name' => Yii::t('project', 'Name'),
			'column1Relativity' => Yii::t('project', 'Relativity'),
			'column1OtherColumn' => Yii::t('project', 'Other column'),
			'useColumn2' => Yii::t('project', '2.'),
			'column2Name' => Yii::t('project', 'Name'),
			'column2Relativity' => Yii::t('project', 'Relativity'),
			'column2OtherColumn' => Yii::t('project', 'Other column'),
		);
	}
	
	/**
	 * Check if a team is selected
	 * @return bool whether a team is selected
	 */
	public function isTeamSelected()
	{
		return $this->teamId !== null && $this->teamId > 0;
	}
	
	/**
	 * Check that a project with the same name does not
	 * exist for the current user's team or in the user projects.
	 */
	public function checkNameNotExists($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			$this->purify();
			
			if($this->isTeamSelected())
			{
				$project = Project::model()->findByAttributes(array('name' => $this->name, 'team_id' => $this->teamId));
				if($project !== null)
				{
					$this->addError('name', 'Project already exists.');
				}
			}
			else
			{
				$project = Project::model()->findByAttributes(array('name' => $this->name, 'user_id' => Yii::app()->user->getId()));
				if($project !== null)
				{
					$this->addError('name', 'Project already exists.');
				}
			}
		}
	}
	
	/**
	 * Check that the user is member of the selected team
	 */
	public function checkUserIsMemberOfTeam($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			$this->purify();
			
			if(!$this->isTeamSelected())
			{
				return;
			}
			
			$isMemberOfThisTeam = TeamMember::model()->exists(
				'team_id = :teamId AND user_id = :userId',
				array(
					'teamId' => $this->teamId,
					'userId' => Yii::app()->user->getId()
					)
				);
			
			if(!$isMemberOfThisTeam)
			{
				$this->addError('teamId', Yii::t('project', 'You are not a member of this team.' . $this->teamId));
			}
		}
	}
	
	/**
	 * Check that the info for a column is complete.
	 */
	public function checkColumnInfoComplete($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			if($attribute === 'useColumn1' && $this->useColumn1 === true)
			{
				if(strlen($this->column1Name) > 0 &&
					strlen($this->column1Relativity) > 0 &&
					strlen($this->column1OtherColumn) > 0
					)
				{
					return true;
				}
				else
				{
					$this->addError('useColumn1', Yii::t('project', 'Incomplete column data.'));
					return false;
				}
			}
			
			if($attribute === 'useColumn2' && $this->useColumn2 === true)
			{
				if(strlen($this->column2Name) > 0 &&
					strlen($this->column2Relativity) > 0 &&
					strlen($this->column2OtherColumn) > 0
					)
				{
					return true;
				}
				else
				{
					$this->addError('useColumn2', Yii::t('project', 'Incomplete column data.'));
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Check that a column with name column1Name does not
	 * exist for the current project.
	 */
	public function checkColumnName1NotExists($attribute, $params)
	{
		if(!$this->useColumn1 || strlen($this->column1Name) === 0)
		{
			return;
		}
		
		if(!$this->hasErrors())
		{
			$this->purify();
			
			$standardColumns = ProjectColumn::model()->findAllByPk(array(1, 2, 3, 4));
			foreach($standardColumns as $standardColumn)
			{
				if(strtolower($standardColumn->name) === strtolower($this->column1Name))
				{
					$this->addError('column1Name', Yii::t('project', 'Column already exists.'));
					return;
				}
			}
			
			if(strtolower($this->column1Name) === strtolower($this->column2Name))
			{
				$this->addError('column1Name', Yii::t('project', 'Column already exists.'));
			}
		}
	}
	
	/**
	 * Check that a column with name column2Name does not
	 * exist for the current project.
	 */
	public function checkColumnName2NotExists($attribute, $params)
	{
		if(!$this->useColumn2 || strlen($this->column2Name) === 0)
		{
			return;
		}
		
		if(!$this->hasErrors())
		{
			$this->purify();
			
			$standardColumns = ProjectColumn::model()->findAllByPk(array(1, 2, 3, 4));
			foreach($standardColumns as $standardColumn)
			{
				if(strtolower($standardColumn->name) === strtolower($this->column2Name))
				{
					$this->addError('column2Name', Yii::t('project', 'Column already exists.'));
					return;
				}
			}
			
			if(strtolower($this->column2Name) === strtolower($this->column1Name))
			{
				$this->addError('column2Name', Yii::t('project', 'Column already exists.'));
			}
		}
	}
	
	public function verifyRelativity($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			if(
				($attribute === 'column1Relativity' && !$this->useColumn1) ||
				($attribute === 'column2Relativity' && !$this->useColumn2)
				)
			{
				return;
			}
			
			$this->purify();
			
			if($this->$attribute != 'before' && $this->$attribute != 'after')
			{
				$this->addError($attribute, Yii::t('project', 'Invalid relativity'));
				return false;
			}
		}
		
		return true;
	}
	
	public function verifyColumnOtherColumn($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			if(
				($attribute === 'column1OtherColumn' && !$this->useColumn1) ||
				($attribute === 'column2OtherColumn' && !$this->useColumn2)
				)
			{
				return true;
			}
			
			$this->purify();
			
			$standardColumns = ProjectColumn::model()->findAllByPk(array(1, 2, 3, 4));
			foreach($standardColumns as $standardColumn)
			{
				if($standardColumn->name === $this->$attribute)
				{
					return true;
				}
			}
			
			if($attribute === 'column1OtherColumn')
			{
				if($this->$attribute === $this->column2Name)
				{
					return true;
				}
			}
			else
			{
				if($this->$attribute === $this->column1Name)
				{
					return true;
				}
			}
			
			$this->addError($attribute, Yii::t('project', 'No such column'));
		}
	}
}
