<?php

/**
 * This is the model class for table "team_member".
 *
 * The followings are the available columns in table 'team_member':
 * @property integer $id
 * @property integer $team_id
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property Team $team
 * @property User $user
 * @property TeamMemberRole[] $teamMemberRoles
 */
class TeamMember extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return TeamMember the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'team_member';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('team_id, user_id', 'required'),
			array('team_id, user_id', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'team' => array(self::BELONGS_TO, 'Team', 'team_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'teamMemberRoles' => array(self::HAS_MANY, 'TeamMemberRole', 'team_member_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'team_id' => 'Team',
			'user_id' => 'User',
		);
	}
}