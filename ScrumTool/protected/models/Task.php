<?php

/**
 * This is the model class for table "task".
 *
 * The followings are the available columns in table 'task':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $estimation
 * @property integer $story_id
 *
 * The followings are the available model relations:
 * @property SprintColumnTask[] $sprintColumnTasks
 * @property SprintTask[] $sprintTasks
 * @property SprintTaskMove[] $sprintTaskMoves
 * @property Story $story
 */
class Task extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Task the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'task';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max' => 128),
			array('description', 'length', 'max' => 1024),
			array('estimation', 'numerical', 'integerOnly' => true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'sprintColumnTasks' => array(self::HAS_MANY, 'SprintColumnTask', 'task_id'),
			'sprintTasks' => array(self::HAS_MANY, 'SprintTask', 'task_id'),
			'sprintTaskMoves' => array(self::HAS_MANY, 'SprintTaskMove', 'task_id'),
			'story' => array(self::BELONGS_TO, 'Story', 'story_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'estimation' => 'Estimation',
			'story_id' => 'Story',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, false);
		$criteria->compare('story_id', $this->story_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}