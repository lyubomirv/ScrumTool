<?php

/**
 * SprintForm class.
 * SprintForm is the data structure for keeping form data
 * when creating and updating a sprint in a project's. It is used by
 * the 'create' and 'edit' actions of 'SprintController'.
 */
class SprintForm extends FormModel
{
	public $name;
	public $story_points;
	public $date_start;
	public $date_end;
	
	private $_projectId;
	
	function __construct($projectId)
	{
		parent::__construct();
		$this->_projectId = $projectId;
	}

	/**
	 * Declares the validation rules.
	 * The rules state that name is required and story with
	 * such name must not exist in the selected project.
	 */
	public function rules()
	{
		return array(
			// name is required
			array('name, story_points', 'required'),
			// name must exist
			array('name', 'checkNotExists'),
			// story_points must be a number
			array('story_points', 'numerical', 'integerOnly' => true, 'min' => 0),
			// date_start, date_end must be valid dates
			//array('date_start, date_end', 'date', 'format' => 'yyyy/MM/dd', 'safe' => true),
			array('date_start, date_end', 'safe'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'name' => Yii::t('sprint', 'Name'),
			'story_points' => Yii::t('sprint', 'Story points'),
			'date_start' => Yii::t('sprint', 'Start date'),
			'date_end' => Yii::t('sprint', 'End date'),
		);
	}
	
	/**
	 * Check that a sprint with the same name does not
	 * exist for the current project.
	 */
	public function checkNotExists($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			$this->purify();
			
			if($this->_projectId === null)
			{
				return;
			}
			
			$sprint = Sprint::model()->find('name = :name AND project_id = :projectId', array(':name' => $this->name, ':projectId' => $this->_projectId));
			if($sprint !== null)
			{
				$this->addError('name', Yii::t('sprint', 'Sprint already exists.'));
			}
		}
	}
}
