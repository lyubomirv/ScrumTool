<?php

/**
 * This is the model class for table "sprint_task_assignment".
 *
 * The followings are the available columns in table 'sprint_task_assignment':
 * @property integer $id
 * @property integer $sprint_task_id
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property SprintTask $sprintTask
 * @property User $user
 */
class SprintTaskAssignment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SprintTaskAssignment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sprint_task_assignment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			// The following rule is used by search().
			array('id, sprint_task_id, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'sprintTask' => array(self::BELONGS_TO, 'SprintTask', 'sprint_task_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sprint_task_id' => 'Sprint Task',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('sprint_task_id', $this->sprint_task_id);
		$criteria->compare('user_id', $this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			));
	}
}