<?php

/**
 * This is the model class for table "sprint_task_move".
 *
 * The followings are the available columns in table 'sprint_task_move':
 * @property integer $id
 * @property integer $sprint_id
 * @property integer $task_id
 * @property integer $from_column_id
 * @property integer $to_column_id
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Sprint $sprint
 * @property Task $task
 * @property ProjectColumn $fromColumn
 * @property ProjectColumn $toColumn
 */
class SprintTaskMove extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return SprintTaskMove the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sprint_task_move';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'sprint' => array(self::BELONGS_TO, 'Sprint', 'sprint_id'),
			'task' => array(self::BELONGS_TO, 'Task', 'task_id'),
			'fromColumn' => array(self::BELONGS_TO, 'ProjectColumn', 'from_column_id'),
			'toColumn' => array(self::BELONGS_TO, 'ProjectColumn', 'to_column_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sprint_id' => 'Sprint',
			'task_id' => 'Task',
			'from_column_id' => 'From Column',
			'to_column_id' => 'To Column',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('sprint_id', $this->sprint_id);
		$criteria->compare('task_id', $this->task_id);
		$criteria->compare('from_column_id', $this->from_column_id);
		$criteria->compare('to_column_id', $this->to_column_id);
		$criteria->compare('date', $this->date);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	
	public function beforeSave()
	{
		if($this->isNewRecord)
		{
			$this->date = new CDbExpression('UTC_TIMESTAMP()');
		}
		
		return parent::beforeSave();
	}
}