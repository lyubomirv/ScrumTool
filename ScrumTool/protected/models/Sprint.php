<?php

/**
 * This is the model class for table "sprint".
 *
 * The followings are the available columns in table 'sprint':
 * @property integer $id
 * @property string $name
 * @property integer $story_points
 * @property string $date_start
 * @property string $date_end
 * @property integer $project_id
 *
 * The followings are the available model relations:
 * @property Project $project
 * @property SprintColumnTask[] $sprintColumnTasks
 * @property SprintTask[] $sprintTasks
 * @property SprintTaskMove[] $sprintTaskMoves
 */
class Sprint extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Sprint the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sprint';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name, story_points', 'required'),
			array('story_points', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max' => 128),
			array('date_start, date_end', 'date', 'format' => 'yyyy/MM/dd'),
			//array('date_start, date_end', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
			'sprintColumnTasks' => array(self::HAS_MANY, 'SprintColumnTask', 'sprint_id'),
			'sprintTasks' => array(self::HAS_MANY, 'SprintTask', 'sprint_id'),
			'sprintTaskMoves' => array(self::HAS_MANY, 'SprintTaskMove', 'sprint_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'story_points' => 'Story Points',
			'date_start' => 'Start Date',
			'date_end' => 'End Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name);
		$criteria->compare('story_points', $this->story_points);
		$criteria->compare('date_start', $this->date_start);
		$criteria->compare('date_end', $this->date_end);
		$criteria->compare('project_id', $this->project_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}