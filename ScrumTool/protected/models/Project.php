<?php

/**
 * This is the model class for table "project".
 *
 * The followings are the available columns in table 'project':
 * @property integer $id
 * @property string $name
 * @property integer $active
 * @property integer $team_id
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property Team $team
 * @property User $user
 * @property ProjectColumn[] $projectColumns
 * @property Sprint[] $sprints
 * @property Story[] $stories
 */
class Project extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'project';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>128),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'team' => array(self::BELONGS_TO, 'Team', 'team_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'projectColumns' => array(self::HAS_MANY, 'ProjectColumn', 'project_id'),
			'sprints' => array(self::HAS_MANY, 'Sprint', 'project_id'),
			'stories' => array(self::HAS_MANY, 'Story', 'project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('project', 'Name'),
			'active' => 'Active',
			'team_id' => 'Team',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name);
		$criteria->compare('active', $this->active);
		$criteria->compare('team_id', $this->team_id);
		$criteria->compare('user_id', $this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}