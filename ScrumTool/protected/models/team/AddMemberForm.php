<?php

/**
 * AddMemberForm class.
 * AddMemberForm is the data structure for keeping
 * form data when adding a member to a team. It is used by
 * the 'addMember' action of 'TeamController'.
 */
class AddMemberForm extends FormModel
{
	public $username;
	
	private $_teamId;
	
	public function __construct($teamId)
	{
		parent::__construct();
		
		$this->_teamId = $teamId;
	}

	/**
	 * Declares the validation rules.
	 * The rules state that username is required.
	 */
	public function rules()
	{
		return array(
			// username is required
			array('username', 'required'),
			// username must exist
			array('username', 'checkExists'),
			// user must not be in any team
			array('username', 'checkNotInSameTeam'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'username' => Yii::t('team', 'Username'),
		);
	}
	
	/**
	 * Check if the username exists.
	 * @return boolean Whether a user with the given username exists.
	 */
	public function checkExists($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			$this->purify();
			
			$user = User::model()->findByAttributes(array('username' => $this->username));
			if($user === null)
			{
				$this->addError('username', Yii::t('team', 'User does not exist.'));
			}
		}
	}
	
	/**
	 * Check if the user is not in the team already.
	 * @return boolean Whether the user with the given username is not already in the team.
	 */
	public function checkNotInSameTeam($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			$this->purify();
			
			$user = User::model()->findByAttributes(array('username' => $this->username));
			if($user === null)
			{
				$this->addError('username', Yii::t('team', 'User does not exist.'));
				return false;
			}
			
			if($user->teamMembers !== null and count($user->teamMembers) > 0)
			{
				foreach($user->teamMembers as $teamMember)
				{
					if($teamMember->team_id === $this->_teamId)
					{
						$this->addError('username', Yii::t('team', 'User is already a member of the team.'));
						return false;
					}
				}
			}
		}
		
		return true;
	}
}
