<?php

/**
 * CreateTeamForm class.
 * CreateTeamForm is the data structure for keeping
 * form data when creating a team. It is used by
 * the 'createTeam' action of 'TeamController'.
 */
class CreateTeamForm extends FormModel
{
	public $teamName;

	/**
	 * Declares the validation rules.
	 * The rules state that username is required.
	 */
	public function rules()
	{
		return array(
			// teamName is required
			array('teamName', 'required'),
			// team with name teamName must not exist
			array('teamName', 'checkNotExists'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'teamName' => Yii::t('team', 'Team name'),
		);
	}
	
	/**
	 * Check if the teamName does not exist.
	 * @return boolean Whether a team with the given name teamName exists.
	 */
	public function checkNotExists($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			$this->purify();
			
			$team = Team::model()->findByAttributes(array('name' => $this->teamName));
			if($team !== null)
			{
				$this->addError('teamName', Yii::t('team', 'Team already exists.'));
			}
		}
	}
}
