<?php

/**
 * This is the model class for table "story".
 *
 * The followings are the available columns in table 'story':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $priority
 * @property integer $project_id
 *
 * The followings are the available model relations:
 * @property Project $project
 * @property Task[] $tasks
 */
class Story extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Story the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'story';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>128),
			array('description', 'length', 'max'=>1024),
			array('priority', 'numerical', 'integerOnly'=>true),
		);
	}
	
	/*
	Example for ordering
	Usage:
	$stories = Story::model()->prioritized()->findAll();
	Also the scopes can be chained, like this:
	$stories = Story::model()->prioritized()->recently()->findAll();
	public function scopes()
	{
		return array(
			'prioritized' => array(
				'order' => 'priority DESC',
			),
			// Example
			//'recently'=>array(
			//	'condition' => 'status=1',
			//	'order' => 'create_time DESC',
			//	'limit' => 5,
			//),
		);
	}
	*/

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
			'tasks' => array(self::HAS_MANY, 'Task', 'story_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'priority' => 'Priority',
			'project_id' => 'Project',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, false);
		$criteria->compare('priority', $this->priority);
		$criteria->compare('project_id', $this->project_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}