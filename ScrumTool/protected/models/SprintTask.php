<?php

/**
 * This is the model class for table "sprint_task".
 *
 * The followings are the available columns in table 'sprint_task':
 * @property integer $id
 * @property integer $sprint_id
 * @property integer $task_id
 *
 * The followings are the available model relations:
 * @property Sprint $sprint
 * @property Task $task
 * @property SprintTaskAssignment[] $sprintTaskAssignments
 */
class SprintTask extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return SprintTask the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sprint_task';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('sprint_id, task_id', 'required'),
			array('sprint_id, task_id', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'sprint' => array(self::BELONGS_TO, 'Sprint', 'sprint_id'),
			'task' => array(self::BELONGS_TO, 'Task', 'task_id'),
			'sprintTaskAssignments' => array(self::HAS_MANY, 'SprintTaskAssignment', 'sprint_task_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sprint_id' => 'Sprint',
			'task_id' => 'Task',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('sprint_id', $this->sprint_id);
		$criteria->compare('task_id', $this->task_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}