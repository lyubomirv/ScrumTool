<?php

/**
 * This is the model class for table "sprint_column_task".
 *
 * The followings are the available columns in table 'sprint_column_task':
 * @property integer $id
 * @property integer $sprint_id
 * @property integer $column_id
 * @property integer $task_id
 * @property integer $previous_id
 *
 * The followings are the available model relations:
 * @property Sprint $sprint
 * @property ProjectColumn $column
 * @property Task $task
 */
class SprintColumnTask extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return SprintColumnTask the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sprint_column_task';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'sprint' => array(self::BELONGS_TO, 'Sprint', 'sprint_id'),
			'column' => array(self::BELONGS_TO, 'ProjectColumn', 'column_id'),
			'task' => array(self::BELONGS_TO, 'Task', 'task_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name => label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sprint_id' => 'Sprint',
			'column_id' => 'Column',
			'task_id' => 'Task',
			'previous_id' => 'Previous',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('sprint_id', $this->sprint_id);
		$criteria->compare('column_id', $this->column_id);
		$criteria->compare('task_id', $this->task_id);
		$criteria->compare('previous_id', $this->previous_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}