<?php

/**
 * This is the model class for table "project_column".
 *
 * The followings are the available columns in table 'project_column':
 * @property integer $id
 * @property integer $project_id
 * @property string $name
 * @property integer $previous_column_id
 *
 * The followings are the available model relations:
 * @property Project $project
 * @property ProjectColumn $previousColumn
 * @property ProjectColumn[] $projectColumns
 * @property SprintColumnTask[] $sprintColumnTasks
 * @property SprintTaskMove[] $sprintTaskMoves
 * @property SprintTaskMove[] $sprintTaskMoves1
 */
class ProjectColumn extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return ProjectColumn the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'project_column';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name', 'required'),
			array('project_id, previous_column_id', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max' => 128),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
			'previousColumn' => array(self::BELONGS_TO, 'ProjectColumn', 'previous_column_id'),
			//'projectColumns' => array(self::HAS_MANY, 'ProjectColumn', 'previous_column_id'),
			'sprintColumnTasks' => array(self::HAS_MANY, 'SprintColumnTask', 'column_id'),
			'sprintTaskMoves' => array(self::HAS_MANY, 'SprintTaskMove', 'from_column_id'),
			'sprintTaskMoves1' => array(self::HAS_MANY, 'SprintTaskMove', 'to_column_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
			'name' => 'Name',
			'previous_column_id' => 'Previous Column',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('name', $this->name);
		$criteria->compare('previous_column_id', $this->previous_column_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}