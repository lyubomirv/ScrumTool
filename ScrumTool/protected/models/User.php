<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $salt
 * @property string $email
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Project[] $projects
 * @property SprintTaskAssignment[] $sprintTaskAssignments
 * @property TeamMember[] $teamMembers
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('username, password, email', 'required'),
			array('username, password, email, name', 'length', 'max' => 128),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: The user will have only one team. This is just in case.
		return array(
			'projects' => array(self::HAS_MANY, 'Project', 'user_id'),
			'sprintTaskAssignments' => array(self::HAS_MANY, 'SprintTaskAssignment', 'user_id'),
			'teamMembers' => array(self::HAS_MANY, 'TeamMember', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('username', $this->username);
		$criteria->compare('password', $this->password);
		$criteria->compare('email', $this->email);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getPasswordHash($password, $salt)
	{
		$saltedPassword = $salt . $password;
		$hashedPassword = hash('sha256', $saltedPassword);
		
		return $hashedPassword;
	}
}