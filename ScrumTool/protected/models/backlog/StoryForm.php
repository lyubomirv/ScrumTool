<?php

/**
 * StoryForm class.
 * StoryForm is the data structure for keeping form data
 * when adding and updating a story in a project's backlog. It is used by
 * the 'addStory' and 'editStory' actions of 'BacklogController'.
 */
class StoryForm extends FormModel
{
	public $name;
	public $description;
	public $priority;
	
	private $_projectId;
	private $_storyId;
	
	function __construct($projectId, $storyId = -1, $scenario = '')
	{
		parent::__construct($scenario);
		$this->_projectId = $projectId;
		$this->_storyId = $storyId;
	}

	/**
	 * Declares the validation rules.
	 * The rules state that name is required and story with
	 * such name must not exist in the selected project.
	 */
	public function rules()
	{
		return array(
			// name is required
			array('name', 'required'),
			// name must exist
			array('name', 'checkNotExists'),
			// description is safe
			array('description', 'safe'),
			// priority is safe
			array('priority', 'safe'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'name' => Yii::t('backlog', 'Name'),
			'description' => Yii::t('backlog', 'Description'),
			'priority' => Yii::t('backlog', 'Priority'),
		);
	}
	
	/**
	 * Check that a story with the same name does not
	 * exist for the current project.
	 */
	public function checkNotExists($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			$this->purify();
			
			$story = Story::model()->findByAttributes(array('name' => $this->name, 'project_id' => $this->_projectId));
			if($story !== null && $story->id !== $this->_storyId)
			{
				$this->addError('name', 'Story already exists.');
			}
		}
	}
}
