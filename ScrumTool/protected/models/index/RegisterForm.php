<?php

/**
 * RegisterForm class.
 * RegisterForm is the data structure for keeping
 * user registration form data. It is used by the 'register' action of 'IndexController'.
 */
class RegisterForm extends FormModel
{
	public $username;
	public $password;
	public $password2;
	public $email;
	public $name;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username, password and password2 are required
			array('username, password, password2, email', 'required'),
			// password and password2 need to be equal
			array('password', 'compare', 'compareAttribute' => 'password2'),
			// email must be valid e-mail
			array('email', 'email'),
			// all the other are safe
			array('name', 'safe'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'username' => Yii::t('register', 'Username'),
			'password' => Yii::t('register', 'Password'),
			'password2' => Yii::t('register', 'Repeat password'),
			'email' => Yii::t('register', 'E-mail'),
			'name' => Yii::t('register', 'Visible name'),
		);
	}
}
