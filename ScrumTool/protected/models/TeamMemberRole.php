<?php

/**
 * This is the model class for table "team_member_role".
 *
 * The followings are the available columns in table 'team_member_role':
 * @property integer $id
 * @property integer $team_member_id
 * @property integer $role_id
 *
 * The followings are the available model relations:
 * @property TeamMember $teamMember
 * @property Role $role
 */
class TeamMemberRole extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return TeamMemberRole the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'team_member_role';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('team_member_id, role_id', 'required'),
			array('team_member_id, role_id', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'teamMember' => array(self::BELONGS_TO, 'TeamMember', 'team_member_id'),
			'role' => array(self::BELONGS_TO, 'Role', 'role_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'team_member_id' => 'Team Member',
			'role_id' => 'Role',
		);
	}
}