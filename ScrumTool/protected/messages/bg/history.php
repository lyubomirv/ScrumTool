<?php

return array(
	'Sprints' => 'Спринтове',
	'There are no previous sprints in this project.' => 'Няма предишни спринтове за този проект.',
	'Please select a sprint to view it.' => 'Изберете спринт, за да го разгледате.',
);
