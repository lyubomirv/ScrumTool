<?php

return array(
	'This is how Scrum should be done!' => 'Това е начинът да се работи със Scrum!',
	'{appname} provides you with an easy and intuitive approach to managing your agile projects.' => '{appname} Ви предоставя лесен и интуитивен подход към управлението на Вашите гъвкави проекти.',
	'{appname} addresses the needs of each Scrum role and helps taking more benefit of the Scrum process by providing easy and friendly enviroment for managing the Scrum artifacts.' => '{appname} адресира нуждите на всяка от Scrum ролите и помага за извличането на още повече полза от Scrum процеса, като осигурява лесна и приветлива среда за управление на спецификите на Scrum.',
	'What you will find in {appname}:' => 'Какво ще намерите в {appname}:',
	'Manage your Product Backlog efficiently' => 'Управлявайте потребителските си истории ефективно',
	'Create User Stories and set a priority to each story based on what you think its business value is. Split the story into manageable and doable tasks the way you prefer. Set the tasks\' estimation that will later affect the sprint planning.' => 'Създавайте потребителски истории и задавайте техните приоритети в зависимост от бизнес стойността им. Разделяйте историята на управляеми и завършваеми задачи както предпочитате. Задавайте оценка на задачите, която след това ще повлияе на планирането на спринтовете.',
	'Change task status with only a click' => 'Променяйте състоянието на задачите с едно щракване',
	'View the tasks, selected for a sprint at a sprint board. With its clean design and usable interface, you can set change status of a task by simply moving it through the columns with a simple drag with the mouse. No refreshing the page needed.' =>
	'Разглеждайте задачите, избрани за спринт на дъската. С нейния изчистен дизайн и удобен интерфейс, можете да променяте състоянието на задача като просто я премествате с мишката между колоните. Няма нужда от презарежждане на страницата.',
	'See the Product status at a glance' => 'Вижте състоянието на продукта с един поглед',
	'Take a look at charts, describing the Product\'s status and giving you visual feedback of the progress. See the sprint Burndown chart to get a view of where the sprint is going. Or have expectations on how much the team can accomplish based on their sprint history, visible in the Velocity chart.' => 'Гледайте графиките, които описват състоянието на продукта и ви дават визуална представа за прогреса. Вижте диаграмата на стопяването за да видите накъде отива спринта. Или си създайте впечатление за това какво може да постигне екипа базирано на историята на спринтовете, видима на диаграмата за скоростта.',
	'You can start using {appname} right now. {register-start}Register{register-end} or {signin-start}Sign in{signin-end}, if you already have an accout.' =>
	'Можете да започнете да използвате {appname} още сега. {register-start}Регистрирайте се{register-end} или се {signin-start}Впишете{signin-end}, ако вече сте регистриран.',
	
	'Welcome to {appname}, {username}!' => 'Добре дошли в {appname}, {username}!',
	'You are already participating in one or more teams. Visit your teams\' pages to get more information.' => 'Вие вече участвате в един или повече екипи. Посетете Вашата страница на екипите за повече информация.',
	'You are still not participating in any teams. Create one by visiting the teams page.' => 'Вие все още не участвате в нито един екип. Можете да създадете, като посетите страницата на екипите.',
	'You are already taking active role in one or more projects. Visit your projects\' page to continue you work.' => 'Вие участвате активно в един или повече проекти. Посетете Вашата страницa на проектите, за да продължите работа.',
	'You are still not working on any projects. Create one by visiting the projects page.' => 'Вие все още не взимате участие в нито един проект. Можете да създадете, като посетите страницата на проектите.',
);
