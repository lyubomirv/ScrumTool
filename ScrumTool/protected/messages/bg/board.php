<?php

return array(
	'{sprint} board' => 'Дъска за {sprint}',
	'assign me' => 'включи ме',
	'unassign me' => 'изключи ме',
	'Not assigned' => 'Не се притежава',
);
