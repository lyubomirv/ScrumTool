<?php

return array(
	'Dashboard' => 'Табло',
	'Burn-down' => 'Диаграма на стопяването',
	'Velocity' => 'Диаграма на скоростта',
	'There is not enough data to draw a burn-down chart.' => 'Няма достатъчно данни за изчертаване на диаграма на стопяването.',
	'There is not enough data to draw a velocity chart.' => 'Няма достатъчно данни за изчертаване на диаграма на скоростта.',
);
