<?php

return array(
	'Register' => 'Регистрация',
	'Username' => 'Потребителско име',
	'Password' => 'Парола',
	'Repeat password' => 'Повторете паролата',
	'E-mail' => 'Е-поща',
	'Visible name' => 'Видимо име',
);
