<?php

return array(
	'Name' => 'Име',
	'Story points' => 'Точки',
	'Start date' => 'Начална дата',
	'End date' => 'Крайна дата',
	'Sprint already exists.' => 'Спринт с това име вече съществува.',
	'Save' => 'Запиши',
);
