<?php

return array(
	'Copyright &copy; {date} by Lyubomir Vasilev.<br />All Rights Reserved.<br />' => '&copy; {date} Любомир Василев.<br />Всички права запазени.<br />',
	'Confirmation required' => 'Нужно е потвърждение',
	'Remove' => 'Премахни',
	'Cancel' => 'Откажи',
	'view' => 'виж',
	'edit' => 'ред.',
	'delete' => 'изтрий',
	'collapse all' => 'събери всичко',
	'expand all' => 'разгъни всичко',
);
