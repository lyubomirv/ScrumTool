<?php

return array(
	'Home' => 'Начало',
	'Sign in' => 'Вход',
	'Sign out' => 'Изход',
	'Username: {username}' => 'Потребител: {username}',
	'Teams' => 'Екипи',
	'Projects' => 'Проекти',
	'Options' => 'Настройки',
	'Language:' => 'Език:',
);
