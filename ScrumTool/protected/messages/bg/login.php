<?php

return array(
	'Sign in' => 'Вход',
	'Register' => 'Регистрация',
	'Username' => 'Потребителско име',
	'Password' => 'Парола',
	'Remember me' => 'Запомни ме',
	'Incorrect username or password.' => 'Неправилно потребителско име или парола.',
);
