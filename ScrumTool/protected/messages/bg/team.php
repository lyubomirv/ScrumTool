<?php

return array(
	'Team' => 'Екип',
	'Team:' => 'Екип:',
	'You are not a member of any team.' => 'Вие не сте член на нито един екип все още.',
	'Create team' => 'Създай екип',
	'Create team ...' => 'Създай екип ...',
	'Leave' => 'Напускане',
	'Team name' => 'Име на екипа',
	'Team already exists.' => 'Екип с това име вече съществува.',
	'User does not exist.' => 'Не съществува такъв потребител.',
	'User is already a member of the team.' => 'Този потребител е вече член на екипа.',
	'Add member' => 'Добави член',
	'Username' => 'Потребителско име',
	'User' => 'Потребител',
	'E-mail' => 'Е-поща',
	'Roles' => 'Роли',
	'Actions' => 'Действия',
	'Remove' => 'Премахни',
	'Are you sure you want to remove this member?' => 'Сигурен/а ли сте, че искате да премахнете този член?',
	'Are you sure you want to leave the team?' => 'Сигурен/а ли сте, че искате да напуснете екипа?',
);
