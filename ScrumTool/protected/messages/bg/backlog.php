<?php

return array(
	'{project} backlog' => 'Потребителски истории за {project}',
	'Add story' => 'Добави история',
	'This project doesn\'t seem to have any stories in its backlog yet. You can add one by clicking on the "Add story" button.' => 'Изглежда в този проект все още няма потребителски истории. Можете да добавите чрез бутона "Добави история"',
	'This project doesn\'t seem to have any stories in its backlog yet. Contact your Product owner so he/she can add some.' => 'Изглежда в този проект все още няма потребителски истории. Свържете се с собственика на проекта, за да създаде.',
	'Add task' => 'Добави задача',
	'Are you sure you want to delete this story?' => 'Сигурен/а ли сте, че искате да изтриете тази история?',
	'Are you sure you want to delete this task?' => 'Сигурен/а ли сте, че искате да изтриете тази задача?',
	'View story' => 'Преглед на история',
	'Priority:' => 'Приоритет:',
	'View task' => 'Преглед на задача',
	'Estimation:' => 'Оценка:',
	'Name' => 'Име',
	'Description' => 'Описание',
	'Priority' => 'Приоритет',
	'Estimation' => 'Оценка',
);
