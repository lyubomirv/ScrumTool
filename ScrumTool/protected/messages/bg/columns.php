<?php

return array(
	'To do' => 'Незапочнати',
	'In progress' => 'В процес на работа',
	'Review' => 'Преглед',
	'Done' => 'Завършени',
);
