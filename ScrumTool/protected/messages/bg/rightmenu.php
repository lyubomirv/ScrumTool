<?php

return array(
	'No project selected' => 'Няма избран проект',
	'Backlog' => 'Потребителски истории',
	'Sprint planning' => 'Планиране на спринт',
	'Sprint history' => 'История на спринтовете',
	'Dashboard' => 'Състояние',
	'Board' => 'Дъска',
);
