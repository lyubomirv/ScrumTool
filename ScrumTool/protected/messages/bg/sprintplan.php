<?php

return array(
	'Sprint plan' => 'Планиране на спринт',
	'Backlog' => 'Потребителски истории',
	'Sprint' => 'Спринт',
	'There are no stories in the backlog. Stories can be added in the {backlog-start}Backlog{backlog-end}.' => 'Няма потребителски истории. Историите могат да бъдат създавани в {backlog-start}Списъка с потребителски истории{backlog-end}',
	'No estimation' => 'Няма оценка',
	'In {othersprint}' => 'В {othersprint}',
	'Add sprint...' => 'Добави спринт...',
	'This project does not have any sprints yet.' => 'Този проект все още няма нито един спринт.',	
	'Add sprint' => 'Добави спринт',
	'There are no tasks in this sprint yet.' => 'Все още няма задачи в този спринт.',
	'Please create or select a sprint to be able to plan it.' => 'Създайте или изберете спринт, който да планирате.',
	'This task has already been moved around during the sprint. Are you sure you want to remove it from the sprint?' => 'Тази задача вече е била местена по дъската на спринта. Сигурен/а ли сте, че искате да я премахнете от спринта?',
);
