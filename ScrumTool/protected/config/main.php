<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name' => 'Scrum Tool',
	'defaultController' => 'index',
	'sourceLanguage' => 'en_us',
	'language' => 'en_us',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'gii',
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			//'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
			'loginUrl' => array('index/login'),
		),
		
		'urlManager'=>array(
			'urlFormat' => 'path',
			'showScriptName' => false,
			'rules' => array(
				//'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'project/<projectId:\d+>/board/<sprintId:\d+>/<action:\w+>' => 'board/<action>',
				'project/<projectId:\d+>/sprintPlan/<sprintId:\d+>/<action:\w+>' => 'sprintPlan/<action>',
				'project/<projectId:\d+>/dashboard/<sprintId:\d+>/<action:\w+>' => 'dashboard/<action>',
				'project/<projectId:\d+>/backlog/<action:\w+>' => 'backlog/<action>',
				'project/<projectId:\d+>/history/<action:\w+>' => 'history/<action>',
				'project/<projectId:\d+>/<action:\w+>' => 'project/<action>',
				'project/<projectId:\d+>/<storyId:>/task/<action:\w+>' => 'task/<action>',
				'project/<projectId:\d+>/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
				'project/<projectId:\d+>/<controller:\w+>' => '<controller>/index',
				'team/<teamId:\d+>/<action:\w+>' => 'team/<action>',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			),
		),
		
		'authManager' => array(
            'class' => 'PhpAuthManager',
			'authFile' => 'protected/data/auth.php'
        ),
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=scrumtool',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		
		'clientScript'=>array(
			'class'=>'CClientScript',
			'scriptMap'=>array(
				'jquery.js' => false,
			),
			'coreScriptPosition' => CClientScript::POS_END,
		),
		
		'errorHandler'=>array(
			// use 'index/error' action to display errors
            'errorAction' => 'index/error',
        ),
		
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail' => 'webmaster@example.com',
		// Length of salt used when hashing user passwords
		'saltLength' => 16,
		// Role identifiers
		'roleNames' => array(
			1 => 'user',
			2 => 'scrum_master',
			3 => 'product_owner',
			4 => 'team_administrator',
			5 => 'administrator',
			),
		'roleIdentifiers' => array(
			'user' => 1,
			'scrum_master' => 2,
			'product_owner' => 3,
			'team_administrator' => 4,
			'administrator' => 5,
			),
	),
);