<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('project', 'Edit project');
?>

<div class="editProject">
	<?php echo $this->renderPartial('form', array('model' => $model, 'showSubmitButton' => $showSubmitButton, 'teams' => $teams, 'standardColumnNames' => $standardColumnNames)); ?>
</div><!-- editProject -->
