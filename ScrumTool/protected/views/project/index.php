<?php
$this->pageTitle = Yii::app()->name . ' - '. Yii::t('project', 'Projects');
?>

<div class="centered-div project">

	<!-- Teams' projects -->
	<?php foreach($teams as $team): ?>
		<?php
			$isAdministrator = $this->isTeamAdministrator($team);
		?>
		<div class="project-title">
			<span><?php echo $team->name; ?></span>
		</div>

		<div class="list">
			<?php if($this->hasActiveProjects($team->id)): ?>
				<?php $this->renderPartial('projectList', array('team' => $team, 'projects' => $team->projects)); ?>
			<?php else: ?>
				<?php if($isAdministrator): ?>
					<div class="description">
						<span><?php echo Yii::t('project', 'This team does not have any projects yet. You can start one by clicking on the "Create project" button.'); ?></span>
					</div>
				<?php else: ?>
					<div class="description">
						<span><?php echo Yii::t('project', 'This team does not have any projects yet. Contact your Team administrator so he/she can start one.'); ?></span>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div><!-- list -->
	<?php endforeach; ?>
	
	<!-- Own projects -->
	<div class="project-title">
		<span><?php echo Yii::t('project', 'Own projects'); ?></span>
	</div>
	
	<div class="list">
		<?php if(!empty($ownProjects)): ?>
			<?php $this->renderPartial('projectList', array('team' => null, 'projects' => $ownProjects)); ?>
		<?php else: ?>
			<div class="description">
				<span><?php echo Yii::t('project', 'You do not have any projects yet. You can start one by clicking on the "Create project" button.'); ?></span>
			</div>
		<?php endif; ?>
	</div><!-- list -->

	<a id="btnCreateProject" href="<?php echo $this->createUrl('project/create'); ?>" class="button"><?php echo Yii::t('project', 'Create project'); ?></a>
	<a id="btnViewArchive" href="<?php echo $this->createUrl('project/viewarchive'); ?>" class="button"><?php echo Yii::t('project', 'View archive'); ?></a>
	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/formDialog.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/confirmationDialog.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/infoDialog.js"></script>
		
	<script type="text/javascript">
	/*<![CDATA[*/
		jQuery(document).ready(function()
		{
			function updateProjects(data)
			{
				window.location.replace('<?php echo $this->createUrl('project/index'); ?>');
			}
			
			jQuery('#btnCreateProject').live('click', function()
			{
				var settings =
				{
					'contentUrl': jQuery(this).attr('href'),
					'actionRender': 'project-render',
					'actionSubmit': 'project-submit',
					'submitSuccessCallback': updateProjects,
					'title': '<?php echo Yii::t('project', 'Create project'); ?>'
				};
				
				return createFormDialog(settings);
			});
			
			jQuery('.btnEditProject').live('click', function()
			{
				var settings =
				{
					'contentUrl': jQuery(this).attr('href'),
					'actionRender': 'project-render',
					'actionSubmit': 'project-submit',
					'submitSuccessCallback': updateProjects,
					'title': '<?php echo Yii::t('project', 'Edit project'); ?>'
				};
				
				return createFormDialog(settings);
			});
			
			jQuery('.btnArchiveProject').live('click', function()
			{
				var settings =
				{
					'url': jQuery(this).attr('href'),
					'title': '<?php echo Yii::t('general', 'Confirmation required'); ?>',
					'content': '<?php echo Yii::t('project', 'Are you sure you want to archive this project?'); ?>',
					'successCallback': updateProjects,
				};
				
				return confirmationDialog(settings);
			});
			
			jQuery('#btnViewArchive').live('click', function()
			{
				var settings =
				{
					'contentUrl': jQuery(this).attr('href'),
					'title': '<?php echo Yii::t('project', 'Projects archive'); ?>',
				};
				
				return createInfoDialog(settings);
			});
		});
	/*]]>*/
	</script>
	
</div><!-- project -->
