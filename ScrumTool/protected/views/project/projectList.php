<?php
	$isAdministrator = true;
	if($team !== null)
	{
		$isAdministrator = $this->isTeamAdministrator($team);
	}
?>

<ul>
	<?php foreach($projects as $project): ?>
		<?php if($project->active): ?>
			<li>
				<?php $element = '<a href="' . $this->createUrl('backlog/index', array('projectId' => $project->id)) . '" class="project-element">' . $project->name . '</a>'; ?>
				
				<?php if($isAdministrator): ?>
					<div class="left">
						<?php echo $element; ?>
					</div>
					<div class="right">
						<div class="errorMessage"></div>
						<div class="project-actions">
							<a href="<?php echo $this->createUrl('project/edit', array('projectId' => $project->id)); ?>" class="btnEditProject"><?php echo Yii::t('project', 'edit'); ?></a>
							<a href="<?php echo $this->createUrl('project/archive', array('projectId' => $project->id)); ?>" class="btnArchiveProject"><?php echo Yii::t('project', 'archive'); ?></a>
						</div>
					</div>
					<br style="clear: both;"/>
				<?php else: ?>
					<?php echo $element; ?>
				<?php endif; ?>
			</li>
		<?php endif; ?>
	<?php endforeach; ?>
</ul>
