<?php
$this->pageTitle = Yii::app()->name . ' - '. Yii::t('project', 'Projects archive');
?>

<div class="centered-div project">

	<?php $atLeastOneProjectArchived = false; ?>
	
	<!-- Teams' projects -->
	<?php foreach($teams as $team): ?>
		<?php if($this->hasInactiveProjects($team->id)): ?>
			<?php $atLeastOneProjectArchived = true; ?>
			
			<?php
				$isAdministrator = $this->isTeamAdministrator($team);
			?>
			<div class="project-title">
				<span><?php echo $team->name; ?></span>
			</div>

			<div class="list">
				<?php $this->renderPartial('archivedprojectlist', array('team' => $team, 'projects' => $team->projects)); ?>
			</div><!-- list -->
		<?php endif; ?>
	<?php endforeach; ?>
	
	<!-- Own projects -->
	<?php if(!empty($ownProjects)): ?>
		<?php $atLeastOneProjectArchived = true; ?>
		
		<div class="project-title">
			<span><?php echo Yii::t('project', 'Own projects'); ?></span>
		</div>
		
		<div class="list">
				<?php $this->renderPartial('archivedprojectlist', array('team' => null, 'projects' => $ownProjects)); ?>
		</div><!-- list -->
	<?php endif; ?>
	
	<?php if(!$atLeastOneProjectArchived): ?>
		<div class="description">
			<?php echo Yii::t('project', 'There are no archived projects.'); ?>
		</div>
	<?php endif; ?>
	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/confirmationDialog.js"></script>
		
	<script type="text/javascript">
	/*<![CDATA[*/
		jQuery(document).ready(function()
		{
			function redirectToProjects(data)
			{
				window.location.replace('<?php echo $this->createUrl('project/index'); ?>');
			}
			
			jQuery('.btnDeleteProject').click(function()
			{
				var settings =
				{
					'url': jQuery(this).attr('href'),
					'title': '<?php echo Yii::t('general', 'Confirmation required'); ?>',
					'content': '<?php echo Yii::t('project', 'Are you sure you want to delete this project? Note that this action is irreversible!'); ?>',
					'successCallback': redirectToProjects,
				};
				
				return confirmationDialog(settings);
			});
		});
	/*]]>*/
	</script>
	
</div><!-- project -->
