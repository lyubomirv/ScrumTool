<?php
	$isAdministrator = true;
	if($team !== null)
	{
		$isAdministrator = $this->isTeamAdministrator($team);
	}
?>

<ul>
	<?php foreach($projects as $project): ?>
		<?php if(!$project->active): ?>
			<li>
				<?php if($isAdministrator): ?>
					<div class="left">
						<span class="project-element"><?php echo $project->name; ?></span>
					</div>
					<div class="right">
						<div class="errorMessage"></div>
						<div class="project-actions">
							<a href="<?php echo $this->createUrl('project/retrieve', array('projectId' => $project->id)); ?>"><?php echo Yii::t('project', 'retrieve'); ?></a>
							<a href="<?php echo $this->createUrl('project/delete', array('projectId' => $project->id)); ?>" class="btnDeleteProject"><?php echo Yii::t('project', 'delete'); ?></a>
						</div>
					</div>
					<br style="clear: both;"/>
				<?php else: ?>
					<span class="project-element"><?php echo $project->name; ?></span>
				<?php endif; ?>
			</li>
		<?php endif; ?>
	<?php endforeach; ?>
</ul>
