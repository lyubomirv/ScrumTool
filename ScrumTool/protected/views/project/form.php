<div class="wide form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'project-form',
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'validateOnType' => true,
			),
		)
	);
	?>
		<div class="row">
			<?php echo $form->label($model, 'name'); ?><br />
			<?php echo $form->textField($model, 'name'); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>
		
		<div class="row inline">
			<?php
				$teamsData = array();
				foreach($teams as $team)
				{
					$teamsData += array($team->id => $team->name);
				}
				
				$teamsData = array(-1 => Yii::t('project', 'None')) + $teamsData;
				
				$showTeamSelect = !empty($teamsData);
			?>
			<?php if($showTeamSelect): ?>
				<?php echo $form->label($model, 'teamId'); ?><br />
				<?php echo $form->dropDownList($model, 'teamId', $teamsData); ?>
				<?php echo $form->error($model,'teamId'); ?>
			<?php endif; ?>
		</div>
		
		<?php
			$showAdditionalColumns = !empty($standardColumnNames);
		?>
		<?php if($showAdditionalColumns): ?>
			<span><?php echo Yii::t('project', 'Additional columns'); ?></span>
			<div class="row inline">
				<?php echo $form->label($model, 'useColumn1'); ?>
				<?php echo $form->checkBox($model, 'useColumn1'); ?>
				
				<?php echo $form->textField($model, 'column1Name', array('disabled' => 'disabled')); ?>
				
				<?php echo $form->dropDownList($model, 'column1Relativity', array('before' => Yii::t('project', 'before'), 'after' => Yii::t('project', 'after')), array('disabled' => 'disabled')); ?>
				<?php echo $form->dropDownList($model, 'column1OtherColumn', $standardColumnNames, array('disabled' => 'disabled')); ?>
			</div>
			
			<div class="row inline">
				<?php echo $form->label($model, 'useColumn2'); ?>
				<?php echo $form->checkBox($model, 'useColumn2'); ?>
				
				<?php echo $form->textField($model, 'column2Name', array('disabled' => 'disabled')); ?>
				
				<?php echo $form->dropDownList($model, 'column2Relativity', array('before' => Yii::t('project', 'before'), 'after' => Yii::t('project', 'after')), array('disabled' => 'disabled')); ?>
				<?php echo $form->dropDownList($model, 'column2OtherColumn', $standardColumnNames, array('disabled' => 'disabled')); ?>
			</div>
		<?php endif; ?>
		
		<?php if($showSubmitButton): ?>
			<div class="buttons">
				<?php echo CHTML::submitButton(Yii::t('project', 'Submit')); ?>
			</div>
		<?php endif; ?>
		
	<?php $this->endWidget(); ?>
</div><!-- form -->

<script type="text/javascript">
/*<![CDATA[*/
	jQuery(document).ready(function()
	{
		jQuery('#ProjectForm_column1Name, #ProjectForm_column2Name').each(function()
		{
			var element = jQuery(this);
			var value = element.attr('value');
			element.data('value', value);
		});
		
		function enableColumnElements(columnIndex, enable)
		{
			jQuery('#ProjectForm_column' + columnIndex + 'Name').attr('disabled', !enable);
			jQuery('#ProjectForm_column' + columnIndex + 'Relativity').attr('disabled', !enable);
			jQuery('#ProjectForm_column' + columnIndex + 'OtherColumn').attr('disabled', !enable);
			
			var columnName = jQuery('#ProjectForm_column' + columnIndex + 'Name').attr('value');
			
			if(columnName.length > 0)
			{
				var otherColumnSelectId = '';
				if(columnIndex === 1)
				{
					otherColumnSelectId = 'ProjectForm_column2OtherColumn';
				}
				else if(columnIndex === 2)
				{
					otherColumnSelectId = 'ProjectForm_column1OtherColumn';
				}
				
				if(enable)
				{
					var option = jQuery('<option>')
						.attr('value', columnName)
						.text(columnName);
					
					jQuery('#' + otherColumnSelectId).append(option);
				}
				else
				{
					var option = jQuery('#' + otherColumnSelectId + ' option[value=' + columnName + ']');
					if(option !== null)
					{
						option.remove();
					}
				}
			}
		}
		
		jQuery('#ProjectForm_useColumn1').live('change', function()
		{
			var checkbox = jQuery(this);
			if(checkbox.attr('checked') === 'checked')
			{
				enableColumnElements(1, true);
			}
			else
			{
				enableColumnElements(1, false);
			}
		});
		
		jQuery('#ProjectForm_useColumn2').live('change', function()
		{
			var checkbox = jQuery(this);
			if(checkbox.attr('checked') === 'checked')
			{
				enableColumnElements(2, true);
			}
			else
			{
				enableColumnElements(2, false);
			}
		});
		
		jQuery('#ProjectForm_column1Name').live('change', function()
		{
			var element = jQuery(this);
			var previousValue = element.data('value');
			var value = element.attr('value');
			var option = jQuery('<option>')
				.attr('value', value)
				.text(value);
			
			jQuery('#ProjectForm_column2OtherColumn option[value=' + previousValue + ']').remove();
			
			if(value.length !== 0)
			{
				jQuery('#ProjectForm_column2OtherColumn').append(option);
			}
			
			element.data('value', value);
		});
		
		jQuery('#ProjectForm_column2Name').live('change', function()
		{
			var element = jQuery(this);
			var previousValue = element.data('value');
			var value = element.attr('value');
			var option = jQuery('<option>')
				.attr('value', value)
				.text(value);
			
			jQuery('#ProjectForm_column1OtherColumn option[value=' + previousValue + ']').remove();
			
			if(value.length !== 0)
			{
				jQuery('#ProjectForm_column1OtherColumn').append(option);
			}
			
			element.data('value', value);
		});
	});
/*]]>*/
</script>