<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('project', 'Create project');
?>

<div class="createProject">
	<?php echo $this->renderPartial('form', array('model' => $model, 'showSubmitButton' => $showSubmitButton, 'teams' => $teams, 'standardColumnNames' => $standardColumnNames)); ?>
</div><!-- createProject -->
