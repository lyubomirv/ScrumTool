<?php
	$authManager = Yii::app()->authManager;
	$currentUser = Yii::app()->user;
	$isScrumMaster = $authManager->isAssigned('scrum_master', $currentUser->getId());
?>

<?php if(count($story->tasks) > 0): ?>
	<ul class="task-list">
		<?php foreach($story->tasks as $task): ?>
			<li class="task">
				<div class="left story-task-title">
					<?php
						$taskDescription = $this->getShortenedString($task->description, 50);
					?>
					<span><?php echo $task->name; ?></span><span class="description"><?php echo $taskDescription; ?></span>
				</div>
				<div class="left task-actions">
					<a href="<?php echo $this->createUrl('task/view', array('projectId' => $projectId, 'storyId' => $story->id, 'taskId' => $task->id)); ?>" class="btnViewTask"><?php echo Yii::t('general', 'view'); ?></a>
					<?php if($isScrumMaster): ?>
					<div class="errorMessage"></div>
					<a href="<?php echo $this->createUrl('task/edit', array('projectId' => $projectId, 'storyId' => $story->id, 'taskId' => $task->id)); ?>" class="btnEditTask"><?php echo Yii::t('general', 'edit'); ?></a>
					<a href="<?php echo $this->createUrl('task/delete', array('projectId' => $projectId, 'storyId' => $story->id, 'taskId' => $task->id)); ?>" class="btnDeleteTask"><?php echo Yii::t('general', 'delete'); ?></a>
					<?php endif; ?>
				</div>
				<div class="right">
					<?php if($task->estimation !== null): ?>
						<span><?php echo $task->estimation; ?></span>
					<?php else: ?>
						<span>?</span>
					<?php endif; ?>
				</div>
				<br style="clear: both;" />
			</li>
		<?php endforeach; ?>
	</ul>
<?php else: ?>
	<span>There are no tasks here yet.</span>
<?php endif; ?>
