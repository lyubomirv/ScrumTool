<div class="wide form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'story-form',
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'validateOnType' => true,
			),
		)
	);
	?>
		<div class="row">
			<?php echo $form->label($model, 'name'); ?><br />
			<?php echo $form->textField($model, 'name'); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>
		
		<div class="row">
			<?php echo $form->label($model, 'description'); ?><br />
			<?php echo $form->textArea($model, 'description'); ?>
			<?php echo $form->error($model, 'description'); ?>
		</div>
		
		<div class="row">
			<?php echo $form->label($model, 'priority'); ?><br />
			<?php echo $form->textField($model, 'priority'); ?>
			<?php echo $form->error($model, 'priority'); ?>
		</div>
		
		<?php if($showSubmitButton): ?>
			<div class="buttons">
				<?php echo CHTML::submitButton('Submit'); ?>
			</div>
		<?php endif; ?>
		
	<?php $this->endWidget(); ?>
</div><!-- form -->
