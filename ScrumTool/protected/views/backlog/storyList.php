<?php
	$authManager = Yii::app()->authManager;
	$currentUser = Yii::app()->user;
	$isProductOwner = $authManager->isAssigned('product_owner', $currentUser->getId());
?>

<div class="actions">
	<a id="btnCollapseAll" href="#"><?php echo Yii::t('general', 'collapse all'); ?></a> |
	<a id="btnExpandAll" href="#"><?php echo Yii::t('general', 'expand all'); ?></a>
</div>

<ul class="story-list">
	<?php foreach($project->stories as $story): ?>
		<li id="<?php echo $story->id; ?>" class="story">
			<div class="left story-title">
				<?php
					$storyDescription = $this->getShortenedString($story->description, 50);
				?>
				<a href="#" class="story story-tasks-show"><span class="title"><?php echo $story->name ?></span><span class="description"><?php echo $storyDescription; ?></span></a>
			</div>
			<div class="left story-actions">
				<a href="<?php echo $this->createUrl('backlog/viewStory', array('projectId' => $project->id, 'storyId' => $story->id)); ?>" class="btnViewStory"><?php echo Yii::t('general', 'view'); ?></a>
				<?php if($isProductOwner): ?>
				<div class="errorMessage"></div>
				<a href="<?php echo $this->createUrl('backlog/editStory', array('projectId' => $project->id, 'storyId' => $story->id)); ?>" class="btnEditStory"><?php echo Yii::t('general', 'edit'); ?></a>
				<a href="<?php echo $this->createUrl('backlog/deleteStory', array('projectId' => $project->id, 'storyId' => $story->id)); ?>" class="btnDeleteStory"><?php echo Yii::t('general', 'delete'); ?></a>
				<?php endif; ?>
			</div>
			<div class="right">
				<?php if($story->priority !== null): ?>
					<?php echo $story->priority; ?>
				<?php else: ?>
					?
				<?php endif; ?>
			</div>
			<br style="clear: both;" />
			<div class="story-tasks">
				<a href="<?php echo $this->createUrl('task/add', array('projectId' => $project->id, 'storyId' => $story->id)) ?>" class="btnAddTask"><?php echo Yii::t('backlog', 'Add task'); ?></a>
				<div class="task-list">
					<?php $this->renderPartial('taskList', array('projectId' => $project->id, 'story' => $story)); ?>
				</div>
			</div>
		</li>
	<?php endforeach; ?>
</ul>
