<?php
$this->pageTitle = Yii::app()->name . ' - Add story';
?>

<div class="addStory">
	<?php echo $this->renderPartial('storyForm', array('model' => $model, 'showSubmitButton' => $showSubmitButton)); ?>
</div><!-- addStory -->
