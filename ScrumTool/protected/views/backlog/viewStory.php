<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('backlog', 'View story');
?>

<div class="backlog">
	<div class="view">
		<span class="name"><?php echo $story->name; ?></span><br/>
		<br/>
		<br/>
		<span class="description"><?php echo $story->description; ?></span><br/>
		<br/>
		<br/>
		<?php echo Yii::t('backlog', 'Priority:'); ?> <span class="description"><?php echo $story->priority !== null ? $story->priority : '?' ?></span><br/>
	</div>
</div>
