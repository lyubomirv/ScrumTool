<?php
$this->pageTitle = Yii::app()->name . ' - ' . $project->name . ' - Backlog';
?>

<?php
	$authManager = Yii::app()->authManager;
	$currentUser = Yii::app()->user;
	$isProductOwner = $authManager->isAssigned('product_owner', $currentUser->getId());
	$isScrumMaster = $authManager->isAssigned('scrum_master', $currentUser->getId());
?>

<div class="centered-div backlog">

	<div class="backlog-title">
		<span><?php echo Yii::t('backlog', '{project} backlog', array('{project}' => $project->name)); ?></span>
	</div>
	
	<?php if($isProductOwner): ?>
		<a href="<?php echo $this->createUrl('backlog/addStory', array('projectId' => $project->id)); ?>" id="btnAddStory" class="button"><?php echo Yii::t('backlog', 'Add story'); ?></a>
	<?php endif; ?>
	
	<div id="backlog" class="list">
		<?php if(count($project->stories) > 0): ?>
			<?php $this->renderPartial('storyList', array('project' => $project)); ?>
		<?php else: ?>
			<div class="backlog-title">
				<?php if($isProductOwner): ?>
					<span><?php echo Yii::t('backlog', 'This project doesn\'t seem to have any stories in its backlog yet. You can add one by clicking on the "Add story" button.'); ?></span>
				<?php else: ?>
					<span>This project doesn't seem to have any stories in its backlog yet. Contact your Product owner so he/she can add some.</span>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div><!-- list -->
	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/jquery-ui/jquery-ui.min.js"></script>
	<?php if($isProductOwner || $isScrumMaster): ?>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/formDialog.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/confirmationDialog.js"></script>
	<?php endif; ?>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/infoDialog.js"></script>
	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/backlog/storyList.js"></script>
	
	<script type="text/javascript">
	/*<![CDATA[*/
		jQuery(document).ready(function()
		{
			jQuery('#btnCollapseAll').live('click', function()
			{
				jQuery('.story-tasks').slideUp(300);
				return false;
			});
			
			jQuery('#btnExpandAll').live('click', function()
			{
				jQuery('.story-tasks').slideDown(300);
				return false;
			});
			
			// View story
			jQuery('.btnViewStory').live('click', function()
			{
				var settings =
				{
					'contentUrl': jQuery(this).attr('href'),
					'title': '<?php echo Yii::t('backlog', 'View story'); ?>',
				};
				
				return createInfoDialog(settings);
			});

			<?php if($isProductOwner): ?>
				function updateBacklog(data)
				{
					var storyList = jQuery('#backlog');
					storyList.load('<?php echo $this->createUrl('backlog/storyList', array('projectId' => $project->id)); ?>', function()
					{
						jQuery('.story-tasks').hide();
						jQuery('.story-actions, .task-actions').hide();
					});
				}
				
				// Add story
				jQuery('#btnAddStory').live('click', function()
				{
					var settings =
					{
						'contentUrl': jQuery(this).attr('href'),
						'actionRender': 'story-render',
						'actionSubmit': 'story-submit',
						'submitSuccessCallback': updateBacklog,
						'title': '<?php echo Yii::t('backlog', 'Add story'); ?>'
					};
					
					return createFormDialog(settings);
				});
				
				// Edit story
				jQuery('.btnEditStory').live('click', function()
				{
					var settings =
					{
						'contentUrl': jQuery(this).attr('href'),
						'actionRender': 'story-render',
						'actionSubmit': 'story-submit',
						'submitSuccessCallback': updateBacklog,
						'title': 'Edit story'
					};
					
					return createFormDialog(settings);
				});
				
				// Delete story
				jQuery('.btnDeleteStory').live('click', function()
				{
					var settings=
					{
						'url': jQuery(this).attr('href'),
						'title': '<?php echo Yii::t('general', 'Confirmation required'); ?>',
						'content': '<?php echo Yii::t('backlog', 'Are you sure you want to delete this story?'); ?>',
						'successCallback': updateBacklog
					};
					
					return confirmationDialog(settings);
				});
			<?php endif; ?>
			
			// View task
			jQuery('.btnViewTask').live('click', function()
			{
				var settings =
				{
					'contentUrl': jQuery(this).attr('href'),
					'title': '<?php echo Yii::t('backlog', 'View task'); ?>',
				};
				
				return createInfoDialog(settings);
			});
			
			<?php if($isScrumMaster): ?>
				/**
				 * Updates the list of tasks of the story
				 * @param taskList Object the div that holds the story's tasks
				 * @param data Object object to send to the server
				 */
				function updateStory(taskList, storyId)
				{
					jQuery.ajax(
					{
						type: 'POST',
						url: '<?php echo $this->createUrl('backlog/taskList', array('projectId' => $project->id)); ?>?storyId=' + storyId,
						dataType: 'html',
						context: taskList,
						success: function(html)
						{
							taskList.html(html);
							jQuery('.task-actions').hide();
						}
					});
				}
				
				// Add task
				jQuery('.btnAddTask').live('click', function()
				{
					var thisElement = jQuery(this);
					var taskList = thisElement.next('.task-list');
					var storyId = taskList.parents('.story').attr('id');
					
					var settings =
					{
						'contentUrl': jQuery(this).attr('href'),
						'actionRender': 'task-render',
						'actionSubmit': 'task-submit',
						'submitSuccessCallback': function(data)
						{
							updateStory(taskList, storyId);
						},
						'title': '<?php echo Yii::t('backlog', 'Add task'); ?>'
					};
					
					return createFormDialog(settings);
				});
				
				// Edit task
				jQuery('.btnEditTask').live('click', function()
				{
					var thisElement = jQuery(this);
					var taskList = thisElement.parents('.task-list');
					var storyId = taskList.parents('.story').attr('id');
					
					var settings =
					{
						'contentUrl': jQuery(this).attr('href'),
						'actionRender': 'task-render',
						'actionSubmit': 'task-submit',
						'submitSuccessCallback': function(data)
						{
							updateStory(taskList, storyId);
						},
						'title': 'Edit task'
					};
					
					return createFormDialog(settings);
				});
				
				// Delete task
				jQuery('.btnDeleteTask').live('click', function()
				{
					var thisElement = jQuery(this);
					var taskList = thisElement.parents('.task-list');
					var storyId = taskList.parents('.story').attr('id');
					
					var settings=
					{
						'url': jQuery(this).attr('href'),
						'title': '<?php echo Yii::t('general', 'Confirmation required'); ?>',
						'content': '<?php echo Yii::t('backlog', 'Are you sure you want to delete this task?'); ?>',
						'successCallback': function(data)
						{
							updateStory(taskList, storyId);
						},
					};
					
					return confirmationDialog(settings);
				});
			<?php endif; ?>
		});
	/*]]>*/
	</script>
	
</div><!-- backlog -->
