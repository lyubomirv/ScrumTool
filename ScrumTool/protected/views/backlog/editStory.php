<?php
$this->pageTitle = Yii::app()->name . ' - Edit story';
?>

<div class="editStory">
	<?php echo $this->renderPartial('storyForm', array('model' => $model, 'showSubmitButton' => $showSubmitButton)); ?>
</div><!-- editStory -->
