<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo Yii::app()->language; ?>" lang="<?php echo Yii::app()->language; ?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="en<?php if(Yii::app()->language !== 'en') echo ', ' . Yii::app()->language; ?>" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui-themes/ui-default/jquery.ui.all.css" />
	
	<?php
		$baseUrl = Yii::app()->baseUrl;
		$clientScript = Yii::app()->getClientScript();
		$clientScript->registerScriptFile($baseUrl . '/assets/javascript/jquery.min.js');
	?>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
	<?php
		$user = !Yii::app()->user->isGuest ? User::model()->findByPk(Yii::app()->user->getId()) : null;
		$userPrettyName = '';
		if($user !== null)
		{
			if($user->name !== null && strlen($user->name) > 0)
			{
				$userPrettyName = $user->name;
			}
			else
			{
				$userPrettyName = $user->username;
			}
		}
	?>

	<div class="page">
		
		<div class="header">
			<div class="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
			<ul class="mainmenu">
				<li><a href="<?php echo $this->createUrl('/index'); ?>"><?php echo Yii::t('mainmenu', 'Home'); ?></a></li>
				<?php if(Yii::app()->user->isGuest): ?>
					<li><a href="<?php echo $this->createUrl('index/login'); ?>"><?php echo Yii::t('mainmenu', 'Sign in'); ?></a></li>
				<?php else: ?>
					<?php if(!Yii::app()->user->isGuest): ?>
						<li>
							<a class="dropdown" href="#"><?php echo $userPrettyName; ?> <span>&#x25BE;</span></a>
							<ul class="dropdown">
								<li><span class="bigger"><?php echo $userPrettyName; ?></span></li>
								<li><?php echo Yii::t('mainmenu', 'Username: {username}', array('{username}' => Yii::app()->user->name)); ?></li>
								<li class="separator"></li>
								<li><a href="<?php echo $this->createUrl('/team'); ?>"><?php echo Yii::t('mainmenu', 'Teams'); ?></a></li>
								<li><a href="<?php echo $this->createUrl('/project'); ?>"><?php echo Yii::t('mainmenu', 'Projects'); ?></a></li>
								<li class="separator"></li>
								<li><a href="<?php echo $this->createUrl('index/logout'); ?>"><?php echo Yii::t('mainmenu', 'Sign out'); ?></a></li>
							</ul>
						</li>
					<?php endif; ?>
				<?php endif; ?>
				
				<li>
					<a class="dropdown" href="#"><?php echo Yii::t('mainmenu', 'Options'); ?> <span>&#x25BE;</span></a>
					<ul class="dropdown options">
						<li>
							<?php echo Yii::t('mainmenu', 'Language:'); ?>
							<ul class="language">
								<li class="first"><a href="<?php echo $this->createUrl('index/selectLanguage', array('language' => 'bg')) ?>">bg</a></li>
								<li><a href="<?php echo $this->createUrl('index/selectLanguage', array('language' => 'en_us')) ?>">en</a></li>
							</ul>
						</li>
					</ul>
				</li>
			</ul><!-- mainmenu -->
			<br class="clearboth"/>
		</div><!-- header -->
		
		<div class="content">
			<?php echo $content; ?>
		</div><!-- content -->

		<div class="push">
		</div>
	</div><!-- page -->

	<div class="footer">
		<?php echo Yii::t('general', 'Copyright &copy; {date} by Lyubomir Vasilev.<br />All Rights Reserved.<br />', array('{date}' => date('Y'))); ?>
	</div><!-- footer -->

</body>
</html>