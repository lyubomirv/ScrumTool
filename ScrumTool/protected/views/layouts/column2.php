<?php $this->beginContent('//layouts/main'); ?>
<div class="two-column-wrap">
	<div class="two-column-left">
		<?php echo $content; ?>
	</div>
	<div class="two-column-right">
		<?php if($this->hasCurrentProject()): ?>
			<?php $project = $this->getCurrentProject(); ?>
			<div class="rightmenu">
				<div class="rightmenu-title">
					<span><?php echo $project->name; ?></span>
				</div>
				<ul>
					<li><a href="<?php echo $this->createUrl('backlog/index', array('projectId' => $project->id)); ?>"><?php echo Yii::t('rightmenu', 'Backlog'); ?></a></li>
					<li><a href="<?php echo $this->createUrl('sprintPlan/index', array('projectId' => $project->id)); ?>"><?php echo Yii::t('rightmenu', 'Sprint planning'); ?></a></li>
					<li><a href="<?php echo $this->createUrl('history/index', array('projectId' => $project->id)); ?>"><?php echo Yii::t('rightmenu', 'Sprint history'); ?></a></li>
				</ul>
			</div><!-- rightmenu -->
			
			<?php
				$sprint = null;
				if($this->hasCurrentSprint())
				{
					$sprint = $this->getSprintSafe($this->getCurrentSprintId(), $project->id);
				}
				else
				{
					$sprint = $this->getLatestSprint($project->id);
				}
			?>
			<?php if($sprint !== null): ?>
				<div class="rightmenu">
					<div class="rightmenu-title">
						<span><?php echo $sprint->name; ?></span>
					</div>
					<ul>
						<li><a href="<?php echo $this->createUrl('dashboard/index', array('projectId' => $project->id, 'sprintId' => $sprint->id)); ?>"><?php echo Yii::t('rightmenu', 'Dashboard'); ?></a></li>
						<li><a href="<?php echo $this->createUrl('board/index', array('projectId' => $project->id, 'sprintId' => $sprint->id)); ?>"><?php echo Yii::t('rightmenu', 'Board'); ?></a></li>
					</ul>
				</div><!-- rightmenu -->
			<?php endif; ?>
		<?php else: ?>
			<div class="rightmenu">
				<div class="rightmenu-title">
					<span><?php echo Yii::t('rightmenu', 'No project selected'); ?></span>
				</div>
			</div>
		<?php endif; ?>
	</div><!-- right -->
</div><!-- two-column-wrap -->
<?php $this->endContent(); ?>