<ul>
	<?php foreach($sprints as $sprint): ?>
		<li>
			<?php
				$dateStart = $sprint->date_start;
				if(empty($dateStart))
				{
					$dateStart = '?';
				}
				else
				{
					$dateStart = date('Y/m/d', strtotime($dateStart));
				}
				$dateEnd = $sprint->date_end;
				if(empty($dateEnd))
				{
					$dateEnd = '?';
				}
				else
				{
					$dateEnd = date('Y/m/d', strtotime($dateEnd));
				}
				$element = '<a href="' . $this->createUrl('board/index', array('projectId' => $projectId, 'sprintId' => $sprint->id)) . '" class="sprint-element">';
				$element .= $sprint->name . '<span class="description">(' . $dateStart . ' - ' . $dateEnd . ')</span>';
				$element .= '</a>';
			?>
			<?php echo $element; ?>
		</li>
	<?php endforeach; ?>
</ul>
