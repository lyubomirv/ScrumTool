<?php
$this->pageTitle = Yii::app()->name . ' - '. Yii::t('history', 'History');
?>

<div class="centered-div history">
	
	<div class="history-title">
		<span><?php echo Yii::t('history', 'Sprints'); ?></span>
	</div>
	
	<div class="history-description">
		<span><?php echo Yii::t('history', 'Please select a sprint to view it.'); ?></span>
	</div>
	
	<div class="list">
		<?php if(count($sprints) > 0): ?>
			<?php $this->renderPartial('sprintList', array('projectId' => $projectId, 'sprints' => $sprints)); ?>
		<?php else: ?>
			<div class="description">
				<span><?php echo Yii::t('history', 'There are no previous sprints in this project.'); ?></span>
			</div>
		<?php endif; ?>
	</div><!-- list -->

</div><!-- history -->
