<?php
	$authManager = Yii::app()->authManager;
	$currentUser = Yii::app()->user;
	$isProductOwner = $authManager->isAssigned('product_owner', $currentUser->getId());
?>

<ul class="story-list">
	<?php foreach($project->stories as $story): ?>
		<li class="story">
			<div class="left story-title">
				<a href="#" class="story story-tasks-show"><?php echo $story->name ?></a>
			</div>
			<div class="left story-actions">
				<a href="<?php echo $this->createUrl('backlog/viewStory', array('projectId' => $project->id, 'storyId' => $story->id)); ?>" class="btnViewStory"><?php echo Yii::t('general', 'view'); ?></a>
			</div>
			<div class="right">
				<?php if($story->priority !== null): ?>
					<?php echo $story->priority; ?>
				<?php else: ?>
					?
				<?php endif; ?>
			</div>
			<br style="clear: both;" />
			<div class="story-tasks">
				<?php if(count($story->tasks) > 0): ?>
					<?php $this->renderPartial('storyTaskList', array('project' => $project, 'story' => $story, 'sprint' => $sprint)); ?>
				<?php else: ?>
					<h5>There are no tasks here yet.</h5>
				<?php endif; ?>
			</div>
		</li>
	<?php endforeach; ?>
</ul>
