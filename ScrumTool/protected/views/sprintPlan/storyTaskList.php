<?php
	$authManager = Yii::app()->authManager;
	$currentUser = Yii::app()->user;
	$isScrumMaster = $authManager->isAssigned('scrum_master', $currentUser->getId());
?>

<ul class="task-list">
	<?php foreach($story->tasks as $task): ?>
		<?php
			$addedTask = false;
			$inAnotherSprint = false;
			$otherSprintName = '';
			
			if($sprint !== null)
			{
				if($task->sprintTasks !== null && count($task->sprintTasks) > 0)
				{
					if($task->sprintTasks[0]->sprint->id === $sprint->id)
					{
						$addedTask = true;
					}
					else
					{
						$inAnotherSprint = true;
						$otherSprintName = $task->sprintTasks[0]->sprint->name;
					}
				}
			}
		?>
		<li class="story-task<?php if($addedTask) echo ' added-task'; ?><?php if($inAnotherSprint) echo ' othersprint-task'; ?>">
			<div class="left story-task-title">
				<?php if(!$inAnotherSprint): ?>
					<?php if($task->estimation !== null): ?>
						<a href="#" class="btnMoveTask task" task="<?php echo $task->id; ?>"><?php echo $task->name; ?></a>
					<?php else: ?>
						<span><?php echo $task->name; ?></span><span class="description"><?php echo Yii::t('sprintplan', 'No estimation'); ?></span>
					<?php endif; ?>
				<?php else: ?>
					<span><?php echo $task->name; ?></span><span class="description"><?php echo Yii::t('sprintplan', 'In {othersprint}', array('{othersprint}' => $otherSprintName)); ?></span>
				<?php endif; ?>
			</div>
			<div class="left task-actions">
				<a href="<?php echo $this->createUrl('task/view', array('projectId' => $project->id, 'storyId' => $story->id, 'taskId' => $task->id)); ?>" class="btnViewTask"><?php echo Yii::t('general', 'view'); ?></a>
			</div>
			<div class="right">
				<?php if($task->estimation !== null): ?>
					<span><?php echo $task->estimation; ?></span>
				<?php else: ?>
					<span>?</span>
				<?php endif; ?>
			</div>
			<br style="clear: both;" />
		</li>
	<?php endforeach; ?>
</ul>
