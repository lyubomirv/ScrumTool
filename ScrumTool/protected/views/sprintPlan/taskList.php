<?php
	Yii::import('application.classes.SprintHelper');
	
	$authManager = Yii::app()->authManager;
	$currentUser = Yii::app()->user;
	$isScrumMaster = $authManager->isAssigned('scrum_master', $currentUser->getId());
	
	$sprintHelper = new SprintHelper($sprint);
	$hasSprintStarted = $sprintHelper->hasSprintStarted();
?>

<ul class="task-list">
	<?php foreach($sprint->sprintTasks as $sprintTask): ?>
		<?php
			$task = $sprintTask->task;
			$warnBeforeRemoval = $sprintHelper->hasTaskBeenMoved($task->id);
		?>
		<li class="story-task added-task<?php if($warnBeforeRemoval) echo ' warn-remove'; ?>">
			<div class="left task-title">
				<a href="#" class="btnMoveTask task" task="<?php echo $task->id; ?>"><?php echo $task->name; ?></a>
			</div>
			<div class="left task-actions">
				<a href="<?php echo $this->createUrl('task/view', array('projectId' => $projectId, 'storyId' => $task->story->id, 'taskId' => $task->id)); ?>" class="btnViewTask"><?php echo Yii::t('general', 'view'); ?></a>
			</div>
			<div class="right">
				<?php if($task->estimation !== null): ?>
					<span><?php echo $task->estimation; ?></span>
				<?php else: ?>
					<span>?</span>
				<?php endif; ?>
			</div>
			<br style="clear: both;" />
		</li>
	<?php endforeach; ?>
</ul>
