<?php
	$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('sprintplan', 'Sprint plan');
?>

<?php
	$authManager = Yii::app()->authManager;
	$currentUser = Yii::app()->user;
	$isScrumMaster = $authManager->isAssigned('scrum_master', $currentUser->getId());
?>

<div class="centered-div sprint-plan">

	<div id="backlog" class="sprint-plan-backlog">
		<div class="title">
			<span><?php echo Yii::t('sprintplan', 'Backlog'); ?></span>
		</div>
		<?php if(count($project->stories) > 0): ?>
			<div class="actions">
				<a id="btnCollapseAll" href="#"><?php echo Yii::t('general', 'collapse all'); ?></a> |
				<a id="btnExpandAll" href="#"><?php echo Yii::t('general', 'expand all'); ?></a>
			</div>
			
			<?php $this->renderPartial('storyList', array('project' => $project, 'sprint' => $selectedSprint)); ?>
		<?php else: ?>
			<span><?php echo Yii::t('sprintplan', 'There are no stories in the backlog. Stories can be added in the {backlog-start}Backlog{backlog-end}.', array('{backlog-start}' => '<a href="' . $this->createUrl('backlog/index', array('projectId' => $project->id)) . '">', '{backlog-end}' => '</a>')); ?></span>
		<?php endif; ?>
	</div>
	<div id="sprint" class="sprint-plan-sprint">
		<div class="title">
			<span><?php echo Yii::t('sprintplan', 'Sprint'); ?></span>
		</div>
		<div class="sprint-list">
			<?php if(count($project->sprints) > 0): ?>
				<div class="form">
					<select id="select_sprint">
						<?php foreach($project->sprints as $sprint): ?>
							<?php
								$selected = '';
								if($selectedSprint !== null && $selectedSprint->id === $sprint->id)
								{
									$selected = ' selected="selected"';
								}
							?>
							<?php if($isScrumMaster): ?>
								<option value="<?php echo $this->createUrl('sprintPlan/index', array('projectId' => $project->id, 'sprintId' => $sprint->id)); ?>"<?php echo $selected; ?>><?php echo $sprint->name; ?></option>
							<?php endif; ?>
						<?php endforeach; ?>
						<option value="sprint-create"><?php echo Yii::t('sprintplan', 'Add sprint...'); ?></option>
					</select>
					
					<a id="btnCreateSprint" href="<?php echo $this->createUrl('sprint/create', array('projectId' => $project->id)); ?>"></a>
				</div>
			<?php else: ?>
				<span><?php echo Yii::t('sprintplan', 'This project does not have any sprints yet.'); ?></span>
				<?php if($isScrumMaster): ?>
					<a id="btnCreateSprint" href="<?php echo $this->createUrl('sprint/create', array('projectId' => $project->id)); ?>"><?php echo Yii::t('sprintplan', 'Add sprint'); ?></a>
				<?php endif; ?>
			<?php endif; ?>
			<br style="clear: both;" />
		</div>
		<div id="sprint_task_list" class="sprint-task-list">
			<?php if($selectedSprint !== null): ?>
				<?php if(count($selectedSprint->sprintTasks) > 0): ?>
					<?php $this->renderPartial('taskList', array('projectId' => $project->id, 'sprint' => $selectedSprint)); ?>
				<?php else: ?>
					<span class="title"><?php echo Yii::t('sprintplan', 'There are no tasks in this sprint yet.'); ?></span>
				<?php endif; ?>
			<?php else: ?>
				<br/>
				<span><?php echo Yii::t('sprintplan', 'Please create or select a sprint to be able to plan it.'); ?></span>
			<?php endif; ?>
		</div>
	</div>
	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/jquery-ui/jquery-ui.min.js"></script>
	<?php if($isScrumMaster): ?>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/formDialog.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/confirmationDialog.js"></script>
	<?php endif; ?>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/infoDialog.js"></script>
	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/backlog/storyList.js"></script>
	
	<script type="text/javascript">
	/*<![CDATA[*/
		jQuery(document).ready(function()
		{
			jQuery('#btnCollapseAll').live('click', function()
			{
				jQuery('.story-tasks').slideUp(300);
				return false;
			});
			
			jQuery('#btnExpandAll').live('click', function()
			{
				jQuery('.story-tasks').slideDown(300);
				return false;
			});
			
			jQuery('.btnViewStory').live('click', function()
			{
				var settings =
				{
					'contentUrl': jQuery(this).attr('href'),
					'title': '<?php echo Yii::t('backlog', 'View story'); ?>',
				};
				
				return createInfoDialog(settings);
			});
			
			jQuery('.btnViewTask').live('click', function()
			{
				var settings =
				{
					'contentUrl': jQuery(this).attr('href'),
					'title': '<?php echo Yii::t('backlog', 'View task'); ?>',
				};
				
				return createInfoDialog(settings);
			});
			
			jQuery('#select_sprint').live('change', function()
			{
				var url = jQuery(this).attr('value');
				
				if(url === 'sprint-create')
				{
					jQuery('#btnCreateSprint').click();
				}
				else
				{
					window.location.replace(url);
				}
			});
			
			<?php if($isScrumMaster): ?>
				jQuery('#btnCreateSprint').live('click', function()
				{
					var settings =
					{
						'contentUrl': jQuery(this).attr('href'),
						'actionRender': 'sprint-render',
						'actionSubmit': 'sprint-submit',
						'submitSuccessCallback': function()
						{
							window.location.replace('<?php echo $this->createUrl('sprintPlan/index', array('projectId' => $project->id)); ?>');
						},
						'title': '<?php echo Yii::t('sprintplan', 'Sprint plan'); ?>'
					};
					
					return createFormDialog(settings);
				});
				
				<?php if($selectedSprint !== null): ?>
					function updateSprintTasks()
					{
						var sprintTaskList = jQuery('#sprint_task_list');
						sprintTaskList.load('<?php echo $this->createUrl('sprintPlan/taskList', array('projectId' => $project->id, 'sprintId' => $selectedSprint->id)); ?>', function()
						{
							// Updates hovering over the tasks
							jQuery('#sprint_task_list .task-actions').hide();
						});
					}
					
					jQuery('.btnMoveTask').live('click', function()
					{
						var thisButton = jQuery(this);
						var thisTask = thisButton.parents('.story-task');
						var taskId = jQuery(this).attr('task')
						
						var moveData = 'Tasks[0]=' + jQuery(this).attr('task');
						
						var addedTaskClass = 'added-task';
						var addTask = !thisTask.hasClass(addedTaskClass);
						
						var successCallback = function(data)
						{
							jQuery('.btnMoveTask').each(function()
							{
								if(jQuery(this).attr('task') === taskId)
								{
									thisTask = jQuery(this).parent().parent();
									
									if(addTask)
									{
										thisTask.addClass(addedTaskClass);
									}
									else
									{
										thisTask.removeClass(addedTaskClass);
									}
								}
							});
							
							updateSprintTasks();
						}
						
						var ajaxCall = function(requestUrl, moveData, successCallback)
						{
							jQuery.ajax(
							{
								type: 'POST',
								url: requestUrl,
								data: moveData,
								success: successCallback
							});
						}
						
						var requestUrl = '';
						var sendRequest = true;
						if(addTask)
						{
							requestUrl = '<?php echo $this->createUrl('sprint/add', array('projectId' => $project->id, 'sprintId' => $selectedSprint->id)); ?>';
							
							ajaxCall(requestUrl, moveData, successCallback);
						}
						else
						{
							requestUrl = '<?php echo $this->createUrl('sprint/remove', array('projectId' => $project->id, 'sprintId' => $selectedSprint->id)); ?>';
							
							var warnRemoveClass = 'warn-remove';
							var warnBeforeRemove = thisTask.hasClass(warnRemoveClass);
							if(warnBeforeRemove)
							{
								var settings =
								{
									'url': requestUrl,
									'parameters': moveData,
									'title': '<?php echo Yii::t('general', 'Confirmation required'); ?>',
									'okText': '<?php echo Yii::t('general', 'Remove'); ?>',
									'cancelText': '<?php echo Yii::t('general', 'Cancel'); ?>',
									'content': '<?php echo Yii::t('sprintplan', 'This task has already been moved around during the sprint. Are you sure you want to remove it from the sprint?'); ?>',
									'successCallback': successCallback
								};
								
								confirmationDialog(settings);
							}
							else
							{
								ajaxCall(requestUrl, moveData, successCallback);
							}
						}
						
						return false;
					});
				<?php endif; ?>
			<?php endif; ?>
		});
	/*]]>*/
	</script>
	
</div><!-- project -->
