<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('board', '{sprint} board', array('{sprint}' => $sprint->name));
?>

<div class="centered-div sprint-board">

	<div class="sprint-board-title">
		<span><?php echo Yii::t('board', '{sprint} board', array('{sprint}' => $sprint->name)); ?></span>
	</div>
	
	<div class="board">
		<table>
			<tr>
				<?php $columnWidthPercent = 100 / count($columns); ?>
				<?php foreach($columns as $column): ?>
					<th style="width: <?php echo $columnWidthPercent; ?>%;"><?php echo Yii::t('columns', $column->name); ?></th>
				<?php endforeach; ?>
			</tr>
			<tr>
				<?php foreach($columns as $column): ?>
					<td>
						<div id="column_<?php echo $column->id; ?>" class="column">
							<?php $columnTasks = $sprintColumnTasks[$column->id]; ?>
							<?php $this->renderPartial('columnTaskList', array('projectId' => $projectId, 'sprint' => $sprint, 'columnTasks' => $columnTasks, 'taskStoryPoints' => $taskStoryPoints, 'taskAssignees' => $taskAssignees, 'currentUserName' => $currentUserName)); ?>
						</div>
					</td>
				<?php endforeach; ?>
			</tr>
		</table>
	</div>
	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/common.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/infoDialog.js"></script>
	<script type="text/javascript">
	/*<![CDATA[*/
		jQuery(document).ready(function()
		{
			jQuery('.btnViewTask').live('click', function()
			{
				var settings =
				{
					'contentUrl': jQuery(this).attr('href'),
					'title': 'View task',
				};
				
				return createInfoDialog(settings);
			});
			
			/**
			 * Load the list of tasks for a column
			 * @param columnId string the element id of the column on the page
			 */
			function loadColumn(columnId)
			{
				var url = createUrl('<?php echo $this->createUrl('board/columnTaskList', array('projectId' => $projectId, 'sprintId' => $sprint->id)); ?>', {columnId: columnId});
				var column = jQuery('#column_' + columnId);
				column.load(url);
			}
			
			// Variables used to track the state of the task dragging
			var oldList;	// Set when sorting starts
			var newList;	// Set on every change
			var item;		// The item being dragged
			jQuery('.column').sortable(
			{
				'containment': 'document',
				'connectWith': '.column',
				'scroll': false,
				'tollerance': 'pointer',
				'placeholder': 'placeholder',
				'forcePlaceholderSize': true,
				'cursor': 'move',
				'start': function(event, ui)
				{
					item = ui.item;
					newList = oldList = ui.item.parent();
				},
				'change': function(event, ui)
				{
					if(ui.sender !== null)
					{
						newList = ui.placeholder.parent();
					}
				},
				'stop': function(event, ui)
				{
					var senderList = jQuery(oldList);
					var senderListItemIds = senderList.sortable('toArray');
					var receiverList = jQuery(newList);
					var receiverListItemIds = receiverList.sortable('toArray');
					
					var index = item.index();
					var previousId = -1;
					
					if(index > 0)
					{
						previousId = getTaskId(receiverListItemIds[index - 1]);
					}
					
					if(senderList.attr('id') === receiverList.attr('id'))
					{
						// The move is in the same column
						var data =
						{
							taskId: getTaskId(receiverListItemIds[index]),
							previousId: previousId,
						};
						
						jQuery.ajax(
						{
							type: 'POST',
							url: createUrl('<?php echo $this->createUrl('board/rearrangeColumnTask', array('projectId' => $projectId, 'sprintId' => $sprint->id)); ?>'),
							data: data,
							dataType: 'json',
							success: function(data)
							{
								//loadColumn(receiverColumnId);
							}
						});
					}
					else
					{
						// The move is from one column to another
						var senderColumnId = getColumnId(senderList.attr('id'));
						var receiverColumnId = getColumnId(receiverList.attr('id'));
						
						var data =
						{
							taskId: getTaskId(receiverListItemIds[index]),
							fromColumnId: senderColumnId,
							toColumnId: receiverColumnId,
							previousId: previousId,
						};
						
						jQuery.ajax(
						{
							type: 'POST',
							url: createUrl('<?php echo $this->createUrl('board/move', array('projectId' => $projectId, 'sprintId' => $sprint->id)); ?>'),
							data: data,
							dataType: 'json',
							success: function(data)
							{
								//loadColumn(senderColumnId);
								//loadColumn(receiverColumnId);
							}
						});
					}
					
					// Log some stuff
					/*
					console.log(event);
					console.log(ui);
					console.log('index in array: ' + item.index());
					console.log('sender column id: ' + senderList.attr('id'));
					
					console.log('receiver column id: ' + receiverList.attr('id'));
					
					var id = receiverList.sortable('toArray')[item.index()];
					console.log('task id: ' + id);
					
					console.log('previous id: ' + previousId);
					
					console.log('sender list: ' + senderList.sortable('toArray'));
					console.log('receiver list: ' + receiverList.sortable('toArray'));
					*/
				},
			});
			
			/**
			 * Implements the assignment and unassigment of tasks
			 * @param jQueryObj jQuery object of the button that was clicked
			 */
			function assignmentImpl(jQueryObj)
			{
				var url = jQueryObj.attr('href');
				
				var task = jQueryObj.parents('.task');
				var taskId = getTaskId(task.attr('id'));
				
				var column = jQueryObj.parents('.column');
				var columnId = getColumnId(column.attr('id'));
				
				var data =
				{
					taskId: taskId
				};
				
				jQuery.ajax(
				{
					type: 'POST',
					url: url,
					data: data,
					dataType: 'json',
					success: function(data)
					{
						loadColumn(columnId);
					}
				});
				
				return false;
			}
			
			jQuery('.btnAssign').live('click', function()
			{
				return assignmentImpl(jQuery(this));
			});
			
			jQuery('.btnUnassign').live('click', function()
			{
				return assignmentImpl(jQuery(this));
			});
			
			/**
			 * Get the id of the task by its element id.
			 */
			function getTaskId(elementId)
			{
				return stripPrefix('task_', elementId);
			}
			
			/**
			 * Get the id of the column by its element id.
			 */
			function getColumnId(elementId)
			{
				return stripPrefix('column_', elementId);
			}
		});
	/*]]>*/
	</script>
	
</div><!-- sprint-board -->
