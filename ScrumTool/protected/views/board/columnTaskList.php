<?php if($columnTasks !== null): ?>
	<?php foreach($columnTasks as $columnTask): ?>
		<?php $task = $columnTask->task; ?>
		<div id="task_<?php echo $task->id; ?>" class="task">
			<div class="task-title">
				<span><?php echo $task->name; ?></span>
			</div>
			<div class="task-estimation">
				<span>
					<?php 
						if(isset($taskStoryPoints[$task->id]))
						{
							echo $taskStoryPoints[$task->id];
						}
						else
						{
							echo '0';
						}
					?>
					/ <?php echo $task->estimation; ?>
				</span>
			</div>
			<br style="clear: both;" />
			<div class="task-actions">
				<span><a href="<?php echo $this->createUrl('task/view', array('projectId' => $projectId, 'storyId' => $task->story->id, 'taskId' => $task->id)); ?>" class="btnViewTask"><?php echo Yii::t('general', 'view'); ?></a></span>
				<?php if(isset($taskAssignees[$task->id]) && in_array($currentUserName, $taskAssignees[$task->id])): ?>
					<span><a href="<?php echo $this->createUrl('board/unassign', array('projectId' => $projectId, 'sprintId' => $sprint->id)); ?>" class="btnUnassign"><?php echo Yii::t('board', 'unassign me'); ?></a></span>
				<?php else: ?>
					<span><a href="<?php echo $this->createUrl('board/assign', array('projectId' => $projectId, 'sprintId' => $sprint->id)); ?>" class="btnAssign"><?php echo Yii::t('board', 'assign me'); ?></a></span>
				<?php endif; ?>
			</div>
			<div class="task-assignees">
				<span>
					<?php if(isset($taskAssignees[$task->id])): ?>
						<?php foreach($taskAssignees[$task->id] as $i => $assignee): ?>
							<?php if($i !== 0): ?>
								<?php echo ', '; ?>
							<?php endif; ?>
							
							<?php echo $assignee; ?>
						<?php endforeach; ?>
					<?php else: ?>
						<?php echo Yii::t('board', 'Not assigned'); ?>
					<?php endif; ?>	
				</span>
			</div>
			<br style="clear: both;" />
		</div>
	<?php endforeach; ?>
<?php endif; ?>
