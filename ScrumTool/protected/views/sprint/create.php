<?php
$this->pageTitle = Yii::app()->name . ' - Create sprint';
?>

<div class="createSprint">
	<?php echo $this->renderPartial('form', array('model' => $model, 'showSubmitButton' => $showSubmitButton)); ?>
</div><!-- createSprint -->
