<div class="wide form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'sprint-form',
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'validateOnType' => true,
			),
		)
	);
	?>
		<div class="row">
			<?php echo $form->label($model, 'name'); ?><br />
			<?php echo $form->textField($model, 'name'); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>
		
		<div class="row">
			<?php echo $form->label($model, 'story_points'); ?><br />
			<?php echo $form->textField($model, 'story_points'); ?>
			<?php echo $form->error($model,'story_points'); ?>
		</div>
		
		<div class="row">
			<?php echo $form->label($model, 'date_start'); ?><br />
			<?php echo $form->textField($model, 'date_start'); ?>
			<?php echo $form->error($model, 'date_start'); ?>
		</div>
		
		<div class="row">
			<?php echo $form->label($model, 'date_end'); ?><br />
			<?php echo $form->textField($model, 'date_end'); ?>
			<?php echo $form->error($model, 'date_end'); ?>
		</div>
		
		<?php if($showSubmitButton): ?>
			<div class="buttons">
				<?php echo CHTML::submitButton(Yii::t('sprint', 'Save')); ?>
			</div>
		<?php endif; ?>
		
	<?php $this->endWidget(); ?>
	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/jquery-ui/jquery-ui.min.js"></script>
	
	<?php if(Yii::app()->language === 'bg'): ?>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/jquery-ui/i18n/jquery.ui.datepicker-bg.js"></script>
	<?php endif; ?>
	
	<script type="text/javascript">
	/*<![CDATA[*/
		jQuery(function()
		{
			jQuery('#SprintForm_date_start').datepicker(
			{
				'dateFormat': 'yy/mm/dd',
				'minDate': '-1w',
				'maxDate': '+1m',
				'onSelect': function(dateText, inst)
				{
					jQuery('#SprintForm_date_end').datepicker('option', 'minDate', dateText);
				}
			});
			
			jQuery('#SprintForm_date_end').datepicker(
			{
				'dateFormat': 'yy/mm/dd',
				'minDate': '-1w',
				'maxDate': '+1m',
				'onSelect': function(dateText, inst)
				{
					jQuery('#SprintForm_date_start').datepicker('option', 'maxDate', dateText);
				}
			});
		});
	/*]]>*/
	</script>
</div><!-- form -->
