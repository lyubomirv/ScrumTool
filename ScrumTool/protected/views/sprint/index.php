<?php
$this->pageTitle = Yii::app()->name . ' - ' . $team->name . ' - ' . $project->name . ' Sprint plan';
?>

<?php
	$authManager = Yii::app()->authManager;
	$currentUser = Yii::app()->user;
	$isScrumMaster = $authManager->isAssigned('scrum_master', $currentUser->getId());
?>

<div class="centered-div sprintPlan">

	<h3><?php echo $project->name ?> Sprint plan:</h3>
	
	<div id="backlog" style="float: left; width: 50%; min-width: 100px; background: yellow;">
		<?php if(count($project->stories) > 0): ?>
			<?php $this->renderPartial('storyList', array('project' => $project)); ?>
		<?php else: ?>
			<h3>There are no stories in the backlog</h3>
		<?php endif; ?>
	</div>
	<div id="sprint" style="float: right; width: 50%; min-width: 100px; background: green;">
		<div class="sprint-list">
			<div class="left select-sprint">
				<?php if(count($project->sprints) > 0): ?>
					<select id="select_sprint">
						<?php foreach($project->sprints as $sprint): ?>
							<?php
								$selected = '';
								if($selectedSprint !== null && $selectedSprint->id === $sprint->id)
								{
									$selected = ' selected="selected"';
								}
							?>
							<option value="<?php echo $sprint->id; ?>"<?php echo $selected; ?>><?php echo $sprint->name; ?></option>
						<?php endforeach; ?>
					</select>
				<?php else: ?>
					<h5>This project does not have any sprints yet.</h5>
				<?php endif; ?>
			</div>
			<div class="right sprint-actions">
				<?php if($selectedSprint !== null) echo $sprint->id; ?>
				<?php if($isScrumMaster): ?>
					<a href="#" id="btnAddSprint">Add sprint</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/jquery-ui/jquery-ui.min.js"></script>
	<?php if($isScrumMaster): ?>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/formDialog.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/confirmationDialog.js"></script>
	<?php endif; ?>
	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/backlog/storyList.js"></script>
	
	<script type="text/javascript">
	/*<![CDATA[*/
		jQuery(document).ready(function()
		{
			jQuery('#select_sprint').live('change', function()
			{
				var sprintId = jQuery(this).attr('value');
				alert(sprintId);
				window.location.replace('<?php echo Yii::app()->request->baseUrl; ?>/sprintPlan?sprint=' + sprintId);
			});
			
			<?php if($isScrumMaster): ?>
				jQuery('#btnAddSprint').live('click', function()
				{
					var settings =
					{
						'contentUrl': '<?php echo Yii::app()->request->baseUrl; ?>/sprint/create',
						'actionRender': 'sprint-render',
						'actionSubmit': 'sprint-submit',
						'submitSuccessCallback': null,
						'title': 'Create sprint'
					};
					
					return createFormDialog(settings);
				});
				// TODO
			<?php endif; ?>
		});
	/*]]>*/
	</script>
	
</div><!-- project -->
