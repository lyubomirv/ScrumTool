<?php
$this->pageTitle = Yii::app()->name . ' - Edit sprint';
?>

<div class="editSprint">
	<?php echo $this->renderPartial('form', array('model' => $model, 'showSubmitButton' => $showSubmitButton)); ?>
</div><!-- editSprint -->
