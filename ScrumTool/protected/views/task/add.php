<?php
$this->pageTitle = Yii::app()->name . ' - Add task';
?>

<div class="addtask">
	<?php echo $this->renderPartial('form', array('model' => $model, 'showSubmitButton' => $showSubmitButton)); ?>
</div><!-- addStory -->
