<?php
	$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('backlog', 'View task');
?>

<div class="backlog">
	<div class="view">
		<span class="name"><?php echo $task->name; ?></span><br/>
		<br/>
		<br/>
		<span class="description"><?php echo $task->description; ?></span><br/>
		<br/>
		<br/>
		<?php echo Yii::t('backlog', 'Estimation:'); ?> <span class="description"><?php echo $task->estimation !== null ? $task->estimation : '?' ?></span><br/>
	</div>
</div>
