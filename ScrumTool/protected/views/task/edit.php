<?php
$this->pageTitle = Yii::app()->name . ' - Edit task';
?>

<div class="editTask">
	<?php echo $this->renderPartial('form', array('model' => $model, 'showSubmitButton' => $showSubmitButton)); ?>
</div><!-- editStory -->
