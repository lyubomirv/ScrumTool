<?php
$this->pageTitle = Yii::app()->name;
?>

<div class="centered-div member-index">
	<div class="title">
		<span><?php echo Yii::t('index', 'Welcome to {appname}, {username}!', array('{appname}' => Yii::app()->name, '{username}' => $name)); ?></span>
	</div>
	
	<div class="directions">
		<div class="direction teams">
			<?php if($hasTeams): ?>
				<?php echo Yii::t('index', 'You are already participating in one or more teams. Visit your teams\' pages to get more information.'); ?>
			<?php else: ?>
				<?php echo Yii::t('index', 'You are still not participating in any teams. Create one by visiting the teams page.'); ?>
			<?php endif; ?>
			<br />
			<br />
			<div class="link">
				<a href="<?php echo $this->createUrl('team/index'); ?>" class="button"><?php echo Yii::t('mainmenu', 'Teams'); ?></a>
			</div>
		</div>
		
		<div class="direction projects">
			<?php if($hasProjects): ?>
				<?php echo Yii::t('index', 'You are already taking active role in one or more projects. Visit your projects\' page to continue you work.'); ?>
			<?php else: ?>
				<?php echo Yii::t('index', 'You are still not working on any projects. Create one by visiting the projects page.'); ?>
			<?php endif; ?>
			<br />
			<br />
			<div class="link">
				<a href="<?php echo $this->createUrl('project/index'); ?>" class="button"><?php echo Yii::t('mainmenu', 'Projects'); ?></a>
			</div>
		</div>
		
		<br class="clearboth" />
	</div>
</div> <!-- index -->
