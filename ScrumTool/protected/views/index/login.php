<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('login', 'Sign in');
?>

<div class="centered-div signin">

	<div class="form">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'login-form',
			)
		);
		?>
			<span><?php echo Yii::t('login', 'Sign in'); ?></span>
			<div class="row">
				<?php echo $form->label($model, 'username'); ?>
				<?php echo $form->textField($model, 'username', array('autofocus' => 'autofocus')); ?>
				<?php echo $form->error($model, 'username'); ?>
			</div>
			
			<div class="row">
				<?php echo $form->label($model, 'password'); ?>
				<?php echo $form->passwordField($model, 'password'); ?>
				<?php echo $form->error($model, 'password'); ?>
			</div>
			
			<div class="row inline">
				<?php echo $form->checkBox($model, 'rememberMe'); ?>
				<?php echo $form->label($model, 'rememberMe'); ?>
			</div>
			
			<div class="row">
				<?php echo CHTML::submitButton(Yii::t('login', 'Sign in')); ?>
				<a href="<?php echo $this->createUrl('index/register'); ?>"><?php echo Yii::t('login', 'Register'); ?></a>
			</div>
			
		<?php $this->endWidget(); ?>
	</div><!-- form -->

</div><!-- login -->
