<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('register', 'Register');
?>

<div class="centered-div register">

	<div class="form">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'register-form',
			'enableAjaxValidation' => true,
			'enableClientValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'validateOnChange' => true,
				'validateOnType' => true,
				),
			)
		);
		?>
			<span><?php echo Yii::t('register', 'Register'); ?></span>
			
			<div class="row">
				<?php echo $form->label($model, 'username'); ?>
				<?php echo $form->textField($model, 'username', array('autofocus' => 'autofocus')); ?>
				<?php echo $form->error($model,'username'); ?>
			</div>
			
			<div class="row">
				<?php echo $form->label($model, 'password'); ?>
				<?php echo $form->passwordField($model, 'password'); ?>
				<?php echo $form->error($model,'password'); ?>
			</div>
			
			<div class="row">
				<?php echo $form->label($model, 'password2'); ?>
				<?php echo $form->passwordField($model, 'password2'); ?>
				<?php echo $form->error($model,'password2'); ?>
			</div>
			
			<div class="row">
				<?php echo $form->label($model, 'email'); ?>
				<?php echo $form->textField($model, 'email'); ?>
				<?php echo $form->error($model,'email'); ?>
			</div>
			
			<div class="row">
				<?php echo $form->label($model, 'name'); ?>
				<?php echo $form->textField($model, 'name'); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>
			
			<div class="buttons">
				<?php echo CHTML::submitButton(Yii::t('register', 'Register')); ?>
			</div>
		<?php $this->endWidget(); ?>
	</div><!-- form -->

</div><!-- register -->
