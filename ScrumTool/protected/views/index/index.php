<?php
$this->pageTitle = Yii::app()->name;
?>

<div class="centered-div index">
	<div class="title">
		<span style="font-size: 110%;"><?php echo Yii::app()->name; ?></span><br />
		<span><?php echo Yii::t('index', 'This is how Scrum should be done!'); ?></span>
	</div>
	
	<div class="describe-system">
		<div class="description">
			<p class="heading"><?php echo Yii::t('index', '{appname} provides you with an easy and intuitive approach to managing your agile projects.', array('{appname}' => Yii::app()->name)); ?></p>
			<p class="desc"><?php echo Yii::t('index', '{appname} addresses the needs of each Scrum role and helps taking more benefit of the Scrum process by providing easy and friendly enviroment for managing the Scrum artifacts.', array('{appname}' => Yii::app()->name)); ?></p>
		</div>
		<div class="img">
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/showcase.png" />
		</div>
		
		<br class="clearboth" />
	</div>
	
	<br class="clearboth" />
	
	<div class="subtitle">
		<p><?php echo Yii::t('index', 'What you will find in {appname}:', array('{appname}' => Yii::app()->name)); ?></p>
	</div>
	
	<div style="margin: 0 auto; max-width: 90%;">
		<div class="describe-backlog">
			<div class="description">
				<p><h4><?php echo Yii::t('index', 'Manage your Product Backlog efficiently'); ?></h4><br /><?php echo Yii::t('index', 'Create User Stories and set a priority to each story based on what you think its business value is. Split the story into manageable and doable tasks the way you prefer. Set the tasks\' estimation that will later affect the sprint planning.'); ?></p>
			</div>
			<div class="img">
				<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/backlog.png" />
			</div>
		</div>
		
		<div class="describe-board">
			<div class="description">
				<p><h4><?php echo Yii::t('index', 'Change task status with only a click'); ?></h4><br /><?php echo Yii::t('index', 'View the tasks, selected for a sprint at a sprint board. With its clean design and usable interface, you can set change status of a task by simply moving it through the columns with a simple drag with the mouse. No refreshing the page needed.'); ?></p>
			</div>
			<div class="img">
				<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/board.png" />
			</div>
		</div>
		
		<div class="describe-charts">
			<div class="description">
				<p><h4><?php echo Yii::t('index', 'See the Product status at a glance'); ?></h4><br /><?php echo Yii::t('index', 'Take a look at charts, describing the Product\'s status and giving you visual feedback of the progress. See the sprint Burndown chart to get a view of where the sprint is going. Or have expectations on how much the team can accomplish based on their sprint history, visible in the Velocity chart.'); ?></p>
			</div>
			<div class="img">
				<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/burndown.png" />&nbsp;&nbsp;&nbsp;
				<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/velocity.png" />
			</div>
		</div>
		
		<br class="clearboth" />
		
		<div class="bottom-message">
			<p><?php echo Yii::t('index', 'You can start using {appname} right now. {register-start}Register{register-end} or {signin-start}Sign in{signin-end}, if you already have an accout.', array('{appname}' => Yii::app()->name, '{register-start}' => '<a href="' . $this->createUrl('index/register') . '">', '{register-end}' => '</a>', '{signin-start}' => '<a href="' . $this->createUrl('index/login') . '">', '{signin-end}' => '</a>')); ?></p>
		</div>
	</div>
</div> <!-- index -->
