<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('team', 'Add member');
?>

<div id="addMember">

<div class="wide form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'addMember-form',
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'validateOnType' => true,
			),
		)
	);
	?>
		<div class="row">
			<?php echo $form->label($model, 'username'); ?>
			<?php echo $form->textField($model, 'username'); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>
		
		<?php if($showSubmitButton): ?>
			<div class="buttons">
				<?php echo CHTML::submitButton(Yii::t('team', 'Add member')); ?>
			</div>
		<?php endif; ?>
	<?php $this->endWidget(); ?>
</div><!-- form -->

</div><!-- addMember -->
