<?php
	$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('team', 'Team');
?>

<?php
	$authManager = Yii::app()->authManager;
	$currentUser = Yii::app()->user;
	$isAdministrator = $authManager->isAssigned('team_administrator', $currentUser->getId());
?>

<div class="centered-div team">

	<?php if(empty($teams)): ?>
		<div class="team-title">
			<span><?php echo Yii::t('team', 'You are not a member of any team.'); ?></span>
		</div>
		
		<div>
			<a id="btnCreateTeam" href="<?php echo $this->createUrl('team/createTeam'); ?>" class="button"><?php echo Yii::t('team', 'Create team'); ?></a>
		</div>
	<?php else: ?>
		<div class="team-select">
			<div class="form">
				<span><?php echo Yii::t('team', 'Team:'); ?></span>
				<select id="select_team">
					<?php foreach($teams as $t): ?>
						<?php
							$selected = '';
							if($team !== null && $team->id === $t->id)
							{
								$selected = ' selected="selected"';
							}
						?>
						<option value="<?php echo $this->createUrl('team/index', array('teamId' => $t->id)); ?>"<?php echo $selected; ?>><?php echo $t->name; ?></option>
					<?php endforeach; ?>
					<option value="team-create"><?php echo Yii::t('team', 'Create team ...'); ?></option>
				</select>
			</div>
		</div>
		
		<a id="btnCreateTeam" href="<?php echo $this->createUrl('team/createTeam'); ?>"></a>
		
		<?php if($team !== null): ?>
			<div id="team">
				<?php $this->renderPartial('teamTable', array('team' => $team)); ?>
			</div><!-- team -->

			<div class="left">
				<?php if($isAdministrator): ?>
					<a id="btnAddMember" href="<?php echo $this->createUrl('team/addMember', array('teamId' => $team->id)); ?>" class="button"><?php echo Yii::t('team', 'Add member'); ?></a>
				<?php endif; ?>
			</div>
			
			<div class="right">
				<div class="errorMessage"></div>
				<a id="btnLeave" href="<?php echo $this->createUrl('team/leave', array('teamId' => $team->id)); ?>" class="button"><?php echo Yii::t('team', 'Leave'); ?></a>
			</div>
			<br style="clear: both;"/>
		<?php endif; ?>
	<?php endif; ?>
	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/formDialog.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/confirmationDialog.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/common.js"></script>

	<script type="text/javascript">
	/*<![CDATA[*/
		jQuery(document).ready(function()
		{
			jQuery('#select_team').live('change', function()
			{
				var url = jQuery(this).attr('value');
				
				if(url === 'team-create')
				{
					jQuery('#btnCreateTeam').click();
				}
				else
				{
					window.location.replace(url);
				}
			});
			
			jQuery('#btnCreateTeam').live('click', function()
			{
				var settings =
				{
					'contentUrl': jQuery(this).attr('href'),
					'actionRender': 'createTeam-render',
					'actionSubmit': 'createTeam-submit',
					'submitSuccessCallback': function(data)
					{
						var url = '<?php echo $this->createUrl('team/index'); ?>';
						if(data.teamId !== undefined)
						{
							url = createUrl(url, {teamId: data.teamId});
						}
						
						window.location.replace(url);
					},
					'title': '<?php echo Yii::t('team', 'Create team'); ?>'
				};
				
				return createFormDialog(settings);
			});
			
			<?php if($team !== null): ?>
				<?php if($isAdministrator): ?>
					function updateTeamMembers(data)
					{
						var teamTable = jQuery('#team');
						teamTable.load('<?php echo $this->createUrl('team/teamTable', array('teamId' => $team->id)); ?>');
					}
					
					// Add member
					jQuery('#btnAddMember').live('click', function()
					{
						var settings =
						{
							'contentUrl': jQuery(this).attr('href'),
							'actionRender': 'addMember-render',
							'actionSubmit': 'addMember-submit',
							'submitSuccessCallback': updateTeamMembers,
							'title': '<?php echo Yii::t('team', 'Add member'); ?>'
						};
						
						return createFormDialog(settings);
					});
					
					// Remove member from the team
					jQuery('.btnRemoveMember').live('click', function()
					{
						var settings =
						{
							'url': jQuery(this).attr('href'),
							'title': '<?php echo Yii::t('general', 'Confirmation required'); ?>',
							'content': '<?php echo Yii::t('team', 'Are you sure you want to remove this member?'); ?>',
							'successCallback': updateTeamMembers
						};
						
						return confirmationDialog(settings);
					});
				<?php endif; ?>
				
				// Leave the team
				jQuery('#btnLeave').click(function()
				{
					var errorMessageDiv = jQuery(this).siblings('.errorMessage');
					
					var settings =
					{
						'url': jQuery(this).attr('href'),
						'title': '<?php echo Yii::t('general', 'Confirmation required'); ?>',
						'content': '<?php echo Yii::t('team', 'Are you sure you want to leave the team?'); ?>',
						'successCallback': function(data)
						{
							window.location.replace('<?php echo $this->createUrl('team/index'); ?>');
						},
						'errorCallback': function(data)
						{
							errorMessageDiv.removeAttr('style');
							errorMessageDiv.html(data.message);
						}
					};
					
					return confirmationDialog(settings);
				});
			<?php endif; ?>
		});
	/*]]>*/
	</script>
	
</div><!-- team -->
