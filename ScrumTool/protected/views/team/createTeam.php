<?php
$this->pageTitle = Yii::app()->name . ' - '. Yii::t('team', 'Create team');
?>

<div class="createTeam">

<div class="wide form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'createTeam-form',
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'validateOnType' => true,
			),
		)
	);
	?>
		<div class="row">
			<?php echo $form->label($model, 'teamName'); ?>
			<?php echo $form->textField($model, 'teamName'); ?>
			<?php echo $form->error($model,'teamName'); ?>
		</div>
		
		<?php if($showSubmitButton): ?>
			<div class="buttons">
				<?php echo CHTML::submitButton(Yii::t('team', 'Create team')); ?>
			</div>
		<?php endif; ?>
	<?php $this->endWidget(); ?>
</div><!-- form -->

</div><!-- addMember -->
