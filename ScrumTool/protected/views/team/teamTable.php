<?php
	$authManager = Yii::app()->authManager;
	$currentUser = Yii::app()->user;
	$isAdministrator = $authManager->isAssigned('team_administrator', $currentUser->getId());
?>
	
<table>
	<tr class="border-bottom">
		<th><?php echo Yii::t('team', 'User'); ?></th>
		<th><?php echo Yii::t('team', 'E-mail'); ?></th>
		<!--<th><?php echo Yii::t('team', 'Roles'); ?></th>-->
		<?php if($isAdministrator): ?>
			<th><?php echo Yii::t('team', 'Actions'); ?></th>
		<?php endif; ?>
	</tr>
	<?php foreach($team->teamMembers as $teamMember): ?>
	<tr>
		<?php $user = $teamMember->user; ?>
		<td><?php echo strlen($user->name) > 0 ? $user->name : $user->username ?></td>
		<td><?php echo $user->email ?></td>
		<!--<td>TODO</td>-->
		<td>
			<?php if($isAdministrator && Yii::app()->user->getId() !== $user->id): ?>
				<a href="<?php echo $this->createUrl('team/removeMember', array('teamId' => $team->id, 'userId' => $user->id)); ?>" class="btnRemoveMember"><?php echo Yii::t('team', 'Remove'); ?></a>
			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach ?>
</table>
