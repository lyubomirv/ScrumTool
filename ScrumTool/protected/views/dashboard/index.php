<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('dashboard', 'Dashboard');
?>

<div class="centered-div dashboard">

	<?php
		$points = array_values($dateRemainingPoints);
		$drawBurnDownChart = (!empty($points) && $points[0] !== 0);
		
		$drawVelocityChart = !empty($sprintCompletedPoints);
	?>
	
	<?php if($drawBurnDownChart): ?>
		<table id="burn-down-data">
			<thead>
				<tr>
					<?php foreach($dateRemainingPoints as $date => $points): ?>
						<th><?php echo date('m-d', $date); ?></th>
					<?php endforeach; ?>
				</tr>
			</thead>
			<tbody>
				<tr>
					<?php foreach($dateRemainingPoints as $date => $points): ?>
						<td><?php echo $points; ?></td>
					<?php endforeach; ?>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<?php foreach($dateRemainingPoints as $date => $points): ?>
						<th><?php if(!empty($points)) { echo Yii::t('dashboard', '{points} story points', array('{points}' => $points)); } ?></th>
					<?php endforeach; ?>
				</tr>
			</tfoot>
		</table>
	<?php endif; ?>
	
	<?php if($drawVelocityChart): ?>
		<table id="velocity-data">
			<thead>
				<tr>
					<?php foreach($sprintCompletedPoints as $sprint => $points): ?>
						<th><?php echo $sprint; ?></th>
					<?php endforeach; ?>
				</tr>
			</thead>
			<tbody>
				<tr>
					<?php foreach($sprintCompletedPoints as $sprint => $points): ?>
						<td><?php echo $points; ?></td>
					<?php endforeach; ?>
				</tr>
			</tbody>
		</table>
	<?php endif; ?>
	
	<div class="burndown-area">
		<div class="dashboard-title">
			<span><?php echo Yii::t('dashboard', 'Burn-down'); ?></span>
		</div>
		
		<?php if(!$drawBurnDownChart): ?>
			<span class="description"><?php echo Yii::t('dashboard', 'There is not enough data to draw a burn-down chart.'); ?></span>
		<?php endif; ?>
		
		<div id="burndown-chart">
		</div>
	</div>
	
	<div class="velocity-area">
		<div class="dashboard-title">
			<p><?php echo Yii::t('dashboard', 'Velocity'); ?></p>
		</div>
		
		<?php if(!$drawVelocityChart): ?>
			<span class="description"><?php echo Yii::t('dashboard', 'There is not enough data to draw a velocity chart.'); ?></span>
		<?php endif; ?>
		
		<div id="velocity-chart">
		</div>
	</div>
	
	<br class="clearboth" />
	
	<?php if($drawBurnDownChart || $drawVelocityChart): ?>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/jquery-ui/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/raphael-min.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/chart/burndown.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/javascript/chart/velocity.js"></script>
		<script type="text/javascript">
		/*<![CDATA[*/
			jQuery(document).ready(function()
			{
				<?php if($drawBurnDownChart): ?>
					var burndownWidth = jQuery('.dashboard').width() / 2 - 1;
					var burndownHeight = 250;
					
					// Get chart data
					var burndownLabels = [];
					var burndownValues = [];
					var burndownValueInformation = [];
						
					jQuery('#burn-down-data thead th').each(function ()
					{
						burndownLabels.push(jQuery(this).html());
					});
					
					jQuery('#burn-down-data tbody td').each(function ()
					{
						var val = jQuery(this).html();
						if(val !== null && val.length > 0)
						{
							burndownValues.push(val);
						}
					});
					
					jQuery('#burn-down-data tfoot th').each(function ()
					{
						var val = jQuery(this).html();
						if(val !== null && val.length > 0)
						{
							burndownValueInformation.push(val);
						}
					});
					
					// Hide the table
					jQuery('#burn-down-data').hide();
					
					if(burndownLabels.length === 0 || burndownValues.length === 0)
					{
						return;
					}
					
					var burnDownChart = BurndownChart('burndown-chart', burndownWidth, burndownHeight, burndownLabels, burndownValues, burndownValueInformation);
					burnDownChart.drawChart();
				<?php endif; ?>
				<?php if($drawVelocityChart): ?>
					var velocityWidth = jQuery('.dashboard').width() / 2 - 1;
					var velocityHeight = 250;
					
					// Get chart data
					var velocityLabels = [];
					var velocityValues = [];
						
					jQuery('#velocity-data thead th').each(function ()
					{
						velocityLabels.push(jQuery(this).html());
					});
					
					jQuery('#velocity-data tbody td').each(function ()
					{
						var val = jQuery(this).html();
						if(val !== null && val.length > 0)
						{
							velocityValues.push(val);
						}
					});
					
					// Hide the table
					jQuery('#velocity-data').hide();
					
					if(velocityLabels.length === 0 || velocityValues.length === 0)
					{
						return;
					}
					
					var velocityChart = VelocityChart('velocity-chart', velocityWidth, velocityHeight, velocityLabels, velocityValues);
					velocityChart.drawChart();
				<?php endif; ?>
			});
		/*]]>*/
		</script>
	<?php endif; ?>
	
</div><!-- sprint-board -->
