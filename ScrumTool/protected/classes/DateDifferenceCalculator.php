<?php

/**
 * DateDifferenceCalculator
 * Calculates the difference between two dates
 */
class DateDifferenceCalculator
{
	private $_year;
	private $_month;
	private $_day;
	private $_hour;
	private $_minute;
	private $_second;
	
	private $_offsetFromUTC;
	private $_calculationPerformed = false;
	
	public function __construct()
	{
		$currentDate = new DateTime(null);
		$this->_offsetFromUTC = $currentDate->getOffset();
	}
	
	/**
	 * Clears the calculated data, allowing the object to be reused.
	 */
	public function clear()
	{
		$this->_year = 0;
		$this->_month = 0;
		$this->_day = 0;
		$this->_hour = 0;
		$this->_minute = 0;
		$this->_second = 0;
		
		$this->_calculationPerformed = false;
	}
	
	/**
	 * Calculates the difference between two dates.
	 * Both dates must be in the format 'yyyy-MM-dd hh:mm:ss' and must be in
	 * the UTC timezone.
	 * @param start string the start date
	 * @param start string the end date
	 */
	public function calculate($start, $end = null)
	{
		$dateStart = strtotime($start);
		$dateEnd = null;
		if($end === null)
		{
			$dateEnd = time() - $this->_offsetFromUTC;
		}
		else
		{
			$dateEnd = strtotime($end);
		}
		
		$dateDifference = $dateEnd - $dateStart;
		
		$this->_second = $dateDifference;
		$this->_minute = $dateDifference / 60;
		$this->_hour = $dateDifference / (60 * 60);
		$this->_day = $dateDifference / (60 * 60 * 24);
		$this->_month = $dateDifference / (60 * 60 * 24 * 30);
		$this->_year = $dateDifference / (60 * 60 * 24 * 365);
		
		$this->_calculationPerformed = true;
		
		return true; 
	}
	
	/**
	 * Returns the difference in days
	 * @return float the days
	 */
	public function getDays()
	{
		if($this->_calculationPerformed === false)
		{
			throw new CException('calculateDifference must be called first.');
		}
		
		return $this->_day;
	}
}
