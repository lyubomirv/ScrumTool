<?php

Yii::import('application.classes.DateHelper');

/**
 * SprintHelper
 * Helps with the sprints
 */
class SprintHelper
{
	private $_sprint;
	private $_dateHelper;
	
	public function __construct($sprint)
	{
		$this->_sprint = $sprint;
		$this->_dateHelper = new DateHelper;
	}
	
	/**
	 * Checks whether the sprint has already started
	 * @return bool whether the sprint has already started
	 */
	public function hasSprintStarted()
	{
		$currentTime = $this->_dateHelper->getCurrentDateTime();
		$sprintStartTimeRaw = strtotime($this->_sprint->date_start);
		$sprintStartTime = $this->_dateHelper->getDateTimeAsUTC($sprintStartTimeRaw);
		
		return $sprintStartTime < $currentTime;
	}
	
	/**
	 * Checks whether the sprint is in progress
	 * @return bool whether the sprint is in progress
	 */
	public function isSprintInProgress()
	{
		$currentTime = $this->_dateHelper->getCurrentDateTime();
		$sprintEndTimeRaw = strtotime($this->_sprint->date_end);
		$sprintEndTime = $this->_dateHelper->getDateTimeAsUTC($sprintEndTimeRaw);
		
		return $this->hasSprintStarted() && $currentTime < $sprintEndTime;
	}
	
	/**
	 * Checks whether the sprint has finished
	 * @return bool whether the sprint has finished
	 */
	public function hasSprintFinished()
	{
		$currentTime = $this->_dateHelper->getCurrentDateTime();
		$sprintEndTimeRaw = strtotime($this->_sprint->date_end);
		$sprintEndTime = $this->_dateHelper->getDateTimeAsUTC($sprintEndTimeRaw);
		
		return $sprintEndTime < $currentTime;
	}
	
	/**
	 * Checks whether the task has been moved in the columns
	 * @return bool whether the task has been moved
	 */
	public function hasTaskBeenMoved($taskId)
	{
		return SprintTaskMove::model()->exists(
			'sprint_id = :sprintId AND task_id = :taskId',
			array(':sprintId' => $this->_sprint->id, ':taskId' => $taskId)
			);
	}
}
