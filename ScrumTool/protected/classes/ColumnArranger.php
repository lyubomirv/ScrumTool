<?php

/**
 * ColumnArranger - arranges project columns in the right order
 */
class ColumnArranger
{
	/**
	 * Get all the project's columns by combining the standard ones with
	 * the project-specific ones and arranging them appropriately
	 * @param projectId int id of the project
	 * @return ProjectColumn[] array of all the columns for the project
	 */
	public function getArrangedColumns($projectId)
	{
		// Get the standard columns - they are the first four in the database
		$standardColumns = ProjectColumn::model()->findAllByPk(
			array(1, 2, 3, 4)
			);
		// Get the project specific columns
		$projectColumns = ProjectColumn::model()->findAllByAttributes(
			array('project_id' => $projectId)
			);
		
		/*
		//Testing
		$projectColumn1 = new ProjectColumn;
		$projectColumn1->id = '25';
		$projectColumn1->name = 'test1';
		$projectColumn1->previous_column_id = '1';
		
		$projectColumn2 = new ProjectColumn;
		$projectColumn2->id = '22';
		$projectColumn2->name = 'test2';
		$projectColumn2->previous_column_id = '25';
		
		$projectColumn3 = new ProjectColumn;
		$projectColumn3->id = '21';
		$projectColumn3->name = 'test3';
		$projectColumn3->previous_column_id = '4';
		
		array_push($projectColumns, $projectColumn1);
		array_push($projectColumns, $projectColumn2);
		array_push($projectColumns, $projectColumn3);
		
		// Should be: To do, test2, test1, In progress, Review, Testing, Done, test3
		*/
		
		if(count($projectColumns) === 0)
		{
			return $standardColumns;
		}
		
		$projectColumns = $this->sortColumns($projectColumns);
		
		return $this->mergeColumns($standardColumns, $projectColumns);
	}
	
	/**
	 * Checks the array of columns for an element with previous_column_id equal to $previousColumnId
	 * @param columns ProjectColumn[] the columns array
	 * @param previousColumnId int the previous_column_id to search for
	 * @return int index of the column in the array or -1 if such is not found
	 */
	private function hasColumnWithPreviousId($columns, $previousColumnId)
	{
		foreach($columns as $i => $column)
		{
			if((int)$column->previous_column_id === (int)$previousColumnId)
			{
				return $i;
			}
		}
		
		return -1;
	}
	
	/**
	 * Arranges columns taking into account their previous_column_id
	 * This means that if a column's previous_column_id is the id of another column in
	 * the array, then the first one will be moved after the second one.
	 * @param columns ProjectColumn[] the columns to sort
	 * @return ProjectColumn[] sorted columns
	 */
	private function sortColumns($columns)
	{
		for($i = 0; $i < count($columns); $i++)
		{
			for($j = $i + 1; $j < count($columns); $j++)
			{
				if((int)$columns[$i]->previous_column_id === (int)$columns[$j]->id)
				{
					list($columns[$i], $columns[$j]) = array($columns[$j], $columns[$i]);
				}
			}
		}
		
		return $columns;
	}
	
	/**
	 * Merge the standard columns with the project-specific columns
	 * This method will arrange the columns depending on their previous_column_id.
	 * The project-specific columns should be already arranged respecting their previous_column_id.
	 * @param standardColumns ProjectColumn[] the standard columns
	 * @param projectColumns ProjectColumn[] the project-specific columns
	 * @return ProjectColumn[] merged columns
	 */
	private function mergeColumns($standardColumns, $projectColumns)
	{
		$scInd = 0;
		$allColumns = array();
		
		$projectColumnIndex = $this->hasColumnWithPreviousId($projectColumns, 0);
		if($projectColumnIndex !== -1)
		{
			array_push($allColumns, $projectColumns[$projectColumnIndex]);
			unset($projectColumns[$projectColumnIndex]);
		}
		else
		{
			array_push($allColumns, $standardColumns[$scInd++]);
		}
		
		while($scInd < count($standardColumns))
		{
			$projectColumnIndex = $this->hasColumnWithPreviousId(
				$projectColumns,
				$allColumns[count($allColumns) - 1]->id
				);
			if($projectColumnIndex !== -1)
			{
				array_push($allColumns, $projectColumns[$projectColumnIndex]);
				unset($projectColumns[$projectColumnIndex]);
			}
			else
			{
				array_push($allColumns, $standardColumns[$scInd++]);
			}
		}
		
		if(count($projectColumns) > 0)
		{
			$allColumns = array_merge($allColumns, $projectColumns);
		}
		
		return $allColumns;
	}
}
