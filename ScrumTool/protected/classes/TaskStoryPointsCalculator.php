<?php

Yii::import('application.classes.DateDifferenceCalculator');

class TaskStoryPointsCalculator
{
	/**
	 * Calculates the story points, spent working on the tasks of a sprint
	 * @param sprintId int id of the sprint
	 * @param columns Column[..] All the project's columns - arranged
	 * @return array array in the form task_id => story_points
	 */
	public static function calculate($sprintId, $columns)
	{
		$tasksMoves = TaskStoryPointsCalculator::getTasksMoves($sprintId);
		
		$dateDifferenceCalculator = new DateDifferenceCalculator;
		
		$uninterestingColumnIds = TaskStoryPointsCalculator::getUninterestingColumnIds($columns);
		
		$taskStoryPoints = array();
		// Initialize the array by giving a value of 0 for all tasks
		$sprintTasks = TaskStoryPointsCalculator::getSprintTasks($sprintId);
		foreach($sprintTasks as $sprintTask)
		{
			$taskStoryPoints[$sprintTask->task_id] = 0;
		}
		
		foreach($tasksMoves as $taskId => $taskMoves)
		{
			$lastDate = null;
			foreach($taskMoves as $taskMove)
			{
				if($lastDate === null)
				{
					$lastDate = $taskMove->date;
					continue;
				}
				
				if(!in_array($taskMove->from_column_id, $uninterestingColumnIds, true))
				{
					TaskStoryPointsCalculator::addStoryPointsToTask(
						$taskStoryPoints,
						$dateDifferenceCalculator,
						$taskId,
						$lastDate,
						$taskMove->date
						);
				}
				
				$lastDate = $taskMove->date;
			}
			
			// Calculate the amount of time that the task has stayed in the current column
			// (the last column to which it has been moved) if that column contributes to
			// the calculation
			if(!empty($taskMoves))
			{
				$taskMove = $taskMoves[count($taskMoves) - 1];
				if(!in_array($taskMove->to_column_id, $uninterestingColumnIds))
				{
					TaskStoryPointsCalculator::addStoryPointsToTask(
						$taskStoryPoints,
						$dateDifferenceCalculator,
						$taskId,
						$taskMove->date,
						null
						);
				}
			}
			
			$taskStoryPoints[$taskId] = round($taskStoryPoints[$taskId]);
		}
		
		return $taskStoryPoints;
	}
	
	/**
	 * Calculates the story points, spent working on the tasks of a sprint
	 * @param sprintId int id of the sprint
	 * @param columns Column[..] All the project's columns - arranged
	 * @return array array in the form task_id => story_points
	 */
	private static function addStoryPointsToTask(
		&$taskStoryPoints,
		$dateDifferenceCalculator,
		$taskId,
		$lastDate,
		$currentDate
		)
	{
		$dateDifferenceCalculator->clear();
		if($dateDifferenceCalculator->calculate($lastDate, $currentDate))
		{
			if(!isset($taskStoryPoints[$taskId]))
			{
				$taskStoryPoints[$taskId] = 0;
			}
			$taskStoryPoints[$taskId] += $dateDifferenceCalculator->getDays();
			
			Yii::trace('Added ' . $dateDifferenceCalculator->getDays() . ' story points to task ' . $taskId, 'board');
		}
	}
	
	/**
	 * Fills an array of all the moves for all tasks. It is in the form:
	 * task_id => SprintTaskMove[..]
	 * @param sprintId int id of the sprint
	 * @return array array in the form task_id => SprintTaskMove[..]
	 */
	private static function getTasksMoves($sprintId)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'sprint_id = :sprintId';
		$criteria->order = 'date ASC';
		$criteria->params = array(':sprintId' => $sprintId);
		
		$sprintTaskMoves = SprintTaskMove::model()->findAll($criteria);
		
		$tasksMoves = array();
		foreach($sprintTaskMoves as $sprintTaskMove)
		{
			$taskId = $sprintTaskMove->task_id;
			if(!isset($tasksMoves[$taskId]))
			{
				$tasksMoves[$taskId] = array();
			}
			array_push($tasksMoves[$taskId], $sprintTaskMove);
		}
		
		return $tasksMoves;
	}
	
	/**
	 * Gathers all the ids of the columns that do not contribute to the calculation.
	 * Such columns are the ones before 'To do' (including the 'To do' itself) and
	 * the ones after 'Done' (including the 'Done' itself).
	 * They do not contribute to the calculation because while
	 * the task stays in one of these columns, in is not being actively developed.
	 * So moving the task from this column to another one must not be added to the
	 * calculation.
	 * @param columns Column[..] All the project's columns - arranged
	 * @return int[..] array of the ids of the unneeded columns
	 */
	private static function getUninterestingColumnIds($columns)
	{
		$uninterestingColumnIds = array();
		$passedToDo = false;
		$passedDone = false;
		
		foreach($columns as $column)
		{
			if(!$passedToDo)
			{
				array_push($uninterestingColumnIds, $column->id);
				if((int)$column->id === 1)
				{
					$passedToDo = true;
				}
			}
			
			if((int)$column->id === 4)
			{
				$passedDone = true;
			}
			
			if($passedDone)
			{
				array_push($uninterestingColumnIds, $column->id);
			}
		}
		
		return $uninterestingColumnIds;
	}
	
	/**
	 * Gets all the sprint tasks of a sprint
	 * @param sprintId int id of the sprint
	 * @return SprintTask[..] array of the sprint tasks
	 */
	private static function getSprintTasks($sprintId)
	{
		return SprintTask::model()->findAllByAttributes(
			array('sprint_id' => $sprintId)
			);
	}
}
