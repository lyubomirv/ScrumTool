<?php

/**
 * DateHelper
 * Helps with the dates
 */
class DateHelper
{
	private $_offsetFromUTC;
	
	public function __construct()
	{
		$currentDate = new DateTime(null);
		$this->_offsetFromUTC = $currentDate->getOffset();
	}
	
	/**
	 * Returns the current time in UTC
	 * @return int timestamp of the current time in UTC
	 */
	public function getCurrentDateTime()
	{
		return time() - $this->_offsetFromUTC;
	}
	
	/**
	 * Returns the given time in UTC
	 * @return int timestamp of the given time in UTC
	 */
	public function getDateTimeAsUTC($dateTime)
	{
		return $dateTime - $this->_offsetFromUTC;
	}
}
