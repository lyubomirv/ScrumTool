<?php

Yii::import('application.models.sprintPlan.*');

class SprintPlanController extends Controller
{
	/**
	 * Filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
			array('application.filters.HasProjectFilter'),
		);
	}
	
	/**
	 * Access rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('index', 'storyList', 'sprintList', 'taskList'),
				'users' => array('@'),
			),
			array('allow',
				'actions' => array('add', 'remove'),
				'roles' => array('scrum_master'),
			),
			array('deny',
				'users' => array('?'),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * Default action
	 * @param sprintId int id of the sprint to display.
	 * If sprintId is not supplied then one of the sprints for this project is displayed.
	 */
	public function actionIndex($projectId, $sprintId = -1)
	{
		$project = $this->getProject($projectId, true);
		
		if($sprintId === -1)
		{
			$sprint = $this->getLatestSprint($projectId);
			
			if($sprint !== null)
			{
				$this->redirect(
					$this->createUrl(
						'sprintPlan/index',
						array('projectId' => $projectId, 'sprintId' => $sprint->id)
						)
					);
			}
		}
		
		$sprint = Sprint::model()->findByPk(
			$sprintId,
			'project_id = :projectId',
			array(':projectId' => $project->id)
			);
		
		$this->render('index', array(
			'project' => $project,
			'selectedSprint' => $sprint,
			)
		);
	}
	
	/**
	 * Render task list
	 */
	public function actionTaskList($projectId, $sprintId)
	{
		$sprint = $this->getSprintSafe($sprintId, $projectId);
		
		$this->renderPartial(
			'taskList',
			array(
				'projectId' => $projectId,
				'sprint' => $sprint
				)
			);
	}
}
