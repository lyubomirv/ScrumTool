<?php

Yii::import('application.models.project.*');

class ProjectController extends Controller
{
	/**
	 * Filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
			array('application.filters.HasProjectFilter + edit, archive, retrieve, delete'),
		);
	}
	
	/**
	 * Access rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array(
					'index',
					'create',
					'edit',
					'archive',
					'viewarchive',
					'retrieve',
					'delete'),
				'users' => array('@'),
			),
			// These actions are guarded manually. This is because projects of several
			// teams may appear on one page, and rights can be different for each team.
			// array('allow',
			// 	'actions' => array('create', 'edit', 'archive'),
			// 	'roles' => array('team_administrator'),
			// ),
			array('deny',
				'users' => array('?'),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * Default action - Show projects
	 */
	public function actionIndex()
	{
		// Testing
		/*
		$project = new Project;
		$project->id = 123;
		$projectForm = new ProjectForm;
		$projectForm->name = 'Test project';
		$projectForm->useColumn1 = true;
		$projectForm->column1Name = 'First';
		$projectForm->column1Relativity = 'before';
		$projectForm->column1OtherColumn = 'To do';
		$projectForm->useColumn2 = true;
		$projectForm->column2Name = 'Second';
		$projectForm->column2Relativity = 'after';
		$projectForm->column2OtherColumn = 'First';
		if(!$projectForm->validate())
		{
			echo $projectForm->errors;
		}
		$columns = $this->addProjectColumns($project, $projectForm);
		
		foreach($columns as $column)
		{
			echo $column->name . ' ' . $column->previous_column_id . '<br/>';
		}
		*/
		// End testing
		
		$this->render(
			'index',
			array(
				'teams' => $this->getTeams(),
				'ownProjects' => $this->getOwnProjects(true),
				)
			);
	}
	
	/**
	 * Create a project
	 */
	public function actionCreate()
	{
		$projectForm = new ProjectForm;
		
		$this->performAjaxValidation('project-form', $projectForm);
		
		if(isset($_POST['ProjectForm']))
		{
			$success = false;
			
			$projectForm->attributes = $_POST['ProjectForm'];
			$projectForm->purify();
			if($projectForm->validate())
			{
				$transaction = Yii::app()->db->beginTransaction();
				try
				{
					// Create the project
					$project = new Project;
					$project->name = $projectForm->name;
					$project->active = true;
					
					if($projectForm->isTeamSelected())
					{
						if(!$this->isTeamAdministratorById($projectForm->teamId))
						{
							throw new CHttpException(403);
						}
						
						$project->team_id = $projectForm->teamId;
					}
					else
					{
						$project->user_id = Yii::app()->user->getId();
					}
					
					if(!$project->save())
					{
						throw new CException('Error creating project.');
					}
					
					$this->addProjectColumns($project, $projectForm);
					
					$transaction->commit();
					
					$success = true;
				}
				catch(Exception $e)
				{
					$transaction->rollBack();
					
					$success = false;
				}
			}
			
			$this->formResponse(
				$success,
				'project-submit',
				$projectForm,
				$this->createUrl('project/index')
				);
		}
		
		$standardColumns = ProjectColumn::model()->findAllByPk(array(1, 2, 3, 4));
		$standardColumnNames = array();
		foreach($standardColumns as $standardColumn)
		{
			$standardColumnNames = array_merge(
				$standardColumnNames,
				array($standardColumn->name => Yii::t('columns', $standardColumn->name))
				);
		}
		
		$teams = array();
		foreach($this->getTeams() as $team)
		{
			if($this->isTeamAdministrator($team))
			{
				array_push($teams, $team);
			}
		}
		
		$this->displayForm(
			'project-render',
			'create',
			$projectForm,
			array(
				'teams' => $teams,
				'standardColumnNames' => $standardColumnNames,
				)
			);
	}
	
	/**
	 * Edit a project
	 */
	public function actionEdit($projectId)
	{
		$project = $this->getProject($projectId);
		
		$projectForm = new ProjectForm;
		$projectForm->name = $project->name;
		$projectForm->teamId = $project->team_id;
		
		$this->performAjaxValidation('project-form', $projectForm);
		
		if(isset($_POST['ProjectForm']))
		{
			$success = false;
			
			$projectForm->attributes = $_POST['ProjectForm'];
			$projectForm->purify();
			if($projectForm->validate())
			{
				$project->name = $projectForm->name;
				$project->team_id = $projectForm->teamId;
				
				$success = $project->save();
			}
			
			$this->formResponse(
				$success,
				'project-submit',
				$projectForm,
				$this->createUrl('project/index')
				);
		}
		
		$this->displayForm(
			'project-render',
			'edit',
			$projectForm,
			array(
				'teams' => array(),
				'standardColumnNames' => array(),
				)
			);
	}
	
	/**
	 * Archive a project
	 */
	public function actionArchive($projectId)
	{
		$db = Yii::app()->db;
		
		$rowsAffected = $db->createCommand()
			->update(
				'project',
				array('active' => false),
				'id = :projectId',
				array(':projectId' => $projectId)
				);
		
		$this->sendJsonResponse(
			$rowsAffected === 1,
			Yii::t('project', 'Can not archive project')
			);
	}
	
	/**
	 * View the archive
	 */
	public function actionViewarchive()
	{
		$this->renderPartial(
			'viewarchive',
			array(
				'teams' => $this->getTeams(),
				'ownProjects' => $this->getOwnProjects(false),
				)
			);
	}
	
	/**
	 * Retrieve a project from the archive - make it active again
	 */
	public function actionRetrieve($projectId)
	{
		$db = Yii::app()->db;
		
		$rowsAffected = $db->createCommand()
			->update(
				'project',
				array('active' => true),
				'id = :projectId',
				array(':projectId' => $projectId)
				);
		
		//$this->sendJsonResponse(
		//	$rowsAffected === 1,
		//	Yii::t('project', 'Can not retrieve project from the archive')
		//	);
		
		$this->redirect($this->createUrl('project/index'));
	}
	
	/**
	 * Delete a project
	 */
	public function actionDelete($projectId)
	{
		$db = Yii::app()->db;
		
		$rowsAffected = $db->createCommand()->delete(
			'project',
			'id = :projectId',
			array(':projectId' => $projectId)
			);
		
		$this->sendJsonResponse(
			$rowsAffected === 1,
			Yii::t('project', 'Can not delete the project')
			);
	}
	
	/**
	 * Creates the project-specific columns.
	 * Creates and arranges the columns in the correct order. Sets their previous_id.
	 * This function is part of the project creation process. It writes data to the
	 * database and must be called inside a transaction 'try' block.
	 * @param project Project the project, for which the columns are
	 * @param projectForm ProjectForm the form with the user input
	 */
	private function addProjectColumns($project, $projectForm)
	{
		if(!$projectForm->useColumn1 && !$projectForm->useColumn2)
		{
			return;
		}
		
		$projectSpecificColumns = array();
		
		if($projectForm->useColumn1)
		{
			$column = new ProjectColumn;
			$column->project_id = $project->id;
			$column->name = $projectForm->column1Name;
			
			array_push($projectSpecificColumns, $column);
		}
		
		if($projectForm->useColumn2)
		{
			$column = new ProjectColumn;
			$column->project_id = $project->id;
			$column->name = $projectForm->column2Name;
			
			array_push($projectSpecificColumns, $column);
		}
		
		if(count($projectSpecificColumns) > 1)
		{
			if($projectForm->column1OtherColumn === $projectForm->column2Name)
			{
				$projectSpecificColumns = array($projectSpecificColumns[1], $projectSpecificColumns[0]);
			}
		}
		
		$columns = ProjectColumn::model()->findAllByPk(array(1, 2, 3, 4));
		
		foreach($projectSpecificColumns as $projectColumn)
		{
			$relativity = '';
			$otherColumnName = '';
			if($projectColumn->name === $projectForm->column1Name)
			{
				$relativity = $projectForm->column1Relativity;
				$otherColumnName = $projectForm->column1OtherColumn;
			}
			else
			{
				$relativity = $projectForm->column2Relativity;
				$otherColumnName = $projectForm->column2OtherColumn;
			}
			
			foreach($columns as $index => $column)
			{
				if($relativity === 'before' &&
					$otherColumnName === $column->name
					)
				{
					array_splice($columns, $index, 0, array($projectColumn));
					break;
				}
				
				if($relativity === 'after' &&
					$otherColumnName === $column->name
					)
				{
					array_splice($columns, $index + 1, 0, array($projectColumn));
					break;
				}
			}
		}
		
		foreach($columns as $index => $column)
		{
			if($column->name === $projectForm->column1Name ||
				$column->name === $projectForm->column2Name
				)
			{
				if($index === 0)
				{
					$column->previous_column_id = 0;
				}
				else
				{
					$column->previous_column_id = $columns[$index - 1]->id;
				}
				
				if(!$column->save())
				{
					throw new CException('Can not create project-specific column.');
				}
			}
		}
	}
	
	/**
	 * Get the user's own projects
	 * @param active bool get the active or the inactive projects
	 * @return Project[..] array of the projects
	 */
	private function getOwnProjects($active)
	{
		$userId = Yii::app()->user->getId();
		
		$projects = Project::model()->findAllByAttributes(
			array(
				'user_id' => $userId,
				'active' => $active
				)
			);
		
		return $projects;
	}
	
	/**
	 * Find out if the user is the team administrator of the given team
	 * @param team Team the team
	 * @return bool Whether the user is the team administrator of the team
	 */
	protected function isTeamAdministrator($team)
	{
		return $this->isTeamAdministratorById($team->id);
	}
	
	/**
	 * Find out if the user is the team administrator of the team with the given team id
	 * @param teamId int the team's id
	 * @return bool Whether the user is the team administrator of the team
	 */
	private function isTeamAdministratorById($teamId)
	{
		$userId = Yii::app()->user->getId();
		$roleId = Yii::app()->params['roleIdentifiers']['team_administrator'];
		
		$criteria = new CDbCriteria;
		$criteria->alias = 'tmr';
		$criteria->join = 'JOIN team_member AS tm ON tmr.team_member_id = tm.id';
		$criteria->condition =
			'tm.team_id = :teamId AND
			tm.user_id = :userId AND
			tmr.role_id = :roleId';
		$criteria->params = array(
			':teamId' => $teamId,
			':userId' => $userId,
			':roleId' => $roleId,
			);
			
		return TeamMemberRole::model()->exists($criteria);
	}
	
	/**
	 * Check if a team has active projects
	 * @param teamId int id of the team
	 * @return bool Whether there is at least one active project owned by the team
	 */
	protected function hasActiveProjects($teamId)
	{
		return Project::model()->exists(
			'team_id = :teamId AND active = :active',
			array(':teamId' => $teamId, ':active' => true)
			);
	}
	
	/**
	 * Check if a team has inactive projects
	 * @param teamId int id of the team
	 * @return bool Whether there is at least one inactive project owned by the team
	 */
	protected function hasInactiveProjects($teamId)
	{
		return Project::model()->exists(
			'team_id = :teamId AND active = :inactive',
			array(':teamId' => $teamId, ':inactive' => false)
			);
	}
	
	/**
	 * Check if the user has active projects
	 * @return bool Whether there is at least one active project owned by the user
	 */
	protected function hasOwnActiveProjects()
	{
		$userId = Yii::app()->user->getId();
		
		return Project::model()->exists(
			'user_id = :userId AND active <> :inactive',
			array(':userId' => $userId, ':inactive' => false)
			);
	}
}
