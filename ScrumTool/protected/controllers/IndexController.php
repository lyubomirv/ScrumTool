<?php

Yii::import('application.models.index.*');

class IndexController extends Controller
{
	/**
	 * Filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	/**
	 * Access rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('selectLanguage'),
				'users' => array('*'),
			),
			array('allow',
				'actions' => array('login'),
				'users' => array('?'),
			),
			array('deny',
				'actions' => array('login'),
				'users' => array('@'),
			),
			/*
			// TEST
			array('deny',
				'actions' => array('logout'),
				'roles' => array('scrum_master'),
			),
			*/
			array('allow',
				'actions' => array('logout'),
				'users' => array('@'),
			),
			array('deny',
				'actions' => array('logout'),
				'users' => array('?'),
			),
			array('deny',
				'actions' => array('logout'),
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * Default action - Home page
	 */
	public function actionIndex()
	{
		if(Yii::app()->user->isGuest)
		{
			$this->render('index');
		}
		else
		{
			$user = User::model()->findByPk(Yii::app()->user->getId());
			$name = $user->username;
			if($user->name !== null && !empty($user->name))
			{
				$name = $user->name;
			}
			
			$teams = $this->getTeams();
			
			$hasTeams = !empty($teams);
			$hasProjects = false;
			
			$ownProjects = Project::model()->findAllByAttributes(
				array('user_id' => $user->id)
				);
			if(empty($ownProjects))
			{
				foreach($teams as $team)
				{
					if(!empty($team->projects))
					{
						$hasProjects = true;
						break;
					}
				}
			}
			else
			{
				$hasProjects = true;
			}
			
			$this->render(
				'memberindex',
				array(
					'name' => $name,
					'hasTeams' => $hasTeams,
					'hasProjects' => $hasProjects,
					)
				);
		}
	}
	
	/**
	 * Select language
	 * @param language string the selected language
	 */
	public function actionSelectLanguage($language)
	{
		Yii::app()->language = $language;
		Yii::app()->request->cookies['language'] = new CHttpCookie('language', $language);
		$this->redirect($this->getPreviousPage());
	}
	
	/**
	 * Logs in an user
	 * Show the form where the user enters their username and password
	 * Validates and logs the user in
	 */
	public function actionLogin()
	{
		$loginForm = new LoginForm;
		
		$this->performAjaxValidation('login-form', $loginForm);
		
		// Collect user input data
		if(isset($_POST['LoginForm']))
		{
			$loginForm->attributes = $_POST['LoginForm'];
			$loginForm->purify();
			if($loginForm->validate() && $loginForm->login())
			{
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		
		// Display login form
		$this->render('login', array(
			'model' => $loginForm)
			);
	}
	
	/**
	 * Registers a new user
	 * Shows a form where the user enters their data
	 * Validates and creates a new user
	 */
	public function actionRegister()
	{
		$registerForm = new RegisterForm;
		
		$this->performAjaxValidation('register-form', $registerForm);
		
		// Collect user input data
		if(isset($_POST['RegisterForm']))
		{
			$registerForm->attributes = $_POST['RegisterForm'];
			$registerForm->purify();
			if($registerForm->validate())
			{
				$user = new User;
				$user->username = $registerForm->username;
				
				$salt = $this->createSalt();
				$user->password = $user->getPasswordHash($registerForm->password, $salt);
				$user->salt = $salt;
				
				$user->email = $registerForm->email;
				if($registerForm->name !== null && strlen($registerForm->name) > 0)
				{
					$user->name = $registerForm->name;
				}
				
				if($user->validate())
				{
					$user->save();
					$this->redirect(Yii::app()->user->returnUrl);
				}
			}
		}
		
		// Display register form
		$this->render('register', array(
			'model' => $registerForm)
			);
	}
	
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	/**
	 * Renders the error page
	 */
	public function actionError()
	{
		if($error = Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
			{
	    		echo $error['message'];
			}
	    	else
			{
	        	$this->render('error', $error);
			}
	    }
	}
	
	private function createSalt()
	{
		$saltLength = Yii::app()->params['saltLength'];
		
		$salt = substr(hash('sha256', uniqid(rand(), true)), 0, $saltLength);
		
		return $salt;
	}
}
