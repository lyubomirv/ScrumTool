<?php

class DashboardController extends Controller
{
	/**
	 * Filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
			array('application.filters.HasSprintFilter'),
		);
	}
	
	/**
	 * Access rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('index'),
				'users' => array('@'),
			),
			array('deny',
				'users' => array('?'),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * Default action
	 */
	public function actionIndex($projectId, $sprintId)
	{
		$dateRemainingPoints = $this->getSprintBurnDownData($projectId, $sprintId);
		
		$sprintCompletedPoints = $this->getProjectVelocityData($projectId, $sprintId);
		
		// Testing
		//echo date('d-m-Y H:i:s', strtotime('01-01-2012 15:48:13'));
		//echo date('d-m-Y H:i:s', strtotime('12-01-2012 15:48:13'));
		//echo date('d-m-Y H:i:s', strtotime('12-01-2012 15:48:13') - strtotime('12-01-2012'));
		//echo $this->getSprintAllocatedPoints(1);
		// End testing
		
		$this->render(
			'index',
			array(
				'dateRemainingPoints' => $dateRemainingPoints,
				'sprintCompletedPoints' => $sprintCompletedPoints
				)
			);
	}
	
	/**
	 * Get the data for creating a burn-down chart.
	 * The result is an array of type timestamp => remaining points.
	 * @param projectId int id of the project, in which the sprint is
	 * @param sprintId int the id of the sprint
	 * @return array(timestamp => remaining points) data for the burn-down chart
	 */
	private function getSprintBurnDownData($projectId, $sprintId)
	{
		$sprint = Sprint::model()->findByPk(
			$sprintId,
			'project_id = :projectId',
			array(':projectId' => $projectId)
			);
		if($sprint === null)
		{
			throw new CHttpException(403);
		}
		
		$completedSprintTaskMoves = $this->getCompletedSprintTaskMoves($sprintId);
		$sprintAllocatedPoints = $this->getSprintAllocatedPoints($sprintId);
		
		$startDate = date('Y-m-d', strtotime($sprint->date_start));
		$endDate = date('Y-m-d', strtotime($sprint->date_end));
		
		$secondsInDay = 60 * 60 * 24;
		$hoursInDay = 24;
		$startTime = strtotime($startDate);
		$endTime = strtotime($endDate);
		$todayDateTime = $this->getCurrentDateTime();
		
		// The first element in the array shows the start of the sprint,
		// which is the amount of allocated points
		$dateRemainingPoints = array();
		$dateRemainingPoints[$startTime] = $sprintAllocatedPoints;
		
		$totalFinishedPoints = 0;
		
		// Iterate the dates between the start and end,
		// starting from the day after the start
		for($currentTime = $startTime + $secondsInDay;
			$currentTime <= $endTime;
			$currentTime += $secondsInDay)
		{
			$currentDate = $currentTime;
			
			$dateRemainingPoints[$currentDate] = null;
			
			foreach($completedSprintTaskMoves as $completedSprintTaskMove)
			{
				$taskFinishedTime = strtotime($completedSprintTaskMove->date);
				
				$difference = $currentTime - $taskFinishedTime;
				
				// Means the day is already passed,
				// as the tasks are ordered by date of completion
				if($difference < 0)
				{
					break;
				}
				
				if($difference < $secondsInDay)
				{
					if($dateRemainingPoints[$currentDate] === null)
					{
						$dateRemainingPoints[$currentDate] =
							$sprintAllocatedPoints - $totalFinishedPoints;
					}
					
					$taskEstimation = $completedSprintTaskMove->task->estimation;
					
					$dateRemainingPoints[$currentDate] -= $taskEstimation;
					$totalFinishedPoints += $taskEstimation;
				}
			}
			
			if($currentDate <= $todayDateTime && $dateRemainingPoints[$currentDate] === null)
			{
				$previousValue = $dateRemainingPoints[$currentDate - $secondsInDay];
				$dateRemainingPoints[$currentDate] = $previousValue;
			}
		}
		
		return $dateRemainingPoints;
	}
	
	/**
	 * Get the data for creating a velocity chart.
	 * The result is an array of type sprint name => completed points.
	 * @param projectId int id of the project, in which the sprint is
	 * @param sprintId int the id of the sprint that should be the limiting sprint for the chart
	 * @return array(sprint name => completed points) data for the velocity chart
	 */
	private function getProjectVelocityData($projectId, $sprintId)
	{
		$result = Yii::app()->db->createCommand()
			->select('s.id')
			->from('sprint s')
			->join('project p', 's.project_id = p.id')
			->where('
				p.id = :projectId AND
				s.date_end < (
					SELECT date_end
					FROM sprint
					WHERE id = :sprintId)',
				array(':projectId' => $projectId, ':sprintId' => $sprintId)
				)
			->query();
		
		$sprintCompletedPoints = array();
		
		foreach($result as $row)
		{
			if(!isset($row['id']))
			{
				continue;
			}
			
			$currentSprintId = $row['id'];
			
			$sprint = Sprint::model()->findByPk($currentSprintId);
			if($sprint === null)
			{
				continue;
			}
			
			$completedPoints = $this->getSprintCompletedPoints($currentSprintId);
			
			$sprintCompletedPoints += array($sprint->name => $completedPoints);
		}
		
		return $sprintCompletedPoints;
	}
	
	/**
	 * Get all the completed tasks for a sprint.
	 * If a task has been moved to 'Done' but then moved back, it will not be in the array.
	 * @param sprintId int the id of the sprint
	 * @return SprintTaskMove[..] all the completed tasks
	 */
	private function getCompletedSprintTaskMoves($sprintId)
	{
		$criteria = new CDbCriteria;
		$criteria->alias = 'taskmove';
		$criteria->condition =
			'taskmove.sprint_id = :sprintId AND
			taskmove.to_column_id = :doneColumnId AND
			taskmove.task_id NOT IN
				(SELECT stm.task_id
				FROM sprint_task_move as stm
				WHERE
					stm.sprint_id = :sprintId AND
					stm.date > taskmove.date
				)
			';
		$criteria->order = 'taskmove.date ASC';
		$criteria->params = array(':sprintId' => $sprintId, ':doneColumnId' => 4);
		
		return SprintTaskMove::model()->findAll($criteria);
	}
	
	/**
	 * Get the amount of the completed points for the sprint
	 * Sums all the copmleted sprint tasks' estimations.
	 * @param sprintId int the id of the sprint
	 * @return int completed points for the sprint
	 */
	private function getSprintCompletedPoints($sprintId)
	{
		$result = Yii::app()->db->createCommand()
			->select('SUM(t.estimation) AS points')
			->from('task t')
			->join('sprint_task_move stm', 't.id = stm.task_id')
			->where('
				stm.sprint_id = :sprintId AND 
				stm.to_column_id = :doneColumn AND 
				t.id NOT IN
					(SELECT stm2.task_id
					FROM sprint_task_move as stm2
					WHERE
						stm2.sprint_id = :sprintId AND
						stm2.date > stm.date
					)',
				array(':sprintId' => $sprintId, ':doneColumn' => 4))
			->queryRow();
		
		if(isset($result['points']))
		{
			return $result['points'];
		}
		
		return 0;
	}
	
	/**
	 * Get the amount of the allocated points for the sprint
	 * Sums all the sprint tasks' estimations.
	 * @param sprintId int the id of the sprint
	 * @return int allocated points for the sprint
	 */
	private function getSprintAllocatedPoints($sprintId)
	{
		$result = Yii::app()->db->createCommand()
			->select('SUM(t.estimation) AS points')
			->from('task t')
			->join('sprint_task st', 't.id = st.task_id')
			->where('st.sprint_id = :sprintId', array(':sprintId' => $sprintId))
			->queryRow();
			
		if(isset($result['points']))
		{
			return $result['points'];
		}
		
		return 0;
	}
	
	private function getCurrentDateTime()
	{
		$currentDate = new DateTime(null);
		$offsetFromUTC = $currentDate->getOffset();
		
		$currentTime = time() - $offsetFromUTC;
		$currentDate = date('Y-m-d', $currentTime);
		
		return strtotime($currentDate);
	}
}
