<?php

Yii::import('application.models.team.*');

class TeamController extends Controller
{
	/**
	 * Filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
			array('application.filters.HasTeamFilter - index, createTeam'),
		);
	}
	
	/**
	 * Access rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('index', 'teamTable', 'createTeam', 'leave'),
				'users' => array('@'),
			),
			array('allow',
				'actions' => array('addMember', 'removeMember'),
				'roles' => array('team_administrator'),
			),
			array('deny',
				'users' => array('?'),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * Default action - Show team page
	 */
	public function actionIndex($teamId = -1)
	{
		$teams = $this->getTeams();
		$team = $this->getTeam($teamId);
		
		if($team === null && count($teams) > 0)
		{
			$this->redirect(
				$this->createUrl('team/index', array('teamId' => $teams[0]->id))
				);
		}
		
		$this->render(
			'index',
			array(
				'teams' => $teams,
				'team' => $team,
				)
		);
	}
	
	/**
	 * Create a team
	 */
	public function actionCreateTeam()
	{
		$createTeamForm = new CreateTeamForm;
		
		$this->performAjaxValidation('createTeam-form', $createTeamForm);
		
		if(isset($_POST['CreateTeamForm']))
		{
			$success = false;
			
			$team = null;
			
			$createTeamForm->attributes = $_POST['CreateTeamForm'];
			$createTeamForm->purify();
			if($createTeamForm->validate())
			{
				$userId = Yii::app()->user->getId();
				
				$transaction = Yii::app()->db->beginTransaction();
				try
				{
					// Create a team
					$team = new Team;
					$team->name = $createTeamForm->teamName;
					$team->save();
					
					// Make the user member of this team
					$teamMember = new TeamMember;
					$teamMember->team_id = $team->id;
					$teamMember->user_id = $userId;
					$teamMember->save();
					
					$roleNames = array(
						'user',
						'scrum_master',
						'product_owner',
						'team_administrator'
						);
						
					// Assign roles in the database
					foreach($roleNames as $roleName)
					{
						$teamMemberRole = new TeamMemberRole;
						$teamMemberRole->team_member_id = $teamMember->id;
						$teamMemberRole->role_id = Yii::app()->params['roleIdentifiers'][$roleName];
						$teamMemberRole->save();
					}
					
					$transaction->commit();
					
					$success = true;
				}
				catch(Exception $e)
				{
					$transaction->rollBack();
					
					$success = false;
				}
			}
			
			$parameters = array();
			if($success && $team !== null)
			{
				$parameters = array('teamId' => $team->id);
			}
			
			$this->formResponse(
				$success,
				'createTeam-submit',
				$createTeamForm,
				$this->createUrl('team/index'),
				$parameters
				);
		}
		
		$this->displayForm('createTeam-render', 'createTeam', $createTeamForm);
	}
	
	/**
	 * Add a member
	 */
	public function actionAddMember($teamId)
	{
		$addMemberForm = new AddMemberForm($teamId);
		
		$this->performAjaxValidation('addMember-form', $addMemberForm);
		
		// Collect user input data
		if(isset($_POST['AddMemberForm']))
		{
			$success = false;
			
			$addMemberForm->attributes = $_POST['AddMemberForm'];
			$addMemberForm->purify();
			if($addMemberForm->validate())
			{
				$user = User::model()->findByAttributes(
					array('username' => $addMemberForm->username)
					);
				if($user === null)
				{
					throw new CHttpException(403);
				}
				
				$transaction = Yii::app()->db->beginTransaction();
				try
				{
					// Add the user to the team
					$teamMember = new TeamMember;
					$teamMember->team_id = $teamId;
					$teamMember->user_id = $user->id;
					$teamMember->save();
					
					// Add roles to the user
					$teamMemberRole = new TeamMemberRole;
					$teamMemberRole->team_member_id = $teamMember->id;
					$teamMemberRole->role_id = Yii::app()->params['roleIdentifiers']['user'];
					$teamMemberRole->save();
					
					$transaction->commit();
					
					$success = true;
				}
				catch(Exception $e)
				{
					$transaction->rollBack();
					
					$success = false;
				}
			}
			
			$this->formResponse(
				$success,
				'addMember-submit',
				$addMemberForm,
				$this->createUrl('team/index', array('teamId' => $teamId))
				);
		}
		
		$this->displayForm('addMember-render', 'addMember', $addMemberForm);
	}
	
	/**
	 * Remove a member from the team
	 */
	public function actionRemoveMember($teamId, $userId)
	{
		$team = $this->getTeam($teamId);
		
		if($userId === Yii::app()->user->getId())
		{
			$this->sendJsonResponse(
				false,
				Yii::t('team', 'You can not remove youself from the team')
				);
			return;
		}
		
		// Team member roles are automatically removed because of a cascade delete rule so
		// there is no need to remove them explicitly
		$rowsDeleted = Yii::app()->db->createCommand()
			->delete(
				'team_member',
				array('and', 'team_id = :teamId', 'user_id = :userId'),
				array(':teamId' => $teamId, ':userId' => $userId)
				);
		$success = $rowsDeleted > 0;

		// Send response to the client
		$this->sendJsonResponse(
			$success,
			Yii::t('team', 'There was an error removing the member.')
			);
	}
	
	/**
	 * Leave the team
	 * If you are a team administrator, you can leave the team only if
	 * there is at least one more team administrator in the team.
	 * If you are the only member of the team, it is deleted (archived).
	 */
	public function actionLeave($teamId)
	{
		$userId = Yii::app()->user->getId();
		$dbConnection = Yii::app()->db;
		
		$removeTeam = false;
		
		// Check if there is anyone else in this team.
		$anotherMemberExists = TeamMember::model()->exists(
			'team_id = :teamId AND user_id <> :userId',
			array(':teamId' => $teamId, ':userId' => $userId)
			);
		if(!$anotherMemberExists)
		{
			$removeTeam = true;
		}
		
		// If the team is not to be removed and the current user is a team administrator,
		// it has to be ensured that there is another team administrator.
		if(!$removeTeam)
		{
			$authManager = Yii::app()->authManager;
			if($authManager->isAssigned('team_administrator', $userId))
			{
				// Check if there is another team administrator in the team.
				// If there isn't you must not be allowed to leave.
				$criteria = new CDbCriteria;
				$criteria->alias = 'tm';
				$criteria->join = 'JOIN team_member_role AS tmr ON tmr.team_member_id = tm.id';
				$criteria->condition =
					'tm.team_id = :teamId AND tm.user_id <> :userId AND tmr.role_id = :roleId';
				$criteria->params = array(
					':teamId' => $teamId,
					':userId' => $userId,
					':roleId' => Yii::app()->params['roleIdentifiers']['team_administrator'],
					);
				
				$anotherTeamAdministratorExists = TeamMember::model()->exists($criteria);
				if(!$anotherTeamAdministratorExists)
				{
					$this->sendJsonResponse(
						false,
						Yii::t('team', 'You are the only Team Aministrator in this team.')
						);
					return;
				}
			}
		}
		
		$success = false;
		
		$transaction = $dbConnection->beginTransaction();
		try
		{
			// Delete the user's membership in the team
			$rowsDeleted = Yii::app()->db->createCommand()
				->delete(
					'team_member',
					array('and', 'team_id = :teamId', 'user_id = :userId'),
					array(':teamId' => $teamId, ':userId' => $userId)
					);
			if($rowsDeleted < 1)
			{
				throw new CException('Can not remove the user from the team.');
			}
			
			// Delete the team if needed
			$rowsDeleted = Yii::app()->db->createCommand()
				->delete(
					'team',
					'id = :teamId',
					array(':teamId' => $teamId)
					);
			if($rowsDeleted !== 1)
			{
				throw new CException('Can not remove the team.');
			}
			
			$transaction->commit();
			
			$success = true;
		}
		catch(Exception $e)
		{
			$transaction->rollBack();
			
			$success = false;
		}
		
		// Send response to client
		$this->sendJsonResponse($success, Yii::t('team', 'There was an error leaving the team.'));
	}
	
	/**
	 * Render the team table
	 */
	public function actionTeamTable($teamId)
	{
		$team = $this->getTeam($teamId, true);
		
		$this->renderPartial('teamTable', array('team' => $team));
	}
}
