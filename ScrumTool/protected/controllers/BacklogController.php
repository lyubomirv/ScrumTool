<?php

Yii::import('application.models.backlog.*');

class BacklogController extends Controller
{
	/**
	 * Filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
			array('application.filters.HasProjectFilter'),
		);
	}
	
	/**
	 * Access rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('index', 'storyList', 'taskList', 'viewStory'),
				'users' => array('@'),
			),
			array('allow',
				'actions' => array('addStory', 'editStory', 'deleteStory'),
				'roles' => array('product_owner'),
			),
			array('deny',
				'users' => array('?'),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * Default action - Show user stories
	 */
	public function actionIndex($projectId)
	{
		$project = $this->getProject($projectId, true);
		
		$this->render('index', array(
			'project' => $project,
			)
		);
	}
	
	/**
	 * View a user story
	 */
	public function actionViewStory($projectId, $storyId)
	{
		$project = $this->getProject($projectId, true);
		
		$story = Story::model()->findByPk(
			$storyId,
			'project_id = :projectId',
			array(':projectId' => $project->id)
			);
		$this->verifyStory($story);
		
		$this->renderPartial(
			'viewStory',
			array(
				'project' => $project,
				'story' => $story,
				)
			);
	}
	
	/**
	 * Add a user story
	 */
	public function actionAddStory($projectId)
	{
		$storyForm = new StoryForm($projectId);
		
		$this->performAjaxValidation('story-form', $storyForm);
		
		if(isset($_POST['StoryForm']))
		{
			$success = false;
			
			$storyForm->attributes = $_POST['StoryForm'];
			$storyForm->purify();
			if($storyForm->validate())
			{
				// Create the story
				$story = new Story;
				$story->name = $storyForm->name;
				$story->project_id = $projectId;
				if($storyForm->description !== null)
				{
					$story->description = $storyForm->description;
				}
				if($storyForm->priority !== null)
				{
					$story->priority = $storyForm->priority;
				}
				$success = $story->save();
			}
			
			$this->formResponse(
				$success,
				'story-submit',
				$storyForm,
				$this->createUrl('backlog/index')
				);
		}
		
		$this->displayForm('story-render', 'addStory', $storyForm);
	}
	
	/**
	 * Edit story
	 */
	public function actionEditStory($projectId, $storyId)
	{
		$story = Story::model()->findByPk(
			$storyId,
			'project_id = :projectId',
			array(':projectId' => $projectId)
			);
		$this->verifyStory($story);
		
		$storyForm = new StoryForm($projectId, $storyId);
		$storyForm->name = $this->purify($story->name);
		$storyForm->description = $this->purify($story->description);
		$storyForm->priority = $this->purify($story->priority);
		
		$this->performAjaxValidation('story-form', $storyForm);
		
		if(isset($_POST['StoryForm']))
		{
			$success = false;
			
			$storyForm->attributes = $_POST['StoryForm'];
			$storyForm->purify();
			if($storyForm->validate())
			{
				$story->name = $storyForm->name;
				$story->description = $storyForm->description;
				$story->priority = $storyForm->priority;
				
				$success = $story->save();
			}
			
			$this->formResponse(
				$success,
				'story-submit',
				$storyForm,
				$this->createUrl('backlog/index')
				);
		}
		
		$this->displayForm('story-render', 'editStory', $storyForm);
	}
	
	/**
	 * Delete story
	 */
	public function actionDeleteStory($projectId, $storyId)
	{
		$story = Story::model()->findByPk(
			$storyId,
			'project_id = :projectId',
			array(':projectId' => $projectId)
			);
		$this->verifyStory($story);
		
		$success = $story->delete();
		
		$this->sendJsonResponse($success, 'There was an error archiving the story.');
	}
	
	/**
	 * Render the stories list
	 */
	public function actionStoryList($projectId)
	{
		$project = $this->getProject($projectId, true);
		
		$this->renderPartial('storyList', array('project' => $project));
	}
	
	/**
	 * Render the tasks of a story
	 */
	public function actionTaskList($projectId, $storyId)
	{
		$story = Story::model()->findByPk(
			$storyId,
			'project_id = :projectId',
			array(':projectId' => $projectId)
			);
		$this->verifyStory($story);
		
		$this->renderPartial('taskList', array('projectId' => $projectId, 'story' => $story));
	}
	
	public function getShortenedString($string, $limit)
	{
		if(strlen($string) < $limit)
		{
			return $string;
		}
		
		$left = substr($string, 0, $limit);
		$right = substr($string, $limit);
		
		$i = 0;
		while($left[strlen($left) - 1] !== ' ')
		{
			$left = $left . $right[$i++];
		}
		
		return $left . ' ...';
	}
}
