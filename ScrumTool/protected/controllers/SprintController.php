<?php

Yii::import('application.models.sprint.*');

class SprintController extends Controller
{
	/**
	 * Filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
			array('application.filters.HasProjectFilter'),
		);
	}
	
	/**
	 * Access rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('create', 'edit', 'add', 'remove'),
				'roles' => array('scrum_master'),
			),
			array('deny',
				'users' => array('?'),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * Create a sprint
	 */
	public function actionCreate($projectId)
	{
		$sprintForm = new SprintForm($projectId);
		
		$this->performAjaxValidation('sprint-form', $sprintForm);
		
		if(isset($_POST['SprintForm']))
		{
			$success = false;
			
			$responseUrl = $this->createUrl('sprintPlan/index', array('projectId' => $projectId));
			
			$sprintForm->attributes = $_POST['SprintForm'];
			$sprintForm->purify();
			if($sprintForm->validate())
			{
				// Create the sprint
				$sprint = new Sprint;
				$sprint->name = $sprintForm->name;
				$sprint->story_points = $sprintForm->story_points;
				if($sprintForm->date_start !== '')
				{
					$sprint->date_start = $sprintForm->date_start;
				}
				if($sprintForm->date_end !== '')
				{
					$sprint->date_end = $sprintForm->date_end;
				}
				$sprint->project_id = $projectId;
				$success = $sprint->save();
				
				if($success)
				{
					$responseUrl = $this->createUrl(
						'sprintPlan/index',
						array(
							'projectId' => $projectId,
							'sprintId' => $sprint->id
							)
						);
				}
			}
			
			$this->formResponse($success, 'sprint-submit', $sprintForm, $responseUrl);
		}
		
		$this->displayForm('sprint-render', 'create', $sprintForm);
	}
	
	/**
	 * Add task(s) to the sprint
	 */
	public function actionAdd($projectId, $sprintId)
	{
		$project = $this->getProject($projectId, true);
		// TODO: Is this necessary? This is just for ensuring that the sprint is of this project
		$sprint = $this->getSprintSafe($sprintId, $projectId);
		
		$tasks = $this->getTasksToAdd($projectId);
		if(count($tasks) === 0)
		{
			$this->sendJsonResponse(false, 'No tasks specified.');
			return;
		}
		
		$firstColumn = $this->getFirstProjectColumn($project);
		if($firstColumn === null)
		{
			$this->sendJsonResponse(false, 'Project columns error.');
			return;
		}
		
		$success = false;
		
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			foreach($tasks as $task)
			{
				$sprintTask = new SprintTask;
				$sprintTask->sprint_id = $sprintId;
				$sprintTask->task_id = $task->id;
				if(!$sprintTask->save())
				{
					throw new CException(
						'Can not add task ' . $task->id . ' to sprint ' . $sprintId
						);
				}
				
				$sprintColumnTask = new SprintColumnTask;
				$sprintColumnTask->sprint_id = $sprintId;
				$sprintColumnTask->column_id = $firstColumn->id;
				$sprintColumnTask->task_id = $task->id;
				$sprintColumnTask->previous_id =
					$this->findSprintFirstColumnLastTaskId($firstColumn->id, $sprintId);
				if(!$sprintColumnTask->save())
				{
					throw new CException(
						'Can not add task ' . $task->id . ' to sprint ' . $sprintId . '\'s first column'
						);
				}
			}
			
			$transaction->commit();
			
			$success = true;
		}
		catch(Exception $e)
		{
			$transaction->rollBack();
			
			$success = false;
		}
		
		$this->sendJsonResponse($success, 'There was an error adding the task(s) to the sprint.');
	}
	
	/**
	 * Remove task(s) from the sprint
	 */
	public function actionRemove($projectId, $sprintId)
	{
		$project = $this->getProject($projectId, true);
		// TODO: Is this necessary? This is just for ensuring that the sprint is of this project
		$sprint = $this->getSprintSafe($sprintId, $projectId);
		
		$tasks = $this->getTasksToRemove($projectId, $sprintId);
		if(count($tasks) === 0)
		{
			$this->sendJsonResponse(false, 'No tasks specified.');
			return;
		}
		
		$firstColumn = $this->getFirstProjectColumn($project);
		if($firstColumn === null)
		{
			$this->sendJsonResponse(false, 'Project columns error.');
			return;
		}
		
		$success = false;
		
		$db = Yii::app()->db;
		$transaction = $db->beginTransaction();
		try
		{
			$sprintTaskIds = array();
			$sprintColumnTaskIds = array();
			$sprintTaskMoveIds = array();
			foreach($tasks as $task)
			{
				$db->createCommand()->delete(
					'sprint_task',
					array('and', 'sprint_id = :sprintId', 'task_id = :taskId'),
					array(':sprintId' => $sprintId, ':taskId' => $task->id)
					);
				
				$db->createCommand()->delete(
					'sprint_column_task',
					array('and', 'sprint_id = :sprintId', 'task_id = :taskId'),
					array(':sprintId' => $sprintId, ':taskId' => $task->id)
					);
					
				$db->createCommand()->delete(
					'sprint_task_move',
					array('and', 'sprint_id = :sprintId', 'task_id = :taskId'),
					array(':sprintId' => $sprintId, ':taskId' => $task->id)
					);
			}
			
			$transaction->commit();
			
			$success = true;
		}
		catch(Exception $e)
		{
			$transaction->rollBack();
			
			$success = false;
		}
		
		$this->sendJsonResponse(
			$success,
			'There was an error removing the task(s) from the sprint.'
			);
	}
	
	/**
	 * Get array of task ids from the POST parameter 'Tasks'
	 * @return int[] array of task ids
	 */
	private function getTasksFromPost()
	{
		if(!isset($_POST['Tasks']))
		{
			return array();
		}
		
		return $_POST['Tasks'];
	}
	
	/**
	 * Get array of Tasks that can be added in a sprint.
	 * The task ids are taken from a POST parameter and tasks are filtered
	 * so that only tasks that are from the given project and that are not
	 * already added in another sprint pass.
	 * @param projectId int id of a project in which the tasks must be
	 * @return Task[] array of tasks that can be safely added to a sprint
	 */
	private function getTasksToAdd($projectId)
	{
		$taskIds = $this->getTasksFromPost();
		
		$criteria = new CDbCriteria;
		$criteria->alias = 't';
		$criteria->join = 'JOIN story ON story.id = t.story_id';
		$criteria->condition = 't.estimation IS NOT NULL AND story.project_id = :projectId';
		$criteria->params = array(':projectId' => $projectId);
		
		$tasks = Task::model()->findAllByPk(
			$taskIds,
			$criteria
			);
			
		$tasksToAdd = array();
		
		foreach($tasks as $task)
		{
			// TODO: Task in multiple sprints?
			// Assert that the task is not already added to another sprint
			if($task->sprintTasks !== null && count($task->sprintTasks) > 0)
			{
				continue;
			}
			
			array_push($tasksToAdd, $task);
		}
		
		return $tasksToAdd;
	}
	
	/**
	 * Get array of Tasks that can be removed from the sprint.
	 * The task ids are taken from a POST parameter and tasks are filtered
	 * so that only tasks that are from the given project and that are
	 * already added in the given sprint pass.
	 * @param projectId int id of a project in which the tasks must be
	 * @param sprintId int id of the sprint in which the tasks must be
	 * @return Task[] array of tasks that can be safely removed from the sprint
	 */
	private function getTasksToRemove($projectId, $sprintId)
	{
		$taskIds = $this->getTasksFromPost();
		
		$criteria = new CDbCriteria;
		$criteria->alias = 't';
		$criteria->join =
			'JOIN story AS s ON s.id = t.story_id
			JOIN sprint_task AS st ON st.task_id = t.id
			';
		$criteria->condition =
			't.estimation IS NOT NULL AND s.project_id = :projectId AND st.sprint_id = :sprintId';
		$criteria->params = array(':projectId' => $projectId, ':sprintId' => $sprintId);
		
		return Task::model()->findAllByPk($taskIds, $criteria);
	}
	
	/*
	 * Get the first column for a project
	 * @param project Project the project
	 */
	private function getFirstProjectColumn($project)
	{
		foreach($project->projectColumns as $column)
		{
			if($column->previous_column_id == 0)
			{
				return $column;
			}
		}
		
		return ProjectColumn::model()->findByPk(1);
	}
	
	/*
	 * Get the id of the last task in the first column for a project
	 * @param firstColumnId int id of the first column of the project
	 * @param sprintId int id of the sprint
	 * @return int id of the last task in the first column
	 */
	private function findSprintFirstColumnLastTaskId($firstColumnId, $sprintId)
	{
		$sprintColumnTasks = SprintColumnTask::model()->findAllByAttributes(
			array(
				'sprint_id' => $sprintId,
				'column_id' => $firstColumnId,
				)
			);
		
		if(count($sprintColumnTasks) === 0)
		{
			return -1;
		}
		
		// Sort the tasks according to the previous_id field
		for($i = 0; $i < count($sprintColumnTasks); ++$i)
		{
			for($j = $i + 1; $j < count($sprintColumnTasks); ++$j)
			{
				if($sprintColumnTasks[$i]->previous_id === $sprintColumnTasks[$j]->task_id ||
					$sprintColumnTasks[$j]->previous_id === -1
					)
				{
					list($sprintColumnTasks[$i], $sprintColumnTasks[$j]) =
						array($sprintColumnTasks[$j], $sprintColumnTasks[$i]);
				}
			}
		}
		
		return $sprintColumnTasks[count($sprintColumnTasks) - 1]->task_id;
	}
}
