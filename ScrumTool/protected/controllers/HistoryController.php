<?php

class HistoryController extends Controller
{
	/**
	 * Filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
			array('application.filters.HasProjectFilter'),
		);
	}
	
	/**
	 * Access rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('index'),
				'users' => array('@'),
			),
			array('deny',
				'users' => array('?'),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * Default action
	 */
	public function actionIndex($projectId)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'project_id = :projectId';
		$criteria->order = 'date_end DESC';
		$criteria->limit = 1;
		$criteria->params = array(':projectId' => $projectId);
		
		$sprint = Sprint::model()->find($criteria);
		
		$criteria = new CDbCriteria;
		$criteria->condition = 'id <> :sprintId AND project_id = :projectId';
		$criteria->order = 'date_end DESC';
		$criteria->params = array(':sprintId' => $sprint->id, ':projectId' => $projectId);
		
		$sprints = Sprint::model()->findAll($criteria);
		
		$this->render(
			'index',
			array(
				'projectId' => $projectId,
				'sprints' => $sprints,
				)
			);
	}
}
