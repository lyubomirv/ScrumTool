<?php

Yii::import('application.models.task.*');

class TaskController extends Controller
{
	/**
	 * Filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
			array('application.filters.HasProjectFilter'),
		);
	}
	
	/**
	 * Access rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('taskList', 'view'),
				'users' => array('@'),
			),
			array('allow',
				'actions' => array('add', 'edit', 'delete'),
				'roles' => array('scrum_master'),
			),
			array('deny',
				'users' => array('?'),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * View a task
	 */
	public function actionView($projectId, $storyId, $taskId)
	{
		$story = Story::model()->findByPk(
			$storyId,
			'project_id = :projectId',
			array(':projectId' => $projectId)
			);
		$this->verifyStory($story);
		
		$task = Task::model()->findByPk(
			$taskId,
			'story_id = :storyId',
			array(':storyId' => $storyId)
			);
		$this->verifyTask($task);
		
		$this->renderPartial(
			'view',
			array(
				'story' => $story,
				'task' => $task,
				)
			);
	}
	
	/**
	 * Add a task to a story
	 */
	public function actionAdd($projectId, $storyId)
	{
		$story = Story::model()->findByPk(
			$storyId,
			'project_id = :projectId', array(':projectId' => $projectId)
			);
		$this->verifyStory($story);
		
		$taskForm = new TaskForm($storyId);
		
		$this->performAjaxValidation('task-form', $taskForm);
		
		if(isset($_POST['TaskForm']))
		{
			$success = false;
			
			$taskForm->attributes = $_POST['TaskForm'];
			$taskForm->purify();
			if($taskForm->validate())
			{
				// Create the task
				$task = new Task;
				$task->name = $taskForm->name;
				$task->story_id = $storyId;
				if($taskForm->description !== null)
				{
					$task->description = $taskForm->description;
				}
				$task->estimation = $taskForm->estimation;
				$success = $task->save();
			}
			
			$this->formResponse(
				$success,
				'task-submit',
				$taskForm,
				$this->createUrl('backlog/index')
				);
		}
		
		$this->displayForm('task-render', 'add', $taskForm);
	}
	
	/**
	 * Edit story
	 */
	public function actionEdit($projectId, $storyId, $taskId)
	{
		$story = Story::model()->findByPk(
			$storyId,
			'project_id = :projectId',
			array(':projectId' => $projectId)
			);
		$this->verifyStory($story);
		
		$task = Task::model()->findByPk(
			$taskId,
			'story_id = :storyId',
			array(':storyId' => $storyId)
			);
		$this->verifyTask($task);
		
		$taskForm = new TaskForm($storyId, $taskId);
		$taskForm->name = $this->purify($task->name);
		$taskForm->description = $this->purify($task->description);
		$taskForm->estimation = $this->purify($task->estimation);
		
		$this->performAjaxValidation('task-form', $taskForm);
		
		if(isset($_POST['TaskForm']))
		{
			$success = false;
			
			$taskForm->attributes = $_POST['TaskForm'];
			$taskForm->purify();
			if($taskForm->validate())
			{
				$task->name = $taskForm->name;
				$task->description = $taskForm->description;
				$task->estimation = $taskForm->estimation;
				
				$success = $task->save();
			}
			
			$this->formResponse(
				$success,
				'task-submit',
				$taskForm,
				$this->createUrl('backlog/index')
				);
		}
		
		$this->displayForm('task-render', 'edit', $taskForm);
	}
	
	/**
	 * Delete story
	 */
	public function actionDelete($projectId, $storyId, $taskId)
	{
		$story = Story::model()->findByPk(
			$storyId,
			'project_id = :projectId',
			array(':projectId' => $projectId)
			);
		$this->verifyStory($story);
		
		$task = Task::model()->findByPk(
			$taskId,
			'story_id = :storyId',
			array(':storyId' => $storyId)
			);
		$this->verifyTask($task);
		
		$success = $task->delete();
		
		$this->sendJsonResponse(
			$success,
			'There was an error archiving the task.'
			);
	}
	
	/**
	 * Verifies that a task is not null.
	 * If the task is null, sends an appropriate json message and ends.
	 * @param Task the Task
	 */
	private function verifyTask($task)
	{
		if($task === null)
		{
			$this->sendJsonResponse(
				false,
				'There is no such task in this story.'
				);
			
			Yii::app()->end();
		}
	}
}
