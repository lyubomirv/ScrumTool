<?php

Yii::import('application.classes.ColumnArranger');
Yii::import('application.classes.TaskStoryPointsCalculator');

class BoardController extends Controller
{
	/**
	 * Filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
			array('application.filters.HasSprintFilter'),
		);
	}
	
	/**
	 * Access rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('index' ,'move', 'columnTaskList', 'rearrangeColumnTask', 'assign', 'unassign'),
				'roles' => array('user'),
			),
			array('deny',
				'users' => array('?'),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * Default action
	 * @param projectId int project, which the sprint belongs to
	 * @param sprintId int id of the sprint whose SCRUM board to display
	 */
	public function actionIndex($projectId, $sprintId)
	{
		$sprint = $this->getSprintSafe($sprintId, $projectId);
		
		$columnArranger = new ColumnArranger;
		$columns = $columnArranger->getArrangedColumns($projectId);
		
		$sprintColumnTasks = array();
		foreach($columns as $column)
		{
			$columnTasks = SprintColumnTask::model()->findAllByAttributes(
				array('sprint_id' => $sprintId, 'column_id' => $column->id)
				);
			if($columnTasks !== null)
			{
				$columnTasks = $this->arrangeColumnTasks($columnTasks);
				$sprintColumnTasks = $sprintColumnTasks + array($column->id => $columnTasks);
			}
		}
		
		$taskStoryPoints = TaskStoryPointsCalculator::calculate($sprintId, $columns);
		
		$taskAssignees = $this->getTaskAssignees($sprintId);
		
		$currentUser = User::model()->findByPk(Yii::app()->user->getId());
		$currentUserName = $currentUser->username;
		if($currentUser->name !== null && !empty($currentUser->name))
		{
			$currentUserName = $currentUser->name;
		}
		
		$this->render('index', array(
			'projectId' => $projectId,
			'sprint' => $sprint,
			'columns' => $columns,
			'sprintColumnTasks' => $sprintColumnTasks,
			'taskStoryPoints' => $taskStoryPoints,
			'taskAssignees' => $taskAssignees,
			'currentUserName' => $currentUserName,
			)
		);
	}
	
	/**
	 * Move a task from one column to another
	 * @param $projectId int id of the project
	 * @param sprintId int id of the sprint
	 */
	public function actionMove($projectId, $sprintId)
	{
		$taskId = $this->getPostDataSafe('taskId');
		$fromColumnId = $this->getPostDataSafe('fromColumnId');
		$toColumnId = $this->getPostDataSafe('toColumnId');
		$previousId = $this->getPostDataSafe('previousId');
		
		// Test
		//$taskId = $_GET['taskId'];
		//$toColumnId = $_GET['toColumnId'];
		//$previousId = $_GET['previousId'];
		
		$task = $this->getTaskAndValidateInputData($projectId, $sprintId, $taskId, $fromColumnId, $toColumnId);
		
		$success = false;
		
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			// Find the entry
			$sprintColumnTask = $task->sprintColumnTasks[0];
			
			$fromColumnId = $sprintColumnTask->column_id;
			
			// Change ordering in the previous column
			$previousColumnNextTask = SprintColumnTask::model()->findByAttributes(
				array(
					'previous_id' => $sprintColumnTask->task->id,
					'sprint_id' => $sprintId,
					'column_id' => $fromColumnId
					)
				);
			if($previousColumnNextTask !== null)
			{
				//echo $sprintColumnTask->previous_id;
				$previousColumnNextTask->previous_id = $sprintColumnTask->previous_id;
				$previousColumnNextTask->save();
			}
			
			// Change ordering in the new column
			$newColumnNextTask = SprintColumnTask::model()->findByAttributes(
				array(
					'previous_id' => $previousId,
					'sprint_id' => $sprintId,
					'column_id' => $toColumnId
					)
				);
			if($newColumnNextTask !== null)
			{
				$newColumnNextTask->previous_id = $sprintColumnTask->task->id;
				$newColumnNextTask->save();
			}
			
			// Update
			$sprintColumnTask->column_id = $toColumnId;
			$sprintColumnTask->previous_id = $previousId;
			if(!$sprintColumnTask->save())
			{
				throw new CException('Can not move task to another column.');
			}
			
			// Log the moving
			$sprintTaskMove = new SprintTaskMove;
			$sprintTaskMove->sprint_id = $sprintId;
			$sprintTaskMove->task_id = $taskId;
			$sprintTaskMove->from_column_id = $fromColumnId;
			$sprintTaskMove->to_column_id = $toColumnId;
			if(!$sprintTaskMove->save())
			{
				throw new CException('Can not log the task moving to another column.');
			}
			
			$transaction->commit();
			
			$success = true;
		}
		catch(Exception $e)
		{
			$transaction->rollBack();
			
			$success = false;
		}
		
		$this->sendJsonResponse($success, 'There was an error moving the task.');
	}
	
	/**
	 * Move a task from inside its own column - change its position
	 * @param $projectId int id of the project
	 * @param sprintId int id of the sprint
	 */
	public function actionRearrangeColumnTask($projectId, $sprintId)
	{
		$taskId = $this->getPostDataSafe('taskId');
		$previousId = $this->getPostDataSafe('previousId');
		
		$task = $this->getTaskAndValidateInputData($projectId, $sprintId, $taskId);
		
		$success = false;
		
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			// Find the entry
			$sprintColumnTask = $task->sprintColumnTasks[0];
			
			$columnId = $sprintColumnTask->column_id;
			
			// Change ordering at the previous place of the task
			$previousNextTask = SprintColumnTask::model()->findByAttributes(
				array(
					'previous_id' => $sprintColumnTask->task->id,
					'sprint_id' => $sprintId,
					'column_id' => $columnId
					)
				);
			if($previousNextTask !== null)
			{
				$previousNextTask->previous_id = $sprintColumnTask->previous_id;
				$previousNextTask->save();
			}
			
			// Change ordering at the new place of the task
			$newNextTask = SprintColumnTask::model()->findByAttributes(
				array(
					'previous_id' => $previousId,
					'sprint_id' => $sprintId,
					'column_id' => $columnId
					)
				);
			if($newNextTask !== null)
			{
				$newNextTask->previous_id = $sprintColumnTask->task->id;
				$newNextTask->save();
			}
			
			// Update
			$sprintColumnTask->previous_id = $previousId;
			if(!$sprintColumnTask->save())
			{
				throw new CException('Can not move task to another column.');
			}
			
			$transaction->commit();
			
			$success = true;
		}
		catch(Exception $e)
		{
			$transaction->rollBack();
			
			$success = false;
		}
		
		$this->sendJsonResponse($success, 'There was an error rearranging the task.');
	}
	
	/**
	 * Assign the current user to a task
	 * @param $projectId int id of the project
	 * @param sprintId int id of the sprint
	 */
	public function actionAssign($projectId, $sprintId)
	{
		$taskId = $this->getPostDataSafe('taskId');
		
		$success = false;
		
		$sprintTask = SprintTask::model()->findByAttributes(
			array(
				'sprint_id' => $sprintId,
				'task_id' => $taskId,
				)
			);
		if($sprintTask === null)
		{
			throw new CHttpException(403);
		}
		
		$sprintTaskAssignment = new SprintTaskAssignment;
		$sprintTaskAssignment->sprint_task_id = $sprintTask->id;
		$sprintTaskAssignment->user_id = Yii::app()->user->getId();
		$success = $sprintTaskAssignment->save();
		
		$this->sendJsonResponse($success, 'There was an error assigning yourself to the task.');
	}
	
	/**
	 * Unassign the current user to a task
	 * @param $projectId int id of the project
	 * @param sprintId int id of the sprint
	 */
	public function actionUnassign($projectId, $sprintId)
	{
		$taskId = $this->getPostDataSafe('taskId');
		
		$success = false;
		
		$criteria = new CDbCriteria;
		$criteria->alias = 'sta';
		$criteria->join = 'LEFT JOIN sprint_task as st ON sta.sprint_task_id = st.id';
		$criteria->condition = 'st.sprint_id = :sprintId AND st.task_id = :taskId AND sta.user_id = :userId';
		$criteria->params = array(
			':sprintId' => $sprintId,
			':taskId' => $taskId,
			':userId' => Yii::app()->user->getId()
			);
		
		$sprintTaskAssignment = SprintTaskAssignment::model()->find($criteria);
		if($sprintTaskAssignment === null)
		{
			throw new CHttpException(403);
		}
		
		$success = $sprintTaskAssignment->delete();
		
		$this->sendJsonResponse($success, 'There was an error unassigning yourself from the task.');
	}
	
	/**
	 * Render the list of tasks for a column
	 * @param sprintId int id of the sprint
	 */
	public function actionColumnTaskList($projectId, $sprintId, $columnId)
	{
		$project = $this->getProject($projectId, true);
		$sprint = $this->getSprintSafe($sprintId, $projectId);
		$column = $this->getColumnSafe($columnId, $project);
		
		$columnArranger = new ColumnArranger;
		$columns = $columnArranger->getArrangedColumns($projectId);
		
		$columnTasks = SprintColumnTask::model()->findAllByAttributes(
			array('sprint_id' => $sprintId, 'column_id' => $column->id)
			);
		
		$taskStoryPoints = TaskStoryPointsCalculator::calculate($sprintId, $columns);
		
		$taskAssignees = $this->getTaskAssignees($sprintId);
		
		$currentUser = User::model()->findByPk(Yii::app()->user->getId());
		$currentUserName = $currentUser->username;
		if($currentUser->name !== null && !empty($currentUser->name))
		{
			$currentUserName = $currentUser->name;
		}
		
		$this->renderPartial(
			'columnTaskList',
			array(
				'projectId' => $projectId,
				'sprint' => $sprint,
				'columnTasks' => $columnTasks,
				'taskStoryPoints' => $taskStoryPoints,
				'taskAssignees' => $taskAssignees,
				'currentUserName' => $currentUserName,
				)
			);
	}
	
	/**
	 * Get task and verify the input data for moving a task.
	 * Verifies the task's validity.
	 * Redirects the user to the main page if the input data is not verified.
	 * @param sprintId int id of the sprint
	 * @param taskId int id of the task
	 * @param toColumnId int id of the column that the task is moved to,
	 * null if the task is moved to the same column
	 * @return Task the task
	 */
	private function getTaskAndValidateInputData($projectId, $sprintId, $taskId, $fromColumnId = null, $toColumnId = null)
	{
		if($toColumnId !== null)
		{
			if((int)$toColumnId !== 1
				&& (int)$toColumnId !== 2
				&& (int)$toColumnId !== 3
				&& (int)$toColumnId !== 4
				)
			{
				if(!ProjectColumn::model()->exists(
					'id = :toColumnId AND project_id = :projectId',
					array(':toColumnId' => $toColumnId, ':projectId' => $projectId)
					)
					)
				{
					throw new CHttpException(403);
				}
			}
		}
		
		$task = $this->getTaskSafe($taskId, $sprintId);
		
		if($fromColumnId !== null && $task->sprintColumnTasks[0]->column_id !== $fromColumnId)
		{
			$this->sendJsonResponse(false, Yii::t('board', 'Task is not in this column anymore'));
			Yii::app()->end();
			
			return null;
		}
		
		return $task;
	}
	
	/**
	 * Get task and verify that it is in the sprint with sprintId
	 * @param taskId int id of the task
	 * @param sprintId int id of the sprint, in which the task must be
	 * @return Task the task
	 */
	private function getTaskSafe($taskId, $sprintId)
	{
		$criteria = new CDbCriteria;
		$criteria->alias = 't';
		$criteria->join = 'JOIN sprint_task AS st ON st.task_id = t.id';
		$criteria->condition = 't.id = :taskId AND st.sprint_id = :sprintId';
		$criteria->limit = 1;
		$criteria->params = array(':taskId' => $taskId, ':sprintId' => $sprintId);
		
		$task = Task::model()->find($criteria);
		if($task === null)
		{
			throw new CHttpException(403);
		}
		
		return $task;
	}
	
	private function getColumnSafe($columnId, $project)
	{
		$column = null;
		
		if((int)$columnId === 1
			|| (int)$columnId === 2
			|| (int)$columnId === 3
			|| (int)$columnId === 4
			)
		{
			$column = ProjectColumn::model()->findByPk($columnId);
		}
		else
		{
			$column = ProjectColumn::model()->findByPk(
				$columnId,
				'project_id = :projectId',
				array(':projectId' => $project->id)
				);
		}
		
		if($column === null)
		{
			throw new CHttpException(403);
		}
		
		return $column;
	}
	
	private function arrangeColumnTasks($columnTasks)
	{
		if(count($columnTasks) === 0)
		{
			return $columnTasks;
		}
		
		$arranged = array();
		
		// Find and insert the first element
		$index = $this->hasColumnTaskWithPreviousId($columnTasks, -1);
		if($index === -1)
		{
			return $columnTasks;
		}
		
		array_push($arranged, $columnTasks[$index]);
		unset($columnTasks[$index]);
		
		// Arrange all the other elements
		while(count($columnTasks) > 0)
		{
			$lastElementId = $arranged[count($arranged) - 1]->task->id;
			
			$index = $this->hasColumnTaskWithPreviousId($columnTasks, $lastElementId);
			if($index === -1)
			{
				break;
			}
			
			array_push($arranged, $columnTasks[$index]);
			unset($columnTasks[$index]);
		}
		
		// Push what is left of the array (this is in case of error)
		$arranged = array_merge($arranged, $columnTasks);
		
		return $arranged;
	}
	
	/**
	 * Checks the array of sprint column tasks for an element with previous_id equal to $previousId
	 * @param columnTasks SprintColumnTask[] the array of tasks for a column in the current sprint
	 * @param previousId int the previous_id to search for
	 * @return int index of the column task in the array or -1 if such is not found
	 */
	private function hasColumnTaskWithPreviousId($columnTasks, $previousId)
	{
		foreach($columnTasks as $i => $columnTask)
		{
			if($columnTask->previous_id == $previousId)
			{
				return $i;
			}
		}
		
		return -1;
	}
	
	/**
	 * Gets the assignees for each task in the sprint
	 * @param sprintId int id of the sprint
	 * @return array in the form taskId -> array of names of assignees
	 */
	private function getTaskAssignees($sprintId)
	{
		$criteria = new CDbCriteria;
		$criteria->alias = 't';
		$criteria->join = 'LEFT JOIN sprint_task as st ON t.id = st.task_id';
		$criteria->condition = 'st.sprint_id = :sprintId';
		$criteria->params = array(':sprintId' => $sprintId);
		
		$tasks = Task::model()->findAll($criteria);
		
		$taskAssignees = array();
		
		foreach($tasks as $task)
		{
			if(empty($task->sprintTasks))
			{
				continue;
			}
			
			$sprintTask = $task->sprintTasks[0];
			if($sprintTask === null)
			{
				continue;
			}
			
			$sprintTaskAssignees = $sprintTask->sprintTaskAssignments;
			if(empty($sprintTaskAssignees))
			{
				continue;
			}
			
			$assignees = array();
			foreach($sprintTaskAssignees as $sprintTaskAssignee)
			{
				$user = $sprintTaskAssignee->user;
				if($user === null)
				{
					continue;
				}
				
				$name = '';
				if($user->name !== null && !empty($user->name))
				{
					$name = $user->name;
				}
				else
				{
					$name = $user->username;
				}
				
				array_push($assignees, $name);
			}
			
			$taskAssignees += array($task->id => $assignees);
		}
		
		return $taskAssignees;
	}
}
