<?php

/**
 * HasTeamFilter is a filter that ensures a user is a member of a team
 */
class HasTeamFilter extends CFilter
{
	protected function preFilter($filterChain)
	{
		return count($filterChain->controller->getTeams()) > 0;
	}
	
	// No need of postFilter
	/*
	protected function postFilter($filterChain)
	{
		// logic being applied after the action is executed
	}
	*/
}
