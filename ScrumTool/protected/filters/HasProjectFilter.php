<?php

/**
 * HasProjectFilter is a filter that ensures a user has selected a project and
 * that the project belongs to the user's team.
 */
class HasProjectFilter extends CFilter
{
	protected function preFilter($filterChain)
	{
		$projectId = Yii::app()->request->getQuery('projectId', -1);
		if($projectId === -1)
		{
			return false;
		}
		
		$project = Project::model()->findByPk($projectId);
		
		return $this->filterProject($project);
	}
	
	/**
	 * Verifies that the project is valid and can be accessed by the user
	 * @param project Project the project
	 * @return bool whether the project is valid and can be accessed by the user
	 */
	protected function filterProject($project)
	{
		if($project === null)
		{
			return false;
		}
		
		$userId = Yii::app()->user->getId();
		
		if($project->user_id !== null)
		{
			if($project->user_id === $userId)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		$teamMember = TeamMember::model()->findByAttributes(array('team_id' => $project->team_id, 'user_id' => $userId));
		
		return $teamMember !== null;
	}
	
	// No need of postFilter
	/*
	protected function postFilter($filterChain)
	{
		// logic being applied after the action is executed
	}
	*/
}
