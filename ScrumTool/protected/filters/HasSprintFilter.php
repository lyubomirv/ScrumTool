<?php

Yii::import('application.filters.HasProjectFilter');

/**
 * HasSprintFilter is a filter that ensures a user has selected a sprint and
 * that the sprint can be accessed by the user.
 */
class HasSprintFilter extends HasProjectFilter
{
	protected function preFilter($filterChain)
	{
		$userId = Yii::app()->user->getId();
		
		$sprintId = Yii::app()->request->getQuery('sprintId', -1);
		if($sprintId === -1)
		{
			return false;
		}
		
		$sprint = Sprint::model()->findByPk($sprintId);
		if($sprint === null)
		{
			return false;
		}
		
		return $this->filterProject($sprint->project);
	}
	
	// No need of postFilter
	/*
	protected function postFilter($filterChain)
	{
		// logic being applied after the action is executed
	}
	*/
}
