<?php

Yii::import('application.classes.DateDifferenceCalculator');

class DateDifferenceCalculatorTest extends CTestCase
{
	private $_offsetFromUTC;
	
	public function __construct()
	{
		parent::__construct();
		
		$currentDate = new DateTime(null);
		$this->_offsetFromUTC = $currentDate->getOffset();
	}
	
	public function testGetDaysNotCalculated()
	{
		$this->setExpectedException(
			'CException', 'calculateDifference must be called first.'
			);
		
		$dateDiffereceCalculator = new DateDifferenceCalculator;
		$dateDiffereceCalculator->getDays();
	}
	
	// public function testCalculateOneParameter()
	// {
		// $start = '20120301';
		
		// $dateDiffereceCalculator = new DateDifferenceCalculator;
		// $result = $dateDiffereceCalculator->calculate($start);
		
		// $this->assertEquals($result, true);
	// }
	
	// public function testCalculateTwoParameters()
	// {
		// $start = '20120301';
		// $end = '20120302';
		
		// $dateDiffereceCalculator = new DateDifferenceCalculator;
		// $result = $dateDiffereceCalculator->calculate($start, $end);
		
		// $this->assertEquals($result, true);
	// }
	
	public function testCalculateDays()
	{
		$start = '20120301';
		$end = '20120302';
		
		$dateDiffereceCalculator = new DateDifferenceCalculator;
		$dateDiffereceCalculator->calculate($start, $end);
		
		$this->assertEquals(
			$dateDiffereceCalculator->getDays(),
			1
			);
	}
	
	// public function testCalculateDaysReversed()
	// {
		// $start = '20120301';
		// $end = '20120302';
		
		// $dateDiffereceCalculator = new DateDifferenceCalculator;
		// $dateDiffereceCalculator->calculate($end, $start);
		
		// $this->assertEquals(
			// $dateDiffereceCalculator->getDays(),
			// -1
			// );
	// }
}
