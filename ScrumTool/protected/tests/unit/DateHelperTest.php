<?php

Yii::import('application.classes.DateHelper');

class SiteTest extends CTestCase
{
	private $_offsetFromUTC;
	
	public function __construct()
	{
		parent::__construct();
		
		$currentDate = new DateTime(null);
		$this->_offsetFromUTC = $currentDate->getOffset();
	}
	
	public function testGetCurrentDateTime()
	{
		echo $this->_offsetFromUTC;
		
		$dateHelper = new DateHelper;
		
		$this->assertEquals(time() - $this->_offsetFromUTC, $dateHelper->getCurrentDateTime());
	}
	
	public function testGetDateTimeAsUTC()
	{
		$dateHelper = new DateHelper;
		
		$this->assertEquals(time() - $this->_offsetFromUTC, $dateHelper->getDateTimeAsUTC(time()));
	}
}
