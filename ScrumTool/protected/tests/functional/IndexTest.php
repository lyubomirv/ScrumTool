<?php

class IndexTest extends WebTestCase
{
	public function testIndex()
	{
		$this->open('');
		$this->assertTextPresent('Welcome');
	}
	
	public function testLogin()
	{
		$this->open('');
		
		$this->assertTextPresent('Sign in');
		
		$this->clickAndWait('link=Sign in');
		
		$this->assertElementPresent('name=LoginForm[username]');
		$this->type('name=LoginForm[username]', 'test1');
		$this->type('name=LoginForm[password]', 'pass1');
		$this->clickAndWait("//input[@value='Sign in']");
		
		$this->assertTextPresent('Sign out');
	}
}
