<?php
/**
 * FormModel is the customized base form model class.
 * All form model classes for this application should extend from this base class.
 */
class FormModel extends CFormModel
{
	private $_purifier;
	
	/**
	 * Constructor
	 */
	public function __construct($scenario = '')
	{
		parent::__construct($scenario);
		
		$this->_purifier = null;
	}
	
	/**
	 * Purifies all attributes
	 */
	public function purify()
	{
		if($this->_purifier === null)
		{
			$this->_purifier = new CHtmlPurifier();
			$this->_purifier->options = array(
				'URI.AllowedSchemes' => array(
					'http' => true,
					'https' => true,
					),
				'Core.EscapeInvalidChildren' => true,
				'Core.EscapeInvalidTags' => true,
				);
		}
		
		foreach($this->attributeNames() as $attribute)
		{
			$this->$attribute = $this->_purifier->purify($this->$attribute);
		}
	}
}