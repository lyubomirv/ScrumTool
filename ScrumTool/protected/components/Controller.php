<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	//public $menu = array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	//public $breadcrumbs = array();
	
	private $_purifier;
	
	/**
	 * Constructor
	 * Set the layout to two-column one if a project is selected
	 */
	public function __construct($id, $module = null)
	{
		parent::__construct($id, $module);
		
		// Apply two-column layout if the user is logged
		$currentUser = Yii::app()->user;
		if(!$currentUser->isGuest)
		{
			$this->layout = '//layouts/column2';
		}
		
		$this->setLanguage();
		
		$this->_purifier = null;
	}
	
	/**
	 * Sets the application language.
	 * Gets the selected language from a 'language' cookie.
	 * If no language is selected (no cookie), it sets the user preferred language.
	 * If there is no preferred language, the default language is set - en_us.
	 */
	private function setLanguage()
	{
		$language = null;
		if(isset(Yii::app()->request->cookies['language']))
		{
			$language = Yii::app()->request->cookies['language']->value;
		}
		else
		{
			$language = Yii::app()->request->getPreferredLanguage();
			if($language === false)
			{
				$language = 'en_us';
			}
		}
		
		Yii::app()->language = $language;
	}
	
	protected function getPreviousPage()
	{
		return $_SERVER['HTTP_REFERER'];
	}
	
	/**
	 * Purifies a string
	 * @param content string string to purify
	 * @return string the purified string
	 */
	protected function purify($content)
	{
		if($this->_purifier === null)
		{
			$this->_purifier = new CHtmlPurifier();
			$this->_purifier->options = array(
				'URI.AllowedSchemes' => array(
					'http' => true,
					'https' => true,
					),
				'Core.EscapeInvalidChildren' => true,
				'Core.EscapeInvalidTags' => true,
				);
		}
		
		return $this->_purifier->purify($content);
	}
	
	public function getTeam($teamId, $verify = false)
	{
		$userId = Yii::app()->user->getId();
		
		$criteria = new CDbCriteria;
		$criteria->alias = 't';
		$criteria->join = 'LEFT JOIN team_member ON team_member.team_id = t.id';
		$criteria->condition = 't.id = :teamId AND team_member.user_id = :userId';
		$criteria->params = array(':teamId' => $teamId, ':userId' => $userId);
		
		$team = Team::model()->find($criteria);
		
		if($verify && $team === null)
		{
			throw new CHttpException;
		}
			
		return $team;
	}
	
	/**
	 * Get the teams that the user participates in
	 * @return Team[] the teams that the user participates in
	 */
	public function getTeams()
	{
		$userId = Yii::app()->user->getId();
		
		$criteria = new CDbCriteria;
		$criteria->alias = 't';
		$criteria->join = 'LEFT JOIN team_member ON team_member.team_id = t.id';
		$criteria->condition = 'team_member.user_id = :userId';
		$criteria->params = array(':userId' => $userId);
		
		$teams = Team::model()->findAll($criteria);
			
		return $teams;
	}
	
	/**
	 * Gets the project specified by its id.
	 * May apply a check that the project can be accessed by the user
	 * @param projectId int the project's id
	 * @param verify bool whether to assert that the project can be accessed by the user
	 * @return Project the project or null
	 */
	protected function getProject($projectId, $verify = false)
	{
		$project = Project::model()->findByPk($projectId);
		
		if($verify)
		{
			if(!$this->verifyProject($project))
			{
				return null;
			}
		}
		
		return $project;
	}
	
	/**
	 * Verify that the project is a valid one and that it can be accessed by the user
	 * @param project Project the project
	 * @return bool whether the project is valid
	 */
	protected function verifyProject($project)
	{
		if($project !== null)
		{
			if($project->user_id === Yii::app()->user->getId())
			{
				return true;
			}
			
			$teams = $this->getTeams();
			if($teams !== null)
			{
				foreach($teams as $team)
				{
					if($project->team_id === $team->id)
					{
						return true;
					}
				}
			}
		}
		
		throw new CHttpException(403);
		
		return false;
	}
	
	/**
	 * Returns whether there is a selected project
	 * @return bool whether there is a selected project
	 */
	protected function hasCurrentProject()
	{
		return $this->getCurrentProjectId() !== -1;
	}
	
	/**
	 * Gets the id of the currently selected project.
	 * @return int the id of the selected project or -1
	 */
	protected function getCurrentProjectId($verify = false)
	{
		$projectId = Yii::app()->request->getQuery('projectId', -1);
		
		if($verify)
		{
			$this->getProject($projectId, true);
		}
		
		return $projectId;
	}
	
	/**
	 * Gets the currently selected project.
	 * Also guards that the selected project belongs to the user's team.
	 * @return Project the selected project or null
	 */
	protected function getCurrentProject()
	{
		$projectId = $this->getCurrentProjectId();
		if($projectId < 0)
		{
			return null;
		}
		
		$project = $this->getProject($projectId, true);
		
		return $project;
	}
	
	/**
	 * Gets the id of the currently selected sprint.
	 * @return int the id of the selected sprint or -1
	 */
	protected function getCurrentSprintId()
	{
		$sprintId = Yii::app()->request->getQuery('sprintId', -1);
		
		return $sprintId;
	}
	
	protected function hasCurrentSprint()
	{
		return $this->getCurrentSprintId() !== -1;
	}
	
	/**
	 * Gets the latest sprint for a given project.
	 * If no project is supplied, tries to use the currently selected one.
	 * @param projectId int id of a project
	 * @return Sprint the latest sprint for the project
	 */
	protected function getLatestSprint($projectId = -1)
	{
		if($projectId === -1)
		{
			$projectId = $this->getCurrentProjectId(true);
		}
		
		$criteria = new CDbCriteria;
		$criteria->condition = 'project_id = :projectId';
		$criteria->order = 'date_end DESC';
		$criteria->limit = 1;
		$criteria->params = array(':projectId' => $projectId);
		
		$sprint = Sprint::model()->find($criteria);
		
		return $sprint;
	}
	
	/**
	 * Verifies that a story is not null.
	 * If the story is null, sends an appropriate json message and ends.
	 * @param Story the Story
	 */
	protected function verifyStory($story)
	{
		if($story === null)
		{
			$json = array('status' => 'error', 'message' => 'Invalid story');
			echo json_encode($json);
			
			Yii::app()->end();
		}
	}
	
	protected function getSprintSafe($sprintId, $projectId)
	{
		$sprint = Sprint::model()->findByPk(
			$sprintId,
			'project_id = :projectId',
			array(':projectId' => $projectId)
			);
		if($sprint === null)
		{
			throw new CHttpException(403);
		}
		
		return $sprint;
	}
	
	protected function getPostDataSafe($variableName)
	{
		if(isset($_POST[$variableName]))
		{
			return $_POST[$variableName];
		}
		else
		{
			throw new CHttpException(403);
		}
	}
	
	/**
     * Performs the ajax validation.
	 * @param formIdentifier string The identifier of the form: a string like: 'example-form'.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($formIdentifier, $model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax'] === $formIdentifier)
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
	
	/**
	 * Sends a response to the client in json form.
	 * If $success if false, the $errorMessage will be used.
	 * @param success Whether the operation was successful
	 * @param errorMessage Message to send in case of error.
	 */
	protected function sendJsonResponse($success, $errorMessage)
	{
		// Send response to the client
		if($success)
		{
			$json = array('status' => 'ok');
			echo json_encode($json);
			
			return;
		}
		else
		{
			$json = array('status' => 'error', 'message' => $errorMessage);
			echo json_encode($json);
			
			return;
		}
	}
	
	/**
	 * Perform a response to the client from processing a form
	 * @param success Whether the processing was successful
	 * @param ajaxString string The string to look for in the 'ajax'
	 * key, if it is an ajax query
	 * @param formModel CFormModel The form model
	 * @param redirectUrl string The usl to redirect if it is not an ajax request
	 * @param parameters array A list of parameters to send with the
	 * ajax response if success is true
	 */
	protected function formResponse($success, $ajaxString, $formModel, $redirectUrl, $parameters = array())
	{
		if(isset($_POST['ajax']) && $_POST['ajax'] === $ajaxString)
		{
			// Report the result directly to the page
			
			if($success)
			{
				$json = array_merge(array('status' => 'ok'), $parameters);
				echo json_encode($json);
				Yii::app()->end();
			}
			else
			{
				echo CActiveForm::validate($formModel);
				Yii::app()->end();
			}
		}
		else
		{
			$this->redirect($redirectUrl);
		}
	}
	
	/**
	 * Display a form to the client
	 * @param ajaxString string The string to look for in the 'ajax'
	 * key, if it is an ajax query
	 * @param view The form's view
	 * @param formModel CFormModel The form model
	 * @param parameters array Additional parameters
	 */
	protected function displayForm($ajaxString, $view, $formModel, $parameters = array())
	{
		$params = array('model' => $formModel);
		$params = array_merge($params, $parameters);
		
		// Display createTeam form
		if(isset($_POST['ajax']) && $_POST['ajax'] === $ajaxString)
		{
			$params = array_merge($params, array('showSubmitButton' => false));
			$this->renderPartial(
				$view,
				$params,
				false /* return the result*/,
				true /* postprocess (adds scripts) */
				);
		}
		else
		{
			$params = array_merge($params, array('showSubmitButton' => true));
			$this->render(
				$view,
				$params
				);
		}
	}
}