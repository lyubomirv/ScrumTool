<?php

/**
 * PhpAuthManager represents the auth manager
 * It overrides the CPhpAuthManager's init method_exists in
 * order to assign the role of the user run-time.
 */
class PhpAuthManager extends CPhpAuthManager
{
	/**
	 * Assignes the roles of the user
	 * Uses the information in team_member_role to set the roles
	 */
	public function init()
	{
		parent::init();
		
		$userId = Yii::app()->user->getId();
		
		if(!Yii::app()->user->isGuest)
		{
			// User role must always be assigned, if the user is a registered one
			$this->assign('user', $userId);
			
			$projectId = Yii::app()->request->getQuery('projectId', -1);
			if($projectId < 0)
			{
				// Assign team roles
				$teamId = Yii::app()->request->getQuery('teamId', -1);
				if($teamId < 0)
				{
					return;
				}
				
				$this->assignTeamRoles($teamId, $userId);
				
				return;
			}
			else
			{
				$project = Project::model()->findByPk($projectId);
				if($project === null)
				{
					return;
				}
				
				// Assign all roles if it is a user project
				if($project->user_id !== null)
				{
					if($project->user_id === $userId)
					{
						foreach(Yii::app()->params['roleNames'] as $role)
						{
							if(!$this->isAssigned($role, $userId))
							{
								$this->assign($role, $userId);
							}
						}
					}
					
					return;
				}
				
				// Assign team roles if it is a team project
				if($project->team_id !== null)
				{
					$this->assignTeamRoles($project->team_id, $userId);
				}
			}
		}
	}
	
	/**
	 * Assignes the team roles of the user
	 * @param teamId int id of the team
	 * @param userId int id of the user
	 */
	private function assignTeamRoles($teamId, $userId)
	{
		$criteria = new CDbCriteria;
		$criteria->alias = 'tmr';
		$criteria->join = 'JOIN team_member ON team_member.id = tmr.team_member_id';
		$criteria->condition = 'team_member.team_id = :teamId AND team_member.user_id = :userId';
		$criteria->params = array(':teamId' => $teamId, ':userId' => $userId);
		
		$teamMemberRoles = TeamMemberRole::model()->findAll($criteria);
		if($teamMemberRoles !== null && count($teamMemberRoles) > 0)
		{
			foreach($teamMemberRoles as $teamMemberRole)
			{
				$roleId = $teamMemberRole->role_id;
				$roleName = Yii::app()->params['roleNames'][$roleId];
				
				if(!$this->isAssigned($roleName, $userId))
				{
					$this->assign($roleName, $userId);
				}
			}
		}
	}
}